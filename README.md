# VueJS 2 - The Complete Guide

These are my notes from [VueJS - The Complete Guide](https://www.udemy.com/course/vuejs-2-the-complete-guide/) on [Udemy](www.udemy.com).

Maximilian Schwarzmüller's courses are excellent! Lesson goals are clearly defined. Lessons are a efficient. The material is great. I highly recommend them.

![certificate](VueJS.jpg)

---
