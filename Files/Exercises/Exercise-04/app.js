new Vue({
  el: '#exercise',
  data: {
    highlight: false,
    shrink: false,
    gotClass: true,
    barWidth: 50
  },
  methods: {
    startEffect: function() {
      this.highlight = true;
      var vm = this;
      setInterval(function() {
        vm.highlight = !vm.highlight;
        vm.shrink = !vm.shrink;
      }, 500)
    },
    progressBar: function() {
      this.barWidth = 50;
      var vm = this;
      setInterval(function() {
        if (vm.barWidth < 1000) {
          vm.barWidth += 50;
        }
      }, 500)
    }
  }
});
