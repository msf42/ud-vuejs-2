import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://udemy-vuejs-axios-515be.firebaseio.com'
})

instance.defaults.headers.common['SOMETHING'] = 'something'

export default instance