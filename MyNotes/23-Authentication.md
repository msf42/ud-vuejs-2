# Authentication

## 353: About this Section

Done

---

## 354 Module Introduction

- This will focus on authentication in the front end
- Firebase will be used as the backend
  - This will all apply to any custom backend

---

## 355: How Authentication Works in SPAs

- in non-SPAs it is managed in a session with the server

- in SPAs
  - we work with stateless, Restful APIs that don't care about connected clients and don't manage sessions
  - instead we receive Tokens (a very long string that can be decoded in JS)
  - the server can always verify the token
  - the token is saved in local storage
  - if we send a request to the server, we send the token
  - the token has an expiration

![image](./images/Lesson355.png)

---

## 356: Project Setup

- Set up email sign-in on Firebase
- Changed access rules

```js
{
  /* Visit https://firebase.google.com/docs/database/security to learn more about security rules. */
  "rules": {
    ".read": "auth != null",
    ".write": "true"
  }
}
```

[Firebase Rest API Docs](https://firebase.google.com/docs/reference/rest/auth#section-create-email-password)

---

## 357: Adding User Signup

- added our API key and the endpoint

### axios-auth.js

```js
const instance = axios.create({
  baseURL: 'https://identitytoolkit.googleapis.com/v1'
})
```

### signup.vue

```js
onSubmit () {
  const formData = {
    email: this.email,
    age: this.age,
    password: this.password,
    confirmPassword: this.confirmPassword,
    country: this.country,
    hobbies: this.hobbyInputs.map(hobby => hobby.value),
    terms: this.terms
  }
  console.log(formData)
  axios.post('/accounts:signUp?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
    email: formData.email,
    password: formData.password,
    returnSecureToken: true
  })
    .then(res => console.log(res))
    .catch(error => console.log(error))
}
```

### Results in browser and on Firebase

![image](./images/Lesson357.png)

![image](./images/Lesson357b.png)

---

## 358: Adding User Signin (Login)

- simply updated signin

### signin.vue

```js
onSubmit () {
  const formData = {
    email: this.email,
    password: this.password,
  }
  console.log(formData)
  axios.post('/accounts:signInWithPassword?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
    email: formData.email,
    password: formData.password,
    returnSecureToken: true
  })
    .then(res => console.log(res))
    .catch(error => console.log(error))
}
```

![image](./images/Lesson358.png)

---

## 359: Using Vue to Send Auth Requests

- moved signup/login logic to store

### store.js

```js
import Vue from 'vue'
import Vuex from 'vuex'
import axios from './axios-auth';


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    idToken: null,
    userId: null,
  },
  mutations: {

  },
  actions: {
    signup ({commit}, authData) {
      axios.post('/accounts:signUp?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
        .then(res => console.log(res))
        .catch(error => console.log(error))
    },
    login ({commit}, authData) {
      axios.post('/accounts:signInWithPassword?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
        .then(res => console.log(res))
        .catch(error => console.log(error))
    }
  },
  getters: {

  }
})
```

- replaced logic in signin.vue and signup.vue

```js
  this.$store.dispatch('login', {email: formData.email, password: formData.password})
```

---

## 360: Storing Auth Data in Vuex

### store.js

```js
import Vue from 'vue'
import Vuex from 'vuex'
import axios from './axios-auth';


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    idToken: null,
    userId: null,
  },
  mutations: {
    authUser (state, userData) {
      state.idToken = userData.token
      state.userId = userData.userId
    }
  },
  actions: {
    signup ({commit}, authData) {
      axios.post('/accounts:signUp?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
        .then(res => {
          console.log(res)
          commit('authUser', {
            token: res.data.idToken,
            userId: res.data.localId
          })
        })
        .catch(error => console.log(error))
    },
    login ({commit}, authData) {
      axios.post('/accounts:signInWithPassword?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
      .then(res => {
        console.log(res)
        commit('authUser', {
          token: res.data.idToken,
          userId: res.data.localId
        })
      })
        .catch(error => console.log(error))
    }
  },
  getters: {

  }
})
```

- verified in Vue DevTools

![image](./images/Lesson360.png)

---

## 361: More About the Token (JWT)

[JSON Web Token](https://jwt.io/)

---

## 362: Accessing Other Resources from Vuex

- updated store.js and dashboard.vue, but will add functionality in next lecture

### store.js

```js
import Vue from 'vue'
import Vuex from 'vuex'
import axios from './axios-auth';
import globalAxios from 'axios'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    idToken: null,
    userId: null,
    user: null
  },
  mutations: {
    authUser (state, userData) {
      state.idToken = userData.token
      state.userId = userData.userId
    },
    storeUser (state, user) {
      state.user = user
    }
  },
  actions: {
    signup ({commit, dispatch}, authData) {
      axios.post('/accounts:signUp?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
        .then(res => {
          console.log(res)
          commit('authUser', {
            token: res.data.idToken,
            userId: res.data.localId
          })
          dispatch('storeUser', authData)
        })
        .catch(error => console.log(error))
    },
    login ({commit}, authData) {
      axios.post('/accounts:signInWithPassword?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
      .then(res => {
        console.log(res)
        commit('authUser', {
          token: res.data.idToken,
          userId: res.data.localId
        })
      })
        .catch(error => console.log(error))
    },
    storeUser ({commit}, userData) {
      globalAxios.post('/users.json', userData)
        .then(res => console.log(res))
        .catch(error => console.log(error))
    },
    fetchUser ({commit}) {
      globalAxios.get('/users.json')
      .then(res => {
        console.log(res)
        const data = res.data
        const users = []
        for (let key in data) {
          const user = data[key]
          user.id = key
          users.push(user)
        }
        console.log(users);
        commit('storeUser', users[0])
      })
      .catch(error => console.log(error))
    }
  },
  getters: {
    user (state) {
      return state.user
    }
  }
})
```

### dashboard.vue

```js
<template>
  <div id="dashboard">
    <h1>That's the dashboard!</h1>
    <p>You should only get here if you're authenticated!</p>
    <p>Your email address: {{ email }}</p>
  </div>
</template>

<script>
import axios from 'axios';

export default {
  computed: {
    email () {
      return this.$store.getters.user.email
    }
  },
  created () {
    this.$store.dispatch('fetchUser')
  }
}
</script>

<style scoped>
  h1, p {
    text-align: center;
  }

  p {
    color: red;
  }
</style>
```

---

## 363: Sending the Token to the Backend

### store.js

```js
import Vue from 'vue'
import Vuex from 'vuex'
import axios from './axios-auth';
import globalAxios from 'axios'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    idToken: null,
    userId: null,
    user: null
  },
  mutations: {
    authUser (state, userData) {
      state.idToken = userData.token
      state.userId = userData.userId
    },
    storeUser (state, user) {
      state.user = user
    }
  },
  actions: {
    signup ({commit, dispatch}, authData) {
      axios.post('/accounts:signUp?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
        .then(res => {
          console.log(res)
          commit('authUser', {
            token: res.data.idToken,
            userId: res.data.localId
          })
          dispatch('storeUser', authData)
        })
        .catch(error => console.log(error))
    },
    login ({commit}, authData) {
      axios.post('/accounts:signInWithPassword?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
      .then(res => {
        console.log(res)
        commit('authUser', {
          token: res.data.idToken,
          userId: res.data.localId
        })
      })
        .catch(error => console.log(error))
    },
    storeUser ({commit, state}, userData) {
      if (!state.idToken) {
        return
      }
      globalAxios.post('/users.json' + '?auth=' + state.idToken, userData)
        .then(res => console.log(res))
        .catch(error => console.log(error))
    },
    fetchUser ({commit, state}) {
      if (!state.idToken) {
        return
      }
      globalAxios.get('/users.json' + '?auth=' + state.idToken)
      .then(res => {
        console.log(res)
        const data = res.data
        const users = []
        for (let key in data) {
          const user = data[key]
          user.id = key
          users.push(user)
        }
        console.log(users);
        commit('storeUser', users[0])
      })
      .catch(error => console.log(error))
    }
  },
  getters: {
    user (state) {
      return state.user
    }
  }
})
```

### dashboard.vue

```html
<template>
  <div id="dashboard">
    <h1>That's the dashboard!</h1>
    <p>You should only get here if you're authenticated!</p>
    <p v-if="email">Your email address: {{ email }}</p>
  </div>
</template>

<script>
import axios from 'axios';

export default {
  computed: {
    email () {
      return !this.$store.getters.user ? false : this.$store.getters.user.email
    }
  },
  created () {
    this.$store.dispatch('fetchUser')
  }
}
</script>

<style scoped>
  h1, p {
    text-align: center;
  }

  p {
    color: red;
  }
</style>
```

- checked in sign-up and sign-in and both work

---

## 364: Protecting Routes

### router.js

- now Dashboard redirects to Sign In if not signed in

```js
import Vue from 'vue'
import VueRouter from 'vue-router'

import store from './store'

import WelcomePage from './components/welcome/welcome.vue'
import DashboardPage from './components/dashboard/dashboard.vue'
import SignupPage from './components/auth/signup.vue'
import SigninPage from './components/auth/signin.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', component: WelcomePage },
  { path: '/signup', component: SignupPage },
  { path: '/signin', component: SigninPage },
  {
    path: '/dashboard',
    component: DashboardPage,
    beforeEnter (to, from, next) {
      if (store.state.idToken) {
        next()
      } else {
        next('/signin')
      }
    }
  }
]

export default new VueRouter({mode: 'history', routes})
```

---

## 365: Updating the UI State (based on Authentication State)

- added new getter to store.js

```js
isAuthenticated (state) {
  return state.idToken !== null
}
```

### Dashboard.vue

```html
<template>
  <header id="header">
    <div class="logo">
      <router-link to="/">Vue - Complete Guide</router-link>
    </div>
    <nav>
      <ul>
        <li v-if="!auth">
          <router-link to="/signup">Sign Up</router-link>
        </li>
        <li v-if="!auth">
          <router-link to="/signin">Sign In</router-link>
        </li>
        <li v-if="auth">
          <router-link to="/dashboard">Dashboard</router-link>
        </li>
      </ul>
    </nav>
  </header>
</template>

<script>
export default {
  computed: {
    auth () {
      return this.$store.getters.isAuthenticated
    }
  }
}
</script>
```

---

## 366: Adding User Logout

- added logout button and functionality

### added to store.js

```js
// mutation
clearAuthData (state) {
  state.idToken = null
  state.userId = null
}

// action
logout ({commit}) {
  commit('clearAuthData')
  router.replace('/signin')
},
```

### header.vue

```html
<template>
  <header id="header">
    <div class="logo">
      <router-link to="/">Vue - Complete Guide</router-link>
    </div>
    <nav>
      <ul>
        <li v-if="!auth">
          <router-link to="/signup">Sign Up</router-link>
        </li>
        <li v-if="!auth">
          <router-link to="/signin">Sign In</router-link>
        </li>
        <li v-if="auth">
          <router-link to="/dashboard">Dashboard</router-link>
        </li>
        <li v-if="auth">
          <button @click="onLogout" class="logout">Logout</button>
        </li>
      </ul>
    </nav>
  </header>
</template>

<script>
export default {
  computed: {
    auth () {
      return this.$store.getters.isAuthenticated
    }
  },
  methods: {
    onLogout () {
      this.$store.dispatch('logout')
    }
  }
}
</script>

<style scoped>
  #header {
    height: 56px;
    display: flex;
    flex-flow: row;
    justify-content: space-between;
    align-items: center;
    background-color: #521751;
    padding: 0 20px;
  }

  .logo {
    font-weight: bold;
    color: white;
  }

  .logo a {
    text-decoration: none;
    color: white;
  }

  nav {
    height: 100%;
  }

  ul {
    list-style: none;
    margin: 0;
    padding: 0;
    height: 100%;
    display: flex;
    flex-flow: row;
    align-items: center;
  }

  li {
    margin: 0 16px;
  }

  li a {
    text-decoration: none;
    color: white;
  }

  li a:hover,
  li a:active,
  li a.router-link-active {
    color: #fa923f;
  }

  .logout {
    background-color: transparent;
    border: none;
    font: inherit;
    color: white;
    cursor: pointer;
  }
</style>
```

---

## 367: Adding Auto Logout

### store.js

```js
import Vue from 'vue'
import Vuex from 'vuex'
import axios from './axios-auth';
import globalAxios from 'axios'
import router from './router'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    idToken: null,
    userId: null,
    user: null
  },
  mutations: {
    authUser (state, userData) {
      state.idToken = userData.token
      state.userId = userData.userId
    },
    storeUser (state, user) {
      state.user = user
    },
    clearAuthData (state) {
      state.idToken = null
      state.userId = null
    }
  },
  actions: {
    // NEW ACTION
    setLogoutTimer ({commit}, expirationTime) {
      setTimeout(() => {
        commit('clearAuthData')
      }, expirationTime * 1000)
    },
    signup ({commit, dispatch}, authData) {
      axios.post('/accounts:signUp?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
        .then(res => {
          console.log(res)
          commit('authUser', {
            token: res.data.idToken,
            userId: res.data.localId
          })
          dispatch('storeUser', authData)
          dispatch('setLogoutTimer', res.data.expiresIn) // new
        })
        .catch(error => console.log(error))
    },
    login ({commit, dispatch}, authData) {
      axios.post('/accounts:signInWithPassword?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
      .then(res => {
        console.log(res)
        commit('authUser', {
          token: res.data.idToken,
          userId: res.data.localId
        })
        dispatch('setLogoutTimer', res.data.expiresIn) // new
      })
        .catch(error => console.log(error))
    },
    logout ({commit}) {
      commit('clearAuthData')
      router.replace('/signin')
    },
    storeUser ({commit, state}, userData) {
      if (!state.idToken) {
        return
      }
      globalAxios.post('/users.json' + '?auth=' + state.idToken, userData)
        .then(res => console.log(res))
        .catch(error => console.log(error))
    },
    fetchUser ({commit, state}) {
      if (!state.idToken) {
        return
      }
      globalAxios.get('/users.json' + '?auth=' + state.idToken)
      .then(res => {
        console.log(res)
        const data = res.data
        const users = []
        for (let key in data) {
          const user = data[key]
          user.id = key
          users.push(user)
        }
        console.log(users);
        commit('storeUser', users[0])
      })
      .catch(error => console.log(error))
    }
  },
  getters: {
    user (state) {
      return state.user
    },
    isAuthenticated (state) {
      return state.idToken !== null
    }
  }
})
```

---

## 368: Adding Auto Login

### store.js

- lots of changes!

```js
import Vue from 'vue'
import Vuex from 'vuex'
import axios from './axios-auth';
import globalAxios from 'axios'
import router from './router'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    idToken: null,
    userId: null,
    user: null
  },
  mutations: {
    authUser (state, userData) {
      state.idToken = userData.token
      state.userId = userData.userId
    },
    storeUser (state, user) {
      state.user = user
    },
    clearAuthData (state) {
      state.idToken = null
      state.userId = null
    }
  },
  actions: {
    setLogoutTimer ({commit}, expirationTime) {
      setTimeout(() => {
        commit('clearAuthData')
      }, expirationTime * 1000)
    },
    signup ({commit, dispatch}, authData) {
      axios.post('/accounts:signUp?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
        .then(res => {
          console.log(res)
          commit('authUser', {
            token: res.data.idToken,
            userId: res.data.localId
          })
          const now = new Date()
          const expirationDate = new Date(now.getTime() + res.data.expiresIn * 1000)
          localStorage.setItem('token', res.data.idToken)
          localStorage.setItem('userId', res.data.localId)
          localStorage.setItem('expirationDate', expirationDate)
          dispatch('storeUser', authData)
          dispatch('setLogoutTimer', res.data.expiresIn)
        })
        .catch(error => console.log(error))
    },
    login ({commit, dispatch}, authData) {
      axios.post('/accounts:signInWithPassword?key=AIzaSyC-hhLUrpLgkPYnco-1q-reOm138cdRZFo', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
      .then(res => {
        console.log(res)
        const now = new Date()
          const expirationDate = new Date(now.getTime() + res.data.expiresIn * 1000)
          localStorage.setItem('token', res.data.idToken)
          localStorage.setItem('userId', res.data.localId)
          localStorage.setItem('expirationDate', expirationDate)
        commit('authUser', {
          token: res.data.idToken,
          userId: res.data.localId
        })
        dispatch('setLogoutTimer', res.data.expiresIn)
      })
        .catch(error => console.log(error))
    },
    tryAutoLogin ({commit}) {
      const token = localStorage.getItem('token')
      if (!token) {
        return
      }
      const expirationDate = localStorage.getItem('expirationDate')
      const now = new Date()
      if (now >= expirationDate) {
        return
      }
      const userId = localStorage.getItem('userId')
      commit('authUser', {
        token: token,
        userId: userId
      })
    },
    logout ({commit}) {
      commit('clearAuthData')
      localStorage.removeItem('expirationDate')
      localStorage.removeItem('token')
      localStorage.removeItem('userId')
      router.replace('/signin')
    },
    storeUser ({commit, state}, userData) {
      if (!state.idToken) {
        return
      }
      globalAxios.post('/users.json' + '?auth=' + state.idToken, userData)
        .then(res => console.log(res))
        .catch(error => console.log(error))
    },
    fetchUser ({commit, state}) {
      if (!state.idToken) {
        return
      }
      globalAxios.get('/users.json' + '?auth=' + state.idToken)
      .then(res => {
        console.log(res)
        const data = res.data
        const users = []
        for (let key in data) {
          const user = data[key]
          user.id = key
          users.push(user)
        }
        console.log(users);
        commit('storeUser', users[0])
      })
      .catch(error => console.log(error))
    }
  },
  getters: {
    user (state) {
      return state.user
    },
    isAuthenticated (state) {
      return state.idToken !== null
    }
  }
})
```

---

## 369: Wrap Up

Done

---

## 370: Useful Resources & Links

Nothing new, only previously saved links

---
