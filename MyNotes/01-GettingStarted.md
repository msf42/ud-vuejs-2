# Getting Started

## 01: Course Introduction

Nice intro.

---

## 02: Join Our Online Learning Community

Joined the Discord community.

---

## 03: Let's Create Our First VueJS Application

Using jsfiddle.net

### Hello World

### HTML

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <p>
  {{ title }}
  </p>
</div>
```

### Vue

```js
new Vue({
el: '#app',
data: {
  title: 'Hello World'
  }
});
```

---

## 04: Extending the VueJS Application

Adding a text box that changes the paragraph text.

### HTML

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <input type="text" v-on:input="changeTitle">
  <p>
  {{ title }}
  </p>
</div>
```

### Vue

```js
new Vue({
  el: '#app',
  data: {
    title: 'Hello World'
    },
   methods: {
    changeTitle: function(event) {
    this.title = event.target.value;
    }
   }
  });
```

---

## 05: Course Structure

Just an overview

---

## 06: Take Advantage of All Course Resources

![image](./images/image-20191101142427283.png)

---

## 07: Setup VueJS Locally

### index.html

```html
<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="">
  <script src="vue.js"></script>
</head>
<body>
  <div id="app">
    <input type="text" v-on:input="changeTitle">
      <p>
       {{ title }}
      </p>
  </div>

  <script>
    new Vue({
      el: '#app',
      data: {
        title: 'Hello World'
      },
      methods: {
        changeTitle: function(event) {
        this.title = event.target.value;
        }
      }
    });
  </script>
</body>
</html>
```

---

## 08: Module Resources and Useful Links

 [Official Docs - VueJS Installation](http://vuejs.org/guide/installation.html)

---
