# Advanced Component Usage

## 121: Intro

Done

---

## 122: Setting up the Module Project

### Summary

- Just made a single component (Quote), added some styling and connected it to App.vue.

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <app-quote></app-quote>
      </div>
    </div>
  </div>
</template>

<script>
  import Quote from './components/Quote.vue'
  export default {
    components: {
      appQuote: Quote
    }
  }
</script>

<style>
</style>

```

### components/Quote.vue

```html
<template>
  <div>
    <p>A Wonderful Quote</p>
  </div>
</template>

<script>
export default {
  
}
</script>

<style scoped>
  div {
    border: 1px solid #ccc;
    box-shadow: 1px 1px 2px black;
    padding: 30px;
    margin: 30px auto;
    text-align: center;
  }
</style>
```

---

## 123: Passing Content - The Suboptimal Solution

### Summary

- No real changes, just discussing how we might pass a large amount of html inside a component tag.

---

## 124: Passing Content with Slots

### Summary

- Adding slots allows us to pass data from the parent and render it in the child component.

### App.vue (part)

```html
  <app-quote>
    <h2>The Quote</h2>
    <p>A wonderful quote</p>
  </app-quote>
```

### Quote.vue (part)

```html
<template>
  <div>
    <slot></slot>
  </div>
</template>
```

---

## 125: How Slot Content Gets Compiled and Styled

### Summary

- The child component styling is applied to the data passed in from the parent. Everything else is handled by the parent.

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
         <app-quote>
           <h2>{{ quoteTitle }}</h2>
           <p>A wonderful quote</p>
         </app-quote>
      </div>
    </div>
  </div>
</template>

<script>
  import Quote from './components/Quote.vue'
  export default {
    data: function() {
      return {
        quoteTitle: 'The Quote'
      }
    },
    components: {
      appQuote: Quote
    }
  }
</script>

<style>
</style>
```

### Quote.vue

```html
<template>
  <div>
    <slot></slot>
  </div>
</template>

<script>
export default {

}
</script>

<style scoped>
  div {
    border: 1px solid #ccc;
    box-shadow: 1px 1px 2px black;
    padding: 30px;
    margin: 30px auto;
    text-align: center;
  }

  h2 {
    color: red;
  }
</style>
```

---

## 126: Changed Slot Styling Behavior

- VueJS now applies styling from the parent as well. But child styling takes precedent.

---

## 127: Using Multiple Slots (Named Slots)

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
         <app-quote>
           <h2 slot="title">{{ quoteTitle }}</h2>
           <p slot="content">A wonderful quote</p>
         </app-quote>
      </div>
    </div>
  </div>
</template>

<script>
  import Quote from './components/Quote.vue'
  export default {
    data: function() {
      return {
        quoteTitle: 'The Quote'
      }
    },
    components: {
      appQuote: Quote
    }
  }
</script>

<style>

</style>
```

### Quote.vue

```html
<template>
  <div>
    <div class="title">
      <slot name="title"></slot>
    </div>
    <hr>
    <div>
      <slot name="content"></slot>
    </div>
  </div>
</template>

<script>
export default {

}
</script>

<style scoped>
  div {
    border: 1px solid #ccc;
    box-shadow: 1px 1px 2px black;
    padding: 30px;
    margin: 30px auto;
    text-align: center;
  }

  h2 {
    color: red;
  }

  .title {
    font-style: italic;
  }
</style>
```

---

## 128: Default Slots and Slot Defaults

### Summary

- If a slot is not named, it will be the default slot.
- If text is inserted between the tags, it will be the default text, and replaced if a slot is made for it in the future.

### Quote.vue (`<template>`)

```html
<template>
  <div>
    <div class="title">
      <slot name="title"></slot>
      <span style="color: ccc"><slot name="subtitle">The Subtitle</slot></span>
    </div>
    <hr>
    <div>
      <slot></slot>
    </div>
  </div>
</template>
```

---

## 129: A Summary on Slots

Done

---

## 130: Switching Multiple Components wiht Dynamic Components

### Summary

- We can use `<component>` to add dynamic components

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <button @click="selectedComponent = 'appQuote'">Quote</button>
        <button @click="selectedComponent = 'appAuthor'">Author</button>
        <button @click="selectedComponent = 'appNew'">New</button>
        <hr>
        <p>{{ selectedComponent }}</p>
        <component :is="selectedComponent">
          <p>Default Content</p>
        </component>
         <!-- <app-quote>
           <h2 slot="title">{{ quoteTitle }}</h2>
           <p>A wonderful quote</p>
         </app-quote> -->
      </div>
    </div>
  </div>
</template>

<script>
  import Quote from './components/Quote.vue'
  import Author from './components/Author.vue'
  import New from './components/New.vue'
  export default {
    data: function() {
      return {
        quoteTitle: 'The Quote',
        selectedComponent: 'appQuote'
      }
    },
    components: {
      appQuote: Quote,
      appAuthor: Author,
      appNew: New
    }
  }
</script>

<style>

</style>
```

### Quote.vue

```html
<template>
  <div>
    <div class="title">
      <slot name="title"></slot>
      <span style="color: ccc"><slot name="subtitle">The Subtitle</slot></span>
    </div>
    <hr>
    <div>
      <slot></slot>
    </div>
  </div>
</template>

<script>
export default {

}
</script>

<style scoped>
  div {
    border: 1px solid #ccc;
    box-shadow: 1px 1px 2px black;
    padding: 30px;
    margin: 30px auto;
    text-align: center;
  }

  h2 {
    color: red;
  }

  .title {
    font-style: italic;
  }
</style>
```

### Author.vue

```html
<template>
  <div>
    <h3>The Author</h3>
  </div>
</template>

<script>
export default {

}
</script>

<style scoped>
</style>
```

### New.vue

```html
<template>
  <div>
    <h3>New Quote</h3>
  </div>
</template>

<script>
export default {

}
</script>

<style scoped>
</style>
```

---

## 131: Understanding Dynamic Component Behavior

![Video](./images/Lesson131.gif)

---

## 132: Keeping Dynamic Components Alive

Wrap the dynamic component in `<keep-alive>` to prevent it from being destroyed.

![Video](./images/Lesson132.gif)

---

## 133: Dynamic Component Lifecycle Hooks

Just a few other lifecycle hooks:

```html
<script>
export default {
  data: function () {
    return {
      counter: 0
    }
  },
  destroyed() {
    console.log('Destroyed');
  },
  deactivated() {
    console.log('Deactivated');
  },
  activated() {
    console.log('Activated');
  }
}
</script>
```

---

## Assignment 8: Time to Practice - Slots and Dynamic Components

### App.vue

```html
<template>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <br>
                <button class="btn btn-primary" @click="selectedComponent = 'appBlue', color='Blue'">Load Blue Template</button>
                <button class="btn btn-success" @click="selectedComponent = 'appGreen', color='Green'">Load Green Template</button>
                <button class="btn btn-danger" @click="selectedComponent = 'appRed', color='Red'">Load Red Template</button>
                <hr>
                <component :is="selectedComponent">
                  <p>Color is {{color}}</p>
                </component>
                <hr>
                <!-- <app-blue></app-blue>
                <app-green></app-green>
                <app-red></app-red> -->
            </div>
        </div>
    </div>
</template>

<script>
    import Blue from './components/Blue.vue';
    import Green from './components/Green.vue';
    import Red from './components/Red.vue';

    export default {
      data: function() {
      return {
        selectedComponent: 'appRed',
        color: 'Red'
      }
    },
        components: {
            appBlue: Blue,
            appGreen: Green,
            appRed: Red
        }
    }
</script>

<style>
</style>

```

---

## 134: Time to Practice - Slots and Dynamic Components (Code)

Done

---

## 135: Wrap Up

Done

---

## 136: Module Resources and Helpful Links

### Helpful Links

- [Official Docs - Slots](http://vuejs.org/guide/components.html#Content-Distribution-with-Slots)
- [Official Docs - Dynamic Components](http://vuejs.org/guide/components.html#Dynamic-Components)
- [Official Docs - Misc](http://vuejs.org/guide/components.html#Misc)

---
