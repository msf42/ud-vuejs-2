# Handling User Input with Forms

## 149: Module Introduction

Done

---

## 150: A Basic `<input>` Form Binding

In App.vue, add `email: ''` (line 127) to data, then use `v-model: email` (line 14) for two-way binding in the input box. Finally, use string interpolation `{{ email }}` to make email show up in box (line 105).

### App.vue

```html
<template>
  <div class="container">
    <form>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <h1>File a Complaint</h1>
          <hr>
          <div class="form-group">
            <label for="email">Mail</label>
            <input
                type="text"
                id="email"
                class="form-control"
                v-model="email">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input
                type="password"
                id="password"
                class="form-control">
          </div>
          <div class="form-group">
            <label for="age">Age</label>
            <input
                type="number"
                id="age"
                class="form-control">
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form-group">
          <label for="message">Message</label><br>
          <!-- Interpolation between <textarea>{{ test }}</textarea> doesn't work!-->
          <textarea
              id="message"
              rows="5"
              class="form-control"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <div class="form-group">
            <label for="sendmail">
              <input
                  type="checkbox"
                  id="sendmail"
                  value="SendMail"> Send Mail
            </label>
            <label for="sendInfomail">
              <input
                  type="checkbox"
                  id="sendInfomail"
                  value="SendInfoMail"> Send Infomail
            </label>
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form-group">
          <label for="male">
            <input
                type="radio"
                id="male"
                value="Male"> Male
          </label>
          <label for="female">
            <input
                type="radio"
                id="female"
                value="Female"> Female
          </label>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 from-group">
          <label for="priority">Priority</label>
          <select
              id="priority"
              class="form-control">
            <option></option>
          </select>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <button
              class="btn btn-primary">Submit!
          </button>
        </div>
      </div>
    </form>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4>Your Data</h4>
          </div>
          <div class="panel-body">
            <p>Mail: {{ email }}</p>
            <p>Password:</p>
            <p>Age:</p>
            <p>Message: </p>
            <p><strong>Send Mail?</strong></p>
            <ul>
              <li></li>
            </ul>
            <p>Gender:</p>
            <p>Priority:</p>
            <p>Switched:</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data () {
      return {
        email: ''
      }
    }
  }
</script>

<style>

</style>
```

---

## 151: Grouping Data and Pre-Populating Inputs

Using a slightly more complex structure, we put our variables in a single object:

```js
        userData: {
          email: '',
          password: '',
          age: 27
        }
```

and refer to them with `userData.email`, etc

---

## 152: Modifying User Input with Input Modifiers

### Summary

- `v-model.lazy` will only update when the user moves to a new area
- `v-model.trim` will trim excess white space
- `v-model.number` will convert it to a number (string is default)
- these can be combined

---

## 153: Binding `<textarea>` and Saving Line Breaks

### Summary

- Preserve line breaks in text box with `style="white-space: pre"`
- Also added to data - `message: 'A new text'`

### App.vue

```html
<template>
  <div class="container">
    <form>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <h1>File a Complaint</h1>
          <hr>
          <div class="form-group">
            <label for="email">Mail</label>
            <input
                type="text"
                id="email"
                class="form-control"
                v-model="userData.email">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input
                type="password"
                id="password"
                class="form-control"
                v-model.lazy="userData.password">
          </div>
          <div class="form-group">
            <label for="age">Age</label>
            <input
                type="number"
                id="age"
                class="form-control"
                v-model="userData.age">
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form-group">
          <label for="message">Message</label><br>
          <!-- Interpolation between <textarea>{{ test }}</textarea> doesn't work!-->
          <textarea
              id="message"
              rows="5"
              class="form-control"
              v-model="message"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <div class="form-group">
            <label for="sendmail">
              <input
                  type="checkbox"
                  id="sendmail"
                  value="SendMail"> Send Mail
            </label>
            <label for="sendInfomail">
              <input
                  type="checkbox"
                  id="sendInfomail"
                  value="SendInfoMail"> Send Infomail
            </label>
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form-group">
          <label for="male">
            <input
                type="radio"
                id="male"
                value="Male"> Male
          </label>
          <label for="female">
            <input
                type="radio"
                id="female"
                value="Female"> Female
          </label>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 from-group">
          <label for="priority">Priority</label>
          <select
              id="priority"
              class="form-control">
            <option></option>
          </select>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <button
              class="btn btn-primary">Submit!
          </button>
        </div>
      </div>
    </form>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4>Your Data</h4>
          </div>
          <div class="panel-body">
            <p>Mail: {{ userData.email }}</p>
            <p>Password: {{ userData.password }}</p>
            <p>Age: {{ userData.age }}</p>
            <p style="white-space: pre">Message: {{ message }}</p>
            <p><strong>Send Mail?</strong></p>
            <ul>
              <li></li>
            </ul>
            <p>Gender:</p>
            <p>Priority:</p>
            <p>Switched:</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data () {
      return {
        userData: {
          email: '',
          password: '',
          age: 27
        },
        message: 'A new text'
      }
    }
  }
</script>

<style>

</style>
```

---

## 154: Using Checkboxes and Saving Data in Arrays

Our checkboxes in App.vue:

```html
<!-- lines 49-63 -->
<div class="form-group">
  <label for="sendmail">
    <input
        type="checkbox"
        id="sendmail"
        value="SendMail"
        v-model="sendMail"> Send Mail
  </label>
  <label for="sendInfomail">
    <input
        type="checkbox"
        id="sendInfomail"
        value="SendInfoMail"
        v-model="sendMail"> Send Infomail
  </label>
</div>
```

We add our `v-model` to *both* checkboxes. VueJS will add the results of both to our array (`sendMail: []` in data section)

---

## 155: Using Radio Buttons

### Summary

-Radio buttons work the same way as checkboxes.

---

## 156: Handling Dropdowns with `<select>` and `<option>`

### Summary

-bind the `selectedPriority` in the `<select>` tag
-list options in the `<option>` tag using `v-for`

```html
<template>
  <div class="container">
    <form>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <h1>File a Complaint</h1>
          <hr>
          <div class="form-group">
            <label for="email">Mail</label>
            <input
                type="text"
                id="email"
                class="form-control"
                v-model="userData.email">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input
                type="password"
                id="password"
                class="form-control"
                v-model.lazy="userData.password">
          </div>
          <div class="form-group">
            <label for="age">Age</label>
            <input
                type="number"
                id="age"
                class="form-control"
                v-model="userData.age">
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form-group">
          <label for="message">Message</label><br>
          <!-- Interpolation between <textarea>{{ test }}</textarea> doesn't work!-->
          <textarea
              id="message"
              rows="5"
              class="form-control"
              v-model="message"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <div class="form-group">
            <label for="sendmail">
              <input
                  type="checkbox"
                  id="sendmail"
                  value="SendMail"
                  v-model="sendMail"> Send Mail
            </label>
            <label for="sendInfomail">
              <input
                  type="checkbox"
                  id="sendInfomail"
                  value="SendInfoMail"
                  v-model="sendMail"> Send Infomail
            </label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form-group">
          <label for="male">
            <input
                type="radio"
                id="male"
                value="Male"
                v-model="gender"> Male
          </label>
          <label for="female">
            <input
                type="radio"
                id="female"
                value="Female"
                v-model="gender"> Female
          </label>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 from-group">
          <label for="priority">Priority</label>
          <select
              id="priority"
              class="form-control"
              v-model="selectedPriority"> <!-- bind it to selectedPriority -->
            <option v-for="priority in priorities">{{ priority }}</option> <!-- show the list -->
          </select>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <button
              class="btn btn-primary">Submit!
          </button>
        </div>
      </div>
    </form>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4>Your Data</h4>
          </div>
          <div class="panel-body">
            <p>Mail: {{ userData.email }}</p>
            <p>Password: {{ userData.password }}</p>
            <p>Age: {{ userData.age }}</p>
            <p style="white-space: pre">Message: {{ message }}</p>
            <p><strong>Send Mail?</strong></p>
            <ul>
              <li v-for="item in sendMail">{{ item }}</li>
            </ul>
            <p>Gender: {{ gender }}</p>
            <p>Priority: {{ selectedPriority }}</p>
            <p>Switched:</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data () {
      return {
        userData: {
          email: '',
          password: '',
          age: 27
        },
        message: 'A new text',
        sendMail: [],
        gender: 'Male',
        selectedPriority: 'High',
        priorities: ['High', 'Medium', 'Low']
      }
    }
  }
</script>

<style>

</style>
```

---

## 157: What v-model does and How to Create a Custom Control

```html
<input v-model="userData.email">
<!-- is a shorter way of saying: -->
<input :value="userData.email"
       @input="userData.email = $event.target.value">
```

- also made Switch.vue (empty)

---

## 158: Creating a Custom Control (Input)

### Summary

- `v-model` **is expecting a value and input**
- Here, in Switch.vue, value is passed as a prop
  - `switched()` passes true/false (isOn) as input

```html
<template>
  <div>
    <div
        id="on"
        @click="switched(true)"
        :class="{active: value}">On</div>
    <div
        id="off"
        @click="switched(false)"
        :class="{active: !value}">Off</div>
  </div>
</template>

<script>
  export default {
    props: ['value'],
    methods: {
      switched(isOn) {
        this.$emit('input', isOn)
      }
    }
  }
</script>
```

- In App.vue, `dataSwitch` is bound to the `app-switch` component, with a default value of true

```html
<!--Line  97--><app-switch v-model="dataSwitch"></app-switch>
<!--Line 150-->dataSwitch: true
```

### App.vue

```html
<template>
  <div class="container">
    <form>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <h1>File a Complaint</h1>
          <hr>
          <div class="form-group">
            <label for="email">Mail</label>
            <input
                type="text"
                id="email"
                class="form-control"
                v-model="userData.email">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input
                type="password"
                id="password"
                class="form-control"
                v-model.lazy="userData.password">
          </div>
          <div class="form-group">
            <label for="age">Age</label>
            <input
                type="number"
                id="age"
                class="form-control"
                v-model="userData.age">
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form-group">
          <label for="message">Message</label><br>
          <!-- Interpolation between <textarea>{{ test }}</textarea> doesn't work!-->
          <textarea
              id="message"
              rows="5"
              class="form-control"
              v-model="message"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <div class="form-group">
            <label for="sendmail">
              <input
                  type="checkbox"
                  id="sendmail"
                  value="SendMail"
                  v-model="sendMail"> Send Mail
            </label>
            <label for="sendInfomail">
              <input
                  type="checkbox"
                  id="sendInfomail"
                  value="SendInfoMail"
                  v-model="sendMail"> Send Infomail
            </label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form-group">
          <label for="male">
            <input
                type="radio"
                id="male"
                value="Male"
                v-model="gender"> Male
          </label>
          <label for="female">
            <input
                type="radio"
                id="female"
                value="Female"
                v-model="gender"> Female
          </label>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 from-group">
          <label for="priority">Priority</label>
          <select
              id="priority"
              class="form-control"
              v-model="selectedPriority">
            <option v-for="priority in priorities">{{ priority }}</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <app-switch v-model="dataSwitch"></app-switch>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <button
              class="btn btn-primary">Submit!
          </button>
        </div>
      </div>
    </form>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4>Your Data</h4>
          </div>
          <div class="panel-body">
            <p>Mail: {{ userData.email }}</p>
            <p>Password: {{ userData.password }}</p>
            <p>Age: {{ userData.age }}</p>
            <p style="white-space: pre">Message: {{ message }}</p>
            <p><strong>Send Mail?</strong></p>
            <ul>
              <li v-for="item in sendMail">{{ item }}</li>
            </ul>
            <p>Gender: {{ gender }}</p>
            <p>Priority: {{ selectedPriority }}</p>
            <p>Switched: {{ dataSwitch }}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
  import Switch from './Switch.vue';
  export default {
    data () {
      return {
        userData: {
          email: '',
          password: '',
          age: 27
        },
        message: 'A new text',
        sendMail: [],
        gender: 'Male',
        selectedPriority: 'High',
        priorities: ['High', 'Medium', 'Low'],
        dataSwitch: true
      }
    },
    components: {
      appSwitch: Switch
    }
  }
</script>
```

---

## 159: Submitting a Form

### App.vue

```html
<template>
  <div class="container">
    <form>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <h1>File a Complaint</h1>
          <hr>
          <div class="form-group">
            <label for="email">Mail</label>
            <input
                type="text"
                id="email"
                class="form-control"
                v-model="userData.email">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input
                type="password"
                id="password"
                class="form-control"
                v-model.lazy="userData.password">
          </div>
          <div class="form-group">
            <label for="age">Age</label>
            <input
                type="number"
                id="age"
                class="form-control"
                v-model="userData.age">
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form-group">
          <label for="message">Message</label><br>
          <!-- Interpolation between <textarea>{{ test }}</textarea> doesn't work!-->
          <textarea
              id="message"
              rows="5"
              class="form-control"
              v-model="message"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <div class="form-group">
            <label for="sendmail">
              <input
                  type="checkbox"
                  id="sendmail"
                  value="SendMail"
                  v-model="sendMail"> Send Mail
            </label>
            <label for="sendInfomail">
              <input
                  type="checkbox"
                  id="sendInfomail"
                  value="SendInfoMail"
                  v-model="sendMail"> Send Infomail
            </label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form-group">
          <label for="male">
            <input
                type="radio"
                id="male"
                value="Male"
                v-model="gender"> Male
          </label>
          <label for="female">
            <input
                type="radio"
                id="female"
                value="Female"
                v-model="gender"> Female
          </label>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 from-group">
          <label for="priority">Priority</label>
          <select
              id="priority"
              class="form-control"
              v-model="selectedPriority">
            <option v-for="priority in priorities">{{ priority }}</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <app-switch v-model="dataSwitch"></app-switch>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <button
              class="btn btn-primary"
              @click.prevent="submitted">Submit!
          </button>
        </div>
      </div>
    </form>
    <hr>
    <div class="row" v-if="isSubmitted">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4>Your Data</h4>
          </div>
          <div class="panel-body">
            <p>Mail: {{ userData.email }}</p>
            <p>Password: {{ userData.password }}</p>
            <p>Age: {{ userData.age }}</p>
            <p style="white-space: pre">Message: {{ message }}</p>
            <p><strong>Send Mail?</strong></p>
            <ul>
              <li v-for="item in sendMail">{{ item }}</li>
            </ul>
            <p>Gender: {{ gender }}</p>
            <p>Priority: {{ selectedPriority }}</p>
            <p>Switched: {{ dataSwitch }}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
  import Switch from './Switch.vue';
  export default {
    data () {
      return {
        userData: {
          email: '',
          password: '',
          age: 27
        },
        message: 'A new text',
        sendMail: [],
        gender: 'Male',
        selectedPriority: 'High',
        priorities: ['High', 'Medium', 'Low'],
        dataSwitch: true,
        isSubmitted: false
      }
    },
    methods: {
      submitted() {
        this.isSubmitted = true;
      }
    },
    components: {
      appSwitch: Switch
    }
  }
</script>
```

---

## Assignment 9: Forms

```html
<template>
  <div class="container">
    <form>
      <div class="row" v-if="!isSubmitted">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <!-- Exercise 1 -->
          <!-- Create a Signup Form where you retrieve the following Information -->
          <!-- Full Name (First Name + Last Name) -->
          <!-- Mail -->
          <!-- Password -->
          <!-- Store Data? Yes/No -->
          <h1> Sign Up Form </h1>
          <div>
            <label for="firstName">First Name</label>
            <input
              type="text"
              id="firstName"
              class="form-control"
              v-model="userData.firstName">
          </div>
          <div>
            <label for="lastName">Last Name</label>
            <input
              type="text"
              id="lastName"
              class="form-control"
              v-model="userData.lastName">
          </div>
          <div>
            <label for="email">E-mail</label>
            <input
              type="text"
              id="email"
              class="form-control"
              v-model="userData.email">
          </div>
          <div>
            <label for="password">Password</label>
            <input
              type="text"
              id="password"
              class="form-control"
              v-model="userData.password">
          </div>
          <div class="form-group">
            <label>Store Data?</label>
            <label for="yes">
              <input
                type="radio"
                id="yes"
                value="yes"
                v-model="userData.storeData"> Yes
            </label>
            <label for="no">
              <input
                type="radio"
                id="no"
                value="no"
                v-model="userData.storeData"> No
            </label>
          </div>
          <div class="row">
            <button
              class="btn btn-primary"
              @click.prevent="submitted"> Submit
            </button>
          </div>

          <!-- Exercise 2 -->
          <!-- Only display the Form if it has NOT been submitted -->
          <!-- Display the Data Summary ONCE the Form HAS been submitted -->

          <!-- Exercise 3 -->
          <!-- Edit the Example from above and create a custom "Full Name" Control -->
          <!-- which still holds the First Name and Last Name Input Field -->
        </div>
      </div>
    </form>
    <hr>
    <div class="row" v-if="isSubmitted">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4>Your Data</h4>
          </div>
          <div class="panel-body">
            <p>Full Name: {{ userData.fullName }}</p>
            <p>Mail: {{ userData.email }}</p>
            <p>Password: {{ userData.password }}</p>
            <p>Store in Database?: {{ userData.storeData }}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data () {
      return {
        userData: {
          firstName: '',
          lastName: '',
          fullName: '',
          email: '',
          password: '',
          storeData: 'Yes'
        },
        isSubmitted: false
      }
    },
    methods: {
      submitted() {
        this.isSubmitted = true;
        this.userData.fullName = this.userData.firstName + ' ' + this.userData.lastName;
      }
    }
  }
</script>
```

---

## 160: Forms (Code)

Done

---

## 161: Wrap Up

Done

---

## 162: Module Resources and Useful Links

### Useful Links

- Official Docs - [Forms](http://vuejs.org/guide/forms.html)

---
