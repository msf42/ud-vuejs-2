# Understanding the VueJS Instance

## 64: Module Introduction

Done.

---

## 65: Some Basics About the VueJS Instance

Done.

---

## 66: Using Multiple Vue Instances

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app1">
    <h1 ref="heading">{{ title }}</h1>
    <button v-on:click="show">Show Paragraph</button>
    <p v-if="showParagraph">This is not always visible</p>
  </div>
  <div id="app2">
    <h1 ref="heading">{{ title }}</h1>
  </div>
  <script src="app.js"></script>
</body>
</html>
```

### app.js

```js
new Vue({
  el: '#app1',
  data: {
    title: 'The VueJS Instance',
    showParagraph: false
  },
  methods: {
    show: function() {
      this.showParagraph = true;
      this.updateTitle('The VueJS Instance (Updated)');
    },
    updateTitle: function(title) {
      this.title = title;
    }
  },
  computed: {
    lowercaseTitle: function() {
      return this.title.toLowerCase();
    }
  },
  watch: {
    title: function(value) {
      alert('Title changed, new value: ' + value);
    }
  }
});

new Vue({
  el: '#app2',
  data: {
    title: 'The second instance'
  }
});
```

![image-20191115125337163](./images/image-20191115125337163.png)

---

## 67: Accessing the Vue Instance from Outside

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app1">
    <h1 ref="heading">{{ title }}</h1>
    <button v-on:click="show">Show Paragraph</button>
    <p v-if="showParagraph">This is not always visible</p>
  </div>
  <div id="app2">
    <h1 ref="heading">{{ title }}</h1>
    <button @click="onChange">Change something in Vue 1</button>
  </div>
  <script src="app.js"></script>
</body>
</html>
```

### app.js

```js
var vm1 = new Vue({
  el: '#app1',
  data: {
    title: 'The VueJS Instance',
    showParagraph: false
  },
  methods: {
    show: function() {
      this.showParagraph = true;
      this.updateTitle('The VueJS Instance (Updated)');
    },
    updateTitle: function(title) {
      this.title = title;
    }
  },
  computed: {
    lowercaseTitle: function() {
      return this.title.toLowerCase();
    }
  },
  watch: {
    title: function(value) {
      alert('Title changed, new value: ' + value);
    }
  }
});

setTimeout(function() {
  vm1.title = 'Changed by Timer';
}, 3000);

var vm2 = new Vue({
  el: '#app2',
  data: {
    title: 'The second instance'
  },
  methods: {
    onChange: function() {
      vm1.title = "Changed!";
    }
  }
});
```

![Video](./images/Lesson067.gif)

---

## 68: How VueJS Manages Your Data and Methods

Done.

---

## 69: A Closer Look at `$el` and `$data`

![image-20191115132232926](./images/image-20191115132232926.png)

- `$el` refers to our div. This is how VueJS keeps track of our instance.
- `$data` refers to our data. It can be accessed using `$variable1`

---

## 70: Placing `$refs` and Using Them on Your Templates

`$refs` allows us to place a tag in any html element and access it. Here,

```html
   <button v-on:click="show" ref="myButton">Show Paragraph</button>
```

```js
  methods: {
    show: function() {
      this.showParagraph = true;
      this.updateTitle('The VueJS Instance (Updated)');
      this.$refs.myButton.innerText = 'Test';
    },
```

causes the button text to change to 'Test' after the 'show' function is called.

![image-20191115133317910](./images/image-20191115133317910.png)

---

## 71: Where to Learn More About the Vue API

[Vue API](https://vuejs.org/v2/api/)

---

## 72: Mounting a Template

Note: these methods are rare.

If we remove `el: #app1` from the VueJS code, we can call it later by using `vm1.$mount('#app1');`

By using

```html
  <div id="app3"></div>
```

```js
vm3.$mount('#app3');
```

we get this:

![image-20191115135804469](./images/image-20191115135804469.png)

We could also write the JS like this:

```js
vm3.$mount();

document.getElementById('app3').appendChild(vm3.$el)
```

---

## 73: Using Components

In the javascript:

- this defines the `hello` component

```js
Vue.component('hello', {
  template: '<h1>Hello!</h1>'
});
```

In the html:

- These will print "Hello!" twice.

```html
<hello></hello>
<hello></hello>
```

In addition, this will print another "Hello!":

```html
<div class="hello"></div>
```

```js
var vm3 = new Vue({
  template: '<h1>Hello!</h1>'
});

vm3.$mount();
```

---

## 74: Limitations of Some Templates

Done.

---

## 75: How VueJS Updates the DOM

![image-20191115151541963](./images/image-20191115151541963.png)

---

## 76: The VueJS Instance Lifecycle

![image-20191115152639042](./images/image-20191115152639042.png)

---

## 77: The VueJS Instance Lifecycle in Practice

![image](./images/image-20191115154758530.png)

The first parts run as expected. The update parts only run once. There is no need to update when you are updating with the exact same thing, so it doesn't waste the time. After clicking 'Destroy', no other methods are available, because VueJS no longer has control of the DOM.

---

## 78: Module Wrap Up

Done

---

## 79: Module Resources and Useful Links

### JSFiddle:

- [The Vue Instance Code](https://jsfiddle.net/smax/9a2k6cja/2/)
- [The VueJS Instance Lifecycle](https://jsfiddle.net/smax/jcgw7ak8/)

### Useful Links:

- Official Docs - [The Vue Instance](http://vuejs.org/guide/instance.html)

---
