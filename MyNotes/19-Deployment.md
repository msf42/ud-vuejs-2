# Deploying a VueJS Application

## 309: Module Introduction

- Just ran `npm run build`

---

## 310: Preparing for Deployment

- overview of dist folder and webpack settings

---

## 311: AWS S3 & Bucket Policies

- notes on Bucket policies

---

## 312: Deploying the App (Example: AWS S3)

- Video uses AWS
- I deployed on Netlify
  - [Link](https://stock-trader-msf42.netlify.app/#)

---
