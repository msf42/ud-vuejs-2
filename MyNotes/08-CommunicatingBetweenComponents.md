# Communicating Between Components

## 106: Module Introduction

Done

---

## 107: Communication Problems

Continues in 108

---

## 108: Using Props for Parent => Child Communication

![Video](./images/Lesson108.gif)

- User.vue and UserDetail.vue are the important files being changed here.

### User.vue

```html
<template>
  <div class="component">
    <h1>The User Component</h1>
    <p>I'm an awesome User!</p>
    <button @click="changeName">Change My Name</button> <!-- method passed here -->
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <app-user-detail :name="name"></app-user-detail> <!-- pass in name here -->
        <!-- the : makes it dynamic, referring to the data below -->
      </div>
      <div class="col-xs-12 col-sm-6">
        <app-user-edit></app-user-edit>
      </div>
    </div>
  </div>
</template>

<script>
  import UserDetail from './UserDetail.vue';
  import UserEdit from './UserEdit.vue';

  export default {
    data: function() {               {/* data and methods here */}
      return {
        name: 'Max'
      }
    },
    methods: {
      changeName() {
        this.name='Anna'
      }
    },
    components: {
      appUserDetail: UserDetail,
      appUserEdit: UserEdit
    }
  }
</script>

<style scoped>
  div {
    background-color: lightblue;
  }
</style>
```

### UserDetail.vue

```html
<template>
  <div class="component">
    <h3>You may view the User Details here</h3>
    <p>Many Details</p>
    <p>User Name: {{ name }}</p>
  </div>
</template>

<script>
  export default {
    props: ['name'] {/* props defined here, must match property in template above */}
  }
</script>

<style scoped>
  div {
    background-color: lightcoral;
  }
</style>
```

---

## 109: Naming "props"

When using single templates, it's okay to use camelCase.

---

## 110: Using "props" in the Child Component

### UserDetail.vue

```html
<template>
  <div class="component">
    <h3>You may view the User Details here</h3>
    <p>Many Details</p>
    <p>User Name: {{ switchName() }}</p>
  </div>
</template>

<script>
  export default {
    props: ['myName'],
    methods: {         {/* We can also add methods */}
      switchName() {
        return this.myName.split("").reverse().join("");
      }
    }
  }
</script>

<style scoped>
  div {
    background-color: lightcoral;
  }
</style>
```

---

## 111: Validating "props"

### UserDetail.vue

```html
<!-- We can make props an object, and make each prop itself an object. Here we added validations for 'myName', making it required and a string -->
<script>
  export default {
    props: {
      myName: {
        type: String,
        required: true
    },
    methods: {
      switchName() {
        return this.myName.split("").reverse().join("");
      }
    }
  }
</script>
```

---

## 112: Using Custom Events for Child => Parent Communication

![image](./images/image-20191121132524755.png)

- Now we will be sending information from the Parent ("The User Component") to the child (pink and green squares)

### User.vue

```html
<template>
  <div class="component">
    <h1>The User Component</h1>
    <p>I'm an awesome User!</p>
    <button @click="changeName">Change My Name</button>
    <p>Name is {{ name }}</p>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <app-user-detail :myName="name" @nameWasReset="name = $event"></app-user-detail>
      </div>  <!-- listens for nameWasReset, sets name to event, see UserDetail.vue below -->
      <div class="col-xs-12 col-sm-6">
        <app-user-edit></app-user-edit>
      </div>
    </div>
  </div>
</template>

<script>
  import UserDetail from './UserDetail.vue';
  import UserEdit from './UserEdit.vue';

  export default {
    data: function() {
      return {
        name: 'Max'
      }
    },
    methods: {
      changeName() {
        this.name='Anna'
      }
    },
    components: {
      appUserDetail: UserDetail,
      appUserEdit: UserEdit
    }
  }
</script>

<style scoped>
  div {
    background-color: lightblue;
  }
</style>
```

### UserDetail.vue

```html
<template>
  <div class="component">
    <h3>You may view the User Details here</h3>
    <p>Many Details</p>
    <p>User Name: {{ switchName() }}</p>
    <button @click="resetName">Reset Name</button>
  </div>
</template>

<script>
  export default {
    props: {
      myName: {
        type: String
      }
    },
    methods: {
      switchName() {
        return this.myName.split("").reverse().join("");
      },
      resetName() {
        this.myName = 'Max';
        this.$emit('nameWasReset', this.myName); {/* emits name Max as 'nameWasReset' */}
      }
    }
  }
</script>

<style scoped>
  div {
    background-color: lightcoral;
  }
</style>
```

![Video](./images/Lesson112.gif)

---

## 113: Understanding Unidirectional Data Flow

![image](./images/113.png)

- Communication can happen from parent to child, or from child to parent, but not from child to child.

![image](./images/113b.png)

- Instead:
  - Parent passes callback as a prop
  - Child uses callback to pas data back to parent
  - Parent passes data as props to other child

---

## 114: Communicating with Callback Functions

### User.vue

```html
<template>
  <div class="component">
    <h1>The User Component</h1>
    <p>I'm an awesome User!</p>
    <button @click="changeName">Change My Name</button>
    <p>Name is {{ name }}</p>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <!-- pass the prop resetFn, which is a pointer to resetName -->
        <app-user-detail
          :myName="name"
          @nameWasReset="name = $event"
          :resetFn="resetName">
        </app-user-detail>
      </div>
      <div class="col-xs-12 col-sm-6">
        <app-user-edit></app-user-edit>
      </div>
    </div>
  </div>
</template>

<script>
  import UserDetail from './UserDetail.vue';
  import UserEdit from './UserEdit.vue';

  export default {
    data: function() {
      return {
        name: 'Max'
      }
    },
    methods: {
      changeName() {
        this.name='Anna'
      },
      resetName() { /* The same function in the child component, now in the parent */
        this.name = 'Max';
      }
    },
    components: {
      appUserDetail: UserDetail,
      appUserEdit: UserEdit
    }
  }
</script>

<style scoped>
  div {
    background-color: lightblue;
  }
</style>
```

### UserDetail.vue

```html
<template>
  <div class="component">
    <h3>You may view the User Details here</h3>
    <p>Many Details</p>
    <p>User Name: {{ switchName() }}</p>
    <button @click="resetName">Reset Name</button>
    <button @click="resetFn()">Reset Name</button>
    <!--instead of executing my own resetName, executes resetFn that was passed down as a prop-->
  </div>
</template>

<script>
  export default {
    props: {
      myName: {
        type: String
      },
      resetFn: Function {/* resetFn is a prop, must be a function */}
    },
    methods: {
      switchName() {
        return this.myName.split("").reverse().join("");
      },
      resetName() {
        this.myName = 'Max';
        this.$emit('nameWasReset', this.myName);
      }
    }
  }
</script>

<style scoped>
  div {
    background-color: lightcoral;
  }
</style>
```

---

## 115: Communication Between Sibling Components

![image](./images/image-20191125114346752.png)

### User.vue

```html
<template>
  <div class="component">
    <h1>The User Component</h1>
    <p>I'm an awesome User!</p>
    <button @click="changeName">Change My Name</button>
    <p>Name is {{ name }}</p>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <app-user-detail
          :myName="name"
          @nameWasReset="name = $event"
          :resetFn="resetName"
          :userAge="age">
        </app-user-detail>
      </div>
      <div class="col-xs-12 col-sm-6">
        <app-user-edit
           :userAge="age"
           @ageWasEdited="age = $event"
        ></app-user-edit>
      </div>
    </div>
  </div>
</template>

<script>
  import UserDetail from './UserDetail.vue';
  import UserEdit from './UserEdit.vue';

  export default {
    data: function() {
      return {
        name: 'Max',
        age: 27
      }
    },
    methods: {
      changeName() {
        this.name='Anna'
      },
      resetName() {
        this.name = 'Max';
      }
    },
    components: {
      appUserDetail: UserDetail,
      appUserEdit: UserEdit
    }
  }
</script>

<style scoped>
  div {
    background-color: lightblue;
  }
</style>
```

### UserDetail.vue

```html
<template>
  <div class="component">
    <h3>You may view the User Details here</h3>
    <p>Many Details</p>
    <p>User Name: {{ switchName() }}</p>
    <p>User Age: {{ userAge }}</p>
    <button @click="resetName">Reset Name</button>
    <button @click="resetFn()">Reset Name</button>
  </div>
</template>

<script>
  export default {
    props: {
      myName: {
        type: String
      },
      resetFn: Function,
      userAge: Number
    },
    methods: {
      switchName() {
        return this.myName.split("").reverse().join("");
      },
      resetName() {
        this.myName = 'Max';
        this.$emit('nameWasReset', this.myName);
      }
    }
  }
</script>

<style scoped>
  div {
    background-color: lightcoral;
  }
</style>
```

### UserEdit.vue

```html
<template>
  <div class="component">
    <h3>You may edit the User here</h3>
    <p>Edit me!</p>
    <p>User AGe: {{ userAge }}</p>
    <button @click="editAge">Edit Age</button>
  </div>
</template>

<script>
  export default {
    props: ['userAge'],
    methods: {
      editAge() {
        this.userAge = 30;
        this.$emit('ageWasEdited', this.userAge);
      }
    }
  }
</script>

<style scoped>
  div {
    background-color: lightgreen;
  }
</style>
```

---

## 116: Using an Event Bus for Communication

- First, add the `eventBus` to our **main.js** file

```js
export const eventBus = new Vue();
```

- Then, import it into both of our child components (**userDetail** and **userEdit**)

```js
import { eventBus } from '../main';
```

- Then, change the `$emit` statement by adding `eventBus` in **userEdit**

```js
// emits name of function and data to be passed
eventBus.$emit('ageWasEdited', this.userAge);
```

- Finally, add a `created` section to **UserDetail**

```js
// turns the listener on, recieves data, sets it to userAge
created() {
  eventBus.$on('ageWasEdited', (age) => {
    this.userAge = age;
  });
```

---

## 117: Centralizing Code in an Event Bus

- We can use eventBus as a central storage location for data.

### main.js

We can add methods to the eventBus (and data too)

```js
import Vue from 'vue'
import App from './App.vue'

export const eventBus = new Vue({
  methods: {
    changeAge(age) {
      this.$emit('ageWasEdited', age);
    }
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
```

And we changed the `editAge()` function yet again in UserEdit.vue

```js
editAge() {
  this.userAge = 30;
  // this.$emit('ageWasEdited', this.userAge);
  // eventBus.$emit('ageWasEdited', this.userAge);
  eventBus.changeAge(this.userAge);
}
```

---

## Assignment 7: Component Communication

### App.vue

```html
<template>
  <div class="container">
    <app-header></app-header>
    <hr>
      <app-body></app-body>
    <hr>
    <app-footer></app-footer>
  </div>
</template>

<script>
  import Header from './components/Shared/Header.vue';
  import Footer from './components/Shared/Footer.vue';
  import ServerBody from './components/Server/ServerBody.vue';
  export default {
    components: {
      'app-header': Header,
      'app-footer': Footer,
      'app-body': ServerBody
    }
  }
</script>

<style>

</style>
```

### main.js

```js
import Vue from 'vue'
import App from './App.vue'

export const serverBus = new Vue();

new Vue({
  el: '#app',
  render: h => h(App)
})
```

### ServerBody.vue

```html
<template>
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <ul class="list-group">
        <app-server-list
        v-for="server in servers"
        :server = "server"></app-server-list>
      </ul>
    </div>
    <app-server-details
      :currentDisplayStatus="currentStatus"
      :currentDisplayServer="currentServer"
      ></app-server-details>
  </div>
</template>


<script>
  import ServerDetails from './ServerDetails.vue';
  import ServerList from './ServerList.vue';
  export default {
    data: function() {
      return {
        servers: [
          { id: 1, status: 'Normal' },
          { id: 2, status: 'Critical' },
          { id: 3, status: 'Unknown' },
          { id: 4, status: 'Normal' },
          { id: 5, status: 'Normal' }
        ]
      }
    },
    methods: {

    },
    components: {
      'app-server-details': ServerDetails,
      'app-server-list': ServerList
    }
  }
</script>
```

### ServerList.vue

```html
<template>
  <div>
    <li class="list-group-item" style="cursor: pointer" @click="serverSelected">
        Server #{{ server.id }}
    </li>
  </div>
</template>

<script>
  import { serverBus } from '../../main';
  export default {
    props: ['server'],
    methods: {
      serverSelected() {
        serverBus.$emit('serverSelected', this.server)
      }
    }
  }
</script>
```

### ServerDetails.vue

```html
<template>
  <div class="col-xs-12 col-sm-6">
    <p v-if="!server">Please select a Server</p>
    <p v-else>Server {{ server.id }} selected, Status: {{ server.status }}</p>
    <hr>
    <button @click="resetStatus">Change to Normal</button>
  </div>
</template>

<script>
  import { serverBus } from '../../main';
  export default {
    data: function() {
      return {
        server: null
      }
    },
    methods: {
      resetStatus() {
        this.server.status = 'Normal';
      }
    },
    created() {
      serverBus.$on('serverSelected', (server) => {
        this.server = server;
      })
    }
  }
</script>
```

---

## 118: Component Communication Code

Done

---

## 119: Wrap Up

Done

---

## 120: Module Resources and Useful Links

**Useful Links:**

- [Official Docs - Props](http://vuejs.org/guide/components.html#Prop)s
- [Official Docs - Custom Events](http://vuejs.org/guide/components.html#Custom-Events)
- [Official Docs - Non-Parent-Child Communication](http://vuejs.org/guide/components.html#Non-Parent-Child-Communication)

---
