# Using and Creating Directives

## 163: Module Introduction

There are built-in directives, such as `v-bind` and `v-on`, but we can build custom directives as well.

---

## 164: Understanding Directives

### App.vue (partial)

```html
<div class="row">
  <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
    <h1>Built-in Directives</h1>
    <p v-text="'Some Text'"></p>
    <p v-html="'<strong>Some strong text</strong>'"></p>
  </div>
</div>
```

`v-text` and `v-html` are some examples of built-in directives

---

## 165: How Directives Work - Hook Functions

![image-20191205090110283](./images/image-20191205090110283.png)

- Everything except "el" should be considered read-only.

---

## 166: Creating a Simple Directive

### main.js

```js
import Vue from 'vue'
import App from './App.vue'

Vue.directive('highlight', {
  bind(el, binding, vnode) {
    el.style.backgroundColor = 'green';
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
```

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Built-in Directives</h1>
        <p v-text="'Some Text'"></p>
        <p v-html="'<strong>Some strong text</strong>'"></p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Custom Directives</h1>
        <p v-highlight>Color This</p>
        <p></p>
      </div>
    </div>
  </div>
</template>
```

![image-20191205090619396](./images/image-20191205090619396.png)

---

## 167: Passing Values to Custom Directives

Change main.js to this:

```js
Vue.directive('highlight', {
  bind(el, binding, vnode) {
    el.style.backgroundColor = binding.value;
  }
});
```

and App.vue to this:

```html
<p v-highlight="'red'">Color This</p>
```

This allows us to pass any value to the new directive

---

## 168: Passing Arguments to Custom Directives

We change our directive in main.js

```js
Vue.directive('highlight', {
  bind(el, binding, vnode) {
    if (binding.arg == 'background') {
      el.style.backgroundColor = binding.value;
    } else {
      el.style.color = binding.value;
    }
  }
});  
```

In our App.js, this will make the background red:

```html
<p v-highlight:background="'red'">Color This</p>
```

while this will make the text red:

```html
<p v-highlight="'red'">Color This</p>
```

---

## 169: Modifying a Custom Directive with Modifiers

The new directive in main.js:

```js
Vue.directive('highlight', {
  bind(el, binding, vnode) {
    var delay = 0;
    if (binding.modifiers['delayed']) {
      delay = 3000;
    }
    setTimeout(() => {
      if (binding.arg == 'background') {
        el.style.backgroundColor = binding.value;
      } else {
        el.style.color = binding.value;
      }
    },delay);
  }
});
```

and the new text in App.js:

```html
<p v-highlight:background.delayed="'red'">Color This</p>
```

If we remove `.delayed`, it will be colored immediately.

---

## 170: Custom Directives - A Summary

Done.

---

## 171: Registering Directives Locally

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Built-in Directives</h1>
        <p v-text="'Some Text'"></p>
        <p v-html="'<strong>Some strong text</strong>'"></p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Custom Directives</h1>
        <p v-highlight:background.delayed="'red'">Color This</p>
        <!-- above is from main.js, below is local -->
        <p v-local-highlight:background.delayed="'red'">Color This too</p>
        <p></p>
      </div>
    </div>
  </div>
</template>

<script>
  {/* added directive below to make it local */}
  export default {
    directives: {
      'localHighlight': {
        bind(el, binding, vnode) {
          var delay = 0;
          if (binding.modifiers['delayed']) {
            delay = 3000;
          }
          setTimeout(() => {
            if (binding.arg == 'background') {
              el.style.backgroundColor = binding.value;
            } else {
              el.style.color = binding.value;
            }
          },delay);
        }
      }
    }
  }
</script>
```

---

## 172: Using Multiple Modifiers

### App.vue

Just added a big if-else statement to create changing colors.

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Built-in Directives</h1>
        <p v-text="'Some Text'"></p>
        <p v-html="'<strong>Some strong text</strong>'"></p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Custom Directives</h1>
        <p v-highlight:background.delayed="'red'">Color This</p>
        <p v-local-highlight:background.delayed.blink="'red'">Color This too</p>
        <p></p>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    directives: {
      'localHighlight': {
        bind(el, binding, vnode) {
          var delay = 0;
          if (binding.modifiers['delayed']) {
            delay = 3000;
          }
          if (binding.modifiers['blink']) {
            let mainColor = binding.value;
            let secondColor = 'blue';
            let currentColor = mainColor;
            setTimeout(() => {
              setInterval(() => {
                currentColor == secondColor ? currentColor = mainColor : currentColor = secondColor;
                if (binding.arg == 'background') {
                  el.style.backgroundColor = currentColor;
                } else {
                  el.style.color = currentColor;
                }
              },1000);
            },delay);
          } else {
            setTimeout(() => {
              if (binding.arg == 'background') {
                el.style.backgroundColor = binding.value;
              } else {
                el.style.color = binding.value;
              }
            },delay);
          }
        }
      }
    }
  }
</script>
```

---

## 173: Passing More Complex Values to Directives

Now we pass in an object:

```html
<p
   v-local-highlight:background.delayed.blink=
   "{mainColor: 'red', secondColor: 'green', delay: 500}"
>
Color This too
</p>
```

and bind them as `binding.value.mainColor`, etc

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Built-in Directives</h1>
        <p v-text="'Some Text'"></p>
        <p v-html="'<strong>Some strong text</strong>'"></p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Custom Directives</h1>
        <p v-highlight:background.delayed="'red'">Color This</p>
        <p v-local-highlight:background.delayed.blink="{mainColor: 'red', secondColor: 'green', delay: 500}">Color This too</p>
        <p></p>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    directives: {
      'localHighlight': {
        bind(el, binding, vnode) {
          var delay = 0;
          if (binding.modifiers['delayed']) {
            delay = 3000;
          }
          if (binding.modifiers['blink']) {
            let mainColor = binding.value.mainColor;
            let secondColor = binding.value.secondColor;
            let currentColor = mainColor;
            setTimeout(() => {
              setInterval(() => {
                currentColor == secondColor ? currentColor = mainColor : currentColor = secondColor;
                if (binding.arg == 'background') {
                  el.style.backgroundColor = currentColor;
                } else {
                  el.style.color = currentColor;
                }
              },binding.value.delay);
            },delay);
          } else {
            setTimeout(() => {
              if (binding.arg == 'background') {
                el.style.backgroundColor = binding.value.mainColor;
              } else {
                el.style.color = binding.value.mainColor;
              }
            },delay);
          }
        }
      }
    }
  }
</script>
```

![video](./images/Lesson173.gif)

---

## Assignment 10: Directives

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Directives Exercise</h1>
        <!-- Exercise -->
        <!-- Build a Custom Directive which works like v-on (Listen for Events) -->
        <button v-customOn:click="clicked" class="btn btn-primary">Click Me</button>
        <hr>
        <div
            style="width: 100px; height: 100px; background-color: lightgreen"
            v-customOn:mouseenter="mouseEnter"
            v-customOn:mouseleave="mouseLeave"></div>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    directives: {
      customOn: {
        bind(el, binding) {
          const type = binding.arg;
          const fn = binding.value;
          el.addEventListener(type, fn);
        }
      }
    },
    methods: {
      clicked() {
        alert('I was clicked!');
      },
      mouseEnter() {
        console.log('Mouse entered!');
      },
      mouseLeave() {
        console.log('Mouse left!');
      }
    }
  }
</script>
```

---

## 174: Directives (Code)

Done

---

## 175: Wrap Up

Done

---

## 176: Module Resources and Useful Links

### Useful Links

- Official Docs - [Custom Directives](http://vuejs.org/guide/custom-directive.html)

---
