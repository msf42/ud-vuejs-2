# Improving Your App with Filters and Mixins

## 177: Module Introduction

Done

---

## 178: Creating a Local Filter

For the local filter, list it in `filters`, then use `{{text | toUppercase}}`

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Filters & Mixins</h1>
        <p>{{text | toUppercase}}</p>

      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        text: 'Hello there!'
      }
    },
    filters: {
      'toUppercase'(value) {
        return value.toUpperCase();
      }
    }
  }
</script>

<style>

</style>
```

---

## 179: Global Filters and How to Chain Multiple Filters

Just added this to main.js:

```js
Vue.filter('to-lowercase', function(value) {
  return value.toLowerCase();
});
```

We can also chain filters together:

```html
<p>{{text | toUppercase | to-lowercase}}</p>
```

---

## 180: An (often-times better) Alternative to Filters: Computed Properties

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Filters & Mixins</h1>
        <p>{{text | toUppercase | to-lowercase}}</p>
        <hr>
        <input v-model="filterText">
        <ul>
          <li v-for="fruit in filteredFruits">{{ fruit }}</li>
        </ul>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        text: 'Hello there!',
        fruits: ['Apple', 'Banana', 'Mango', 'Melon'],
        filterText: ''
      }
    },
    filters: {
      'toUppercase'(value) {
        return value.toUpperCase();
      }
    },
    computed: {
      filteredFruits() {
        return this.fruits.filter((element) => {
          return element.match(this.filterText);
        });
      }
    }
  }
</script>
```

---

## 181: Understanding Mixins

For now, code is duplicated, but this will be fixed in the next lesson.

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Filters & Mixins</h1>
        <p>{{text | toUppercase | to-lowercase}}</p>
        <hr>
        <input v-model="filterText">
        <ul>
          <li v-for="fruit in filteredFruits">{{ fruit }}</li>
        </ul>
        <hr>
        <app-list></app-list>
      </div>
    </div>
  </div>
</template>

<script>
  import List from './List.vue';

  export default {
    data() {
      return {
        text: 'Hello there!',
        fruits: ['Apple', 'Banana', 'Mango', 'Melon'],
        filterText: ''
      }
    },
    filters: {
      'toUppercase'(value) {
        return value.toUpperCase();
      }
    },
    computed: {
      filteredFruits() {
        return this.fruits.filter((element) => {
          return element.match(this.filterText);
        });
      }
    },
    components: {
      appList: List
    }
  }
</script>
```

### List.vue

```html
<template>
  <div>
    <h1>Filters & Mixins</h1>
    <input v-model="filterText">
    <ul>
      <li v-for="fruit in filteredFruits">{{ fruit }}</li>
    </ul>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        fruits: ['Apple', 'Banana', 'Mango', 'Melon'],
        filterText: ''
      }
    },
    computed: {
      filteredFruits() {
        return this.fruits.filter((element) => {
          return element.match(this.filterText);
        });
      }
    }
  }
</script>
```

![image-20191211152854076](./images/image-20191211152854076.png)

---

## 182: Creating and Using Mixins

- When a mixin is imported, VueJS merges the sections (data, computed, etc) as necessary.

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Filters & Mixins</h1>
        <p>{{text | toUppercase | to-lowercase}}</p>
        <hr>
        <input v-model="filterText">
        <ul>
          <li v-for="fruit in filteredFruits">{{ fruit }}</li>
        </ul>
        <hr>
        <app-list></app-list>
      </div>
    </div>
  </div>
</template>

<script>
  import List from './List.vue';
  import { fruitMixin } from './fruitMixin';

  export default {
    mixins: [fruitMixin],
    data() {
      return {
        text: 'Hello there!'
      }
    },
    filters: {
      'toUppercase'(value) {
        return value.toUpperCase();
      }
    },
    components: {
      appList: List
    }
  }
</script>
```

### List.vue

```html
<template>
  <div>
    <h1>Filters & Mixins</h1>
    <input v-model="filterText">
    <ul>
      <li v-for="fruit in filteredFruits">{{ fruit }}</li>
    </ul>
  </div>
</template>

<script>
  import { fruitMixin } from './fruitMixin';
  
  export default {
    mixins: [fruitMixin]
  }
</script>
```

### fruitMixin.js

```js
export const fruitMixin = {
  data() {
    return {
      fruits: ['Apple', 'Banana', 'Mango', 'Melon'],
      filterText: ''
    }
    },
    computed: {
    filteredFruits() {
      return this.fruits.filter((element) => {
      return element.match(this.filterText);
      });
    }
  }
}
```

---

## 183: How Mixins Get Merged

Added a `created()` section to the mixin to log "created" and one to the List.vue file to return "Inside List created". The log was as follows:

- Created
- Created
- Inside List Created

Vue imports the mixins first, then gives the local component the last say - it can override changes introduced by the mixin.

---

## 184: Creating a Global Mixin (Special Case!)

- A global mixin is added to every component in your project.
- Generally not used in production
- Perhaps used in plugins

### Results:

- Global Mixin - Created Hook (from the new Vue instance in main.js)
- Global Mixin - Created Hook (from App.vue)
- Created (App.vue also has fruitMixin)
- Global Mixin - Created Hook (list component)
- Created (fruitMixin)
- Inside List Created (local)

---

## 185: Mixins and Scope

- Each component that imports the mixin gets its own copy of it. Here, the button only adds "Berries" to the first list.

![image-20191212151224350](./images/image-20191212151224350.png)

### Final Code

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Filters & Mixins</h1>
        <p>{{text | toUppercase | to-lowercase}}</p>
        <hr>
        <button @click="fruits.push('Berries')">Add New Item</button>
        <input v-model="filterText">
        <ul>
          <li v-for="fruit in filteredFruits">{{ fruit }}</li>
        </ul>
        <hr>
        <app-list></app-list>
      </div>
    </div>
  </div>
</template>

<script>
  import List from './List.vue';
  import { fruitMixin } from './fruitMixin';

  export default {
    mixins: [fruitMixin],
    data() {
      return {
        text: 'Hello there!'
      }
    },
    filters: {
      'toUppercase'(value) {
        return value.toUpperCase();
      }
    },
    components: {
      appList: List
    }
  }
</script>
```

### List.vue

```html
<template>
  <div>
    <h1>Filters & Mixins</h1>
    <input v-model="filterText">
    <ul>
      <li v-for="fruit in filteredFruits">{{ fruit }}</li>
    </ul>
  </div>
</template>

<script>
  import { fruitMixin } from './fruitMixin';
  
  export default {
    mixins: [fruitMixin],
    created() {
      console.log('Inside List Created Hook')
    }
  }
</script>
```

### fruitMixin.js

```js
export const fruitMixin = {
  data() {
    return {
      fruits: ['Apple', 'Banana', 'Mango', 'Melon'],
      filterText: ''
    }
    },
    computed: {
    filteredFruits() {
      return this.fruits.filter((element) => {
      return element.match(this.filterText);
      });
    }
  },
  created() {
    console.log('Created')
  }
}
```

### main.js

```js
import Vue from 'vue'
import App from './App.vue'

Vue.filter('to-lowercase', function(value) {
  return value.toLowerCase();
});

Vue.mixin({
  created() {
    console.log('Global Mixin - Created Hook');
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
```

![Video](./images/Lesson185.gif)

---

## Assignment 11: Filters and Mixins

![image-20191212154445528](./images/image-20191212154445528.png)

---

## 186: Time to Practice: Filters and Mixins

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Filters & Mixins</h1>
        <p>Exercise 01: {{ text | reverseMe }}</p>
        <!-- Exercise 1) -->
        <!-- Build a local Filter which reverses the Text it is applied on -->
        <p>Exercise 02: {{ text | addLength }}</p>
        <!-- Exercise 2 -->
        <!-- Build a global Filter which counts the length of a word and it appends it -->
        <!-- Like this: "Test" => Gets Filtered to => "Test (4)" -->
        <p>Exercise 03a: {{ reverseWord }}</p>
        <p>Exercise 03b: {{ addWordLength }}</p>
        <!-- Exercise 3 -->
        <!-- Do the same as in Exercises 1 & 2, now with Computed Properties -->
        <p>Exercise 04: {{ addWordLengthMixin }}</p>
        <!-- Exercise 4 -->
        <!-- Share the Computed Property rebuilding Exercise 2 via a Mixin -->
      </div>
    </div>
  </div>
</template>

<script>
  import { textMixin } from './textMixin';
  export default {
    mixins: [textMixin],
    data() {
      return {
        text: 'Hello'
      }
    },
    filters: {
      'reverseMe'(value) {
        return value.split("").reverse().join("");
      }
    },
    computed: {
      reverseWord() {
        return this.text.split("").reverse().join("");
      },
      addWordLength() {
        return this.text + " (" + this.text.length + ")";
      }
    }
  }
</script>
```

### main.js

```js
import Vue from 'vue'
import App from './App.vue'

Vue.filter('to-lowercase', function(value) {
  return value.toLowerCase();
});

Vue.filter('addLength', function(value) {
  return value + " (" + value.length + ")";
});

Vue.mixin({
  created() {
    console.log('Global Mixin - Created Hook');
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
```

### textMixin.js

```js
export const textMixin = {
  data() {
    return {
      text: 'Hello'
    }
  },
  computed: {
    addWordLengthMixin() {
      return this.text + " (" + this.text.length + ")";
    }
  }
}
```

---

## 187: Wrap Up

Done

---

## 188: Module Resources and Useful Links

### Useful Links:

- Official Docs - [Filters](https://vuejs.org/v2/guide/filters.html)
- Official Docs - [Mixins](http://vuejs.org/guide/mixins.html)

---
