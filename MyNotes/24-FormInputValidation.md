# Bonus: Form Input Validation

## 371: About This Section

Done

---

## 372: Module Introduction

Done

---

## 373: Add a Firebase Project

Done in last module

---

## 374: Installing Vuelidate

`npm install --save vuelidate`

---

## 375: Adding a Validator

- added Vuelidate to main.js

### Signup.vue

```html
<template>
  <div id="signup">
    <div class="signup-form">
      <form @submit.prevent="onSubmit">
        <div class="input">
          <label for="email">Mail</label>
          <input
                  type="email"
                  id="email"
                  @input="$v.email.$touch()"
                  v-model="email">
                  <div>{{ $v }}</div>
        </div>
        <div class="input">
          <label for="age">Your Age</label>
          <input
                  type="number"
                  id="age"
                  v-model.number="age">
        </div>
        <div class="input">
          <label for="password">Password</label>
          <input
                  type="password"
                  id="password"
                  v-model="password">
        </div>
        <div class="input">
          <label for="confirm-password">Confirm Password</label>
          <input
                  type="password"
                  id="confirm-password"
                  v-model="confirmPassword">
        </div>
        <div class="input">
          <label for="country">Country</label>
          <select id="country" v-model="country">
            <option value="usa">USA</option>
            <option value="india">India</option>
            <option value="uk">UK</option>
            <option value="germany">Germany</option>
          </select>
        </div>
        <div class="hobbies">
          <h3>Add some Hobbies</h3>
          <button @click="onAddHobby" type="button">Add Hobby</button>
          <div class="hobby-list">
            <div
                    class="input"
                    v-for="(hobbyInput, index) in hobbyInputs"
                    :key="hobbyInput.id">
              <label :for="hobbyInput.id">Hobby #{{ index }}</label>
              <input
                      type="text"
                      :id="hobbyInput.id"
                      v-model="hobbyInput.value">
              <button @click="onDeleteHobby(hobbyInput.id)" type="button">X</button>
            </div>
          </div>
        </div>
        <div class="input inline">
          <input type="checkbox" id="terms" v-model="terms">
          <label for="terms">Accept Terms of Use</label>
        </div>
        <div class="submit">
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  </div>
</template>

<script>
  import { required, email } from 'vuelidate/lib/validators'

  export default {
    data () {
      return {
        email: '',
        age: null,
        password: '',
        confirmPassword: '',
        country: 'usa',
        hobbyInputs: [],
        terms: false
      }
    },
// this is only allowed by Vuelidate
    validations: {
      email: {
        required,
        email
      }
    },
    methods: {
      onAddHobby () {
        const newHobby = {
          id: Math.random() * Math.random() * 1000,
          value: ''
        }
        this.hobbyInputs.push(newHobby)
      },
      onDeleteHobby (id) {
        this.hobbyInputs = this.hobbyInputs.filter(hobby => hobby.id !== id)
      },
      onSubmit () {
        const formData = {
          email: this.email,
          age: this.age,
          password: this.password,
          confirmPassword: this.confirmPassword,
          country: this.country,
          hobbies: this.hobbyInputs.map(hobby => hobby.value),
          terms: this.terms
        }
        console.log(formData)
        this.$store.dispatch('signup', formData)
      }
    }
  }
</script>
```

---

## 376: Adding Validation UI Feedback

### signup.vue

- updated this section

```js
<div class="input" :class="{ invalid: $v.email.$error}">
  <label for="email">Mail</label>
  <input
          type="email"
          id="email"
          @input="$v.email.$touch()"
          v-model="email">
  <p v-if="!$v.email.email">Please provide a valid email address</p>
  <p v-if="!$v.email.required">This field must not be empty</p>
</div>
```

---

## 377: Controlling Styles for Invalid Entries

- replaced `@input` with `@blur`, so that error only shows when focus is taken away
- can also pin this to a button instead

---

## 378: More Validators

- imported more validators

```js
import { required, email, numeric, minValue } from 'vuelidate/lib/validators'
```

- using them to validate age

```js
age: {
  required,
  numeric,
  minVal: minValue(18)
}
```

- and putting them to use

```html
<input
        type="number"
        id="age"
        @blur="$v.age.$touch()"
        v-model.number="age">
<p v-if="!$v.age.minVal">You have to be at least {{ $v.age.$params.minVal.min }} years old</p>
```

---

## 379: Validating Passwords for Equality

- imported more validators

```js
import { required, email, numeric, minValue, minLength, sameAs } from 'vuelidate/lib/validators'
```

- using them for passwords

```js
password: {
  required,
  minLen: minLength(6)
},
confirmPassword: {
  // sameAs: sameAs('password')
  sameAs: sameAs(vm => {
    return vm.password
  })
}
```

---

## 380: Using the Required-Unless Validator

- imported requiredUnless

```js
import { required, email, numeric, minValue, minLength, sameAs, requiredUnless } from 'vuelidate/lib/validators'
```

- set up terms

```js
terms: {
  checked(val) {
    return this.country === "germany" ? true : val;
  }
}
```

- and put them to use in the terms checkbo

```html
<div class="input inline" :class="{invalid: $v.terms.$invalid}">
  <input
          type="checkbox"
          id="terms"
          @change="$v.terms.$touch()"
          v-model="terms">
  <label for="terms">Accept Terms of Use</label>
</div>
```

---

## 381: Validating Arrays

- hobby section

```html
<div class="hobbies">
  <h3>Add some Hobbies</h3>
  <button @click="onAddHobby" type="button">Add Hobby</button>
  <div class="hobby-list">
    <div
            class="input"
            v-for="(hobbyInput, index) in hobbyInputs"
            :class="{invalid: $v.hobbyInputs.$each[index].$error}"
            :key="hobbyInput.id">
      <label :for="hobbyInput.id">Hobby #{{ index }}</label>
      <input
              type="text"
              :id="hobbyInput.id"
              @blur="$v.hobbyInputs.$each[index].value.$touch()"
              v-model="hobbyInput.value">
      <button @click="onDeleteHobby(hobbyInput.id)" type="button">X</button>
    </div>
    <p v-if="!$v.hobbyInputs.minLen">You must specify at least {{ $v.hobbyInputs.$params.minLen.min }} hobbies.</p>
    <p v-if="!$v.hobbyInputs.required">Please add hobbies</p>
  </div>
</div>
```

- and validation

```js
hobbyInputs: {
  required,
  minLen: minLength(2),
  $each: {
    value: {
      required,
      minLen: minLength(5)
    }
  }
}
```

---

## 382: Controlling the Form Submit Button

- simple way to disable Submit button until all requirements are met

```html
<button type="submit" :disabled="$v.$invalid">Submit</button>
```

---

## 383: Creating Custom Validators

- set up a custom validator

```js
unique: val => {
  return val !== 'test@test.com'
}
```

---

## 384: Async Validators

- updated Firebase rules to allow email search

```js
{
  /* Visit https://firebase.google.com/docs/database/security to learn more about security rules. */
  "rules": {
    ".read": "true",
    ".write": "auth != null",
      "users" : {
        ".indexOn" : ["email"]
      }
  }
}
```

- our new validator
- checks if any email is equal to our input

```js
email: {
  required,
  email,
  unique: val => {
    if (val === '') return true
    return axios.get('/users.json?orderBy="email"&equalTo="' + val + '"')
    .then(res => {
      return Object.keys(res.data).length === 0
    })
  }
}
```

---

## 385: Wrap Up

Done

---

## 386: Useful Resources & Links

[Vuelidate Docs](https://github.com/vuelidate/vuelidate)

---
