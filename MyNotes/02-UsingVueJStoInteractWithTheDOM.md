# Using VueJS to Interact with the DOM

## 09: Module Introduction

Intro

---

## 10: Understanding VueJS Templates

- Vue creates a template based on our HTML, stores it internally (the Vue Instance), then uses it to create the real HTML code that is rendered in our DOM.

---

## 11: How the VueJS Template Syntax and Instance Work Together

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <p>{{ sayHello() }}</p>
    <!--It is okay to call a a function here, as long as it returns 
      something that can bedisplayed in the DOM.
      We have access to all 'data' and 'methods'-->
  </div>

  <script>
    new Vue({
      el: '#app',
      data: {
        title: 'Hello World!'
      },
      methods: {
        sayHello: function() {
          return 'Hello!';
        }
      }
    })
  </script>
</body>
</html>
```

---

## 12: Accessing Data in the Vue Instance

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <p>{{ sayHello() }}</p>
  </div>

  <script>
    new Vue({
      el: '#app',
      data: {
        title: 'Hello World!'
      },
      methods: {
        sayHello: function() {
          return this.title;
          {*/ We can use 'this' anywhere in the Vue instance */}
        }
      }
    })
  </script>
</body>
</html>
```

---

## 13: Binding to Attributes

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <p>{{ sayHello() }} - <a v-bind:href="link">Google</a></p>
  <!--{{ }} can only be used in places where we would normally 
            use text, not inside of HTML elements-->
  </div>

  <script>
    new Vue({
      el: '#app',
      data: {
        title: 'Hello World!',
        link: 'http://google.com'
      },
      methods: {
        sayHello: function() {
          return this.title;
        }
      }
    })
  </script>
</body>
</html>
```

---

## 14: Understanding and Using Directives

- **Directives** are instructions built in to the code, such as `v-bind:href` above.

---

## 15: Disable Re-Rendering with `v-once`

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <h1 v-once>{{ title }}</h1>
    <p>{{ sayHello() }} - <a v-bind:href="link">Google</a></p>
  </div>

  <script>
    new Vue({
      el: '#app',
      data: {
        title: 'Hello World!',
        link: 'http://google.com'
      },
      methods: {
        sayHello: function() {
          this.title = 'Hello!'
          return this.title;
        }
      }
    })
  </script>
</body>
</html>
```

- Use of **`v-once`** prevents the `<h1>` from re-rendering when the value of title is changed. Otherwise, the title would simply say "Hello!".

![image-20191102143135593](./images/image-20191102143135593.png)

---

## 16: How to Output Raw HTML

### Use `v-html`

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <h1 v-once>{{ title }}</h1>
    <p>{{ sayHello() }} - <a v-bind:href="link">Google</a></p>
    <hr>
    <p v-html="finishedLink"></p>
    <!-- v-html allows raw html to be rendered -->
  </div>

  <script>
    new Vue({
      el: '#app',
      data: {
        title: 'Hello World!',
        link: 'http://google.com',
        finishedLink: '<a href="http://google.com">Google</a>'
      },
      methods: {
        sayHello: function() {
          this.title = 'Hello!'
          return this.title;
        }
      }
    })
  </script>
</body>
</html>
```

---

## 17: Optional: Assignment Starting Code

---

## Assignment 1: Time to Practice - Outputting Data to Templates

### Answer

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="exercise">
    <!-- 1) Fill the <p> below with your Name and Age - using Interpolation -->
      <p>VueJS is pretty cool - {{ name }} ({{ age }})</p>

      <!-- 2) Output your age, multiplied by 3 -->
      <p>AGE * 3 = {{ ageX3() }}</p>

      <!-- 3) Call a function to output a random float between 0 and 1 (Math.random()) -->
      <p>RANDOM NUMBER BETWEEN 0 AND 1: {{ randomNum() }}</p>

      <!-- 4) Search any image on Google and output it here by binding the "src" attribute -->
      <div>
        <img src="https://upload.wikimedia.org/wikipedia/commons/6/6e/Joseph_Banks_1773_Reynolds.jpg" style="width:100px;height:100px">
      </div>

      <!-- 5) Pre-Populate this input with your name (set the "value" attribute) -->
      <div>
        <input type="text" v-bind:value="name">
      </div>
    </div>

  <script>
    new Vue({
      el: '#exercise',
      data: {
        name: 'Steve',
        age: '43'
      },
      methods: {
        ageX3: function() {
          return this.age * 3;
        },
        randomNum: function() {
          return Math.random();
        }
      }
    })
  </script>
</body>
</html>
```

### Screenshot

![image-20191103140951474](./images/image-20191103140951474.png)

---

## 18: Listening to Events

### index2.html

```html
<!--v-bind allows us to pass data to something in our template
v-on allows us to listen for information -->

<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <button v-on:click="increase">Click Me!</button>
    <!--We can use any DOM event here, e.g. mouseover, keypress, etc -->
    <p>{{ counter }}</p>
  </div>
  <script>
    new Vue({
      el: '#app',
      data: {
        counter: 0
      },
      methods: {
        increase: function() {
          this.counter++;
        }
      }
    })
  </script>
</body>
</html>
```

---

## 19: Getting Event Data from the Event Object

### index2.html

#### updates coordinates as mouse moves

```html
<!-- the 'event' for updateCoordinates method is 'click', or whatever is after v-on -->
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <button v-on:click="increase">Click Me!</button>
    <p>{{ counter }}</p>
    <p v-on:mousemove="updateCoordinates">Coordinates {{ x }} / {{ y }}</p>
  </div>
  <script>
    new Vue({
      el: '#app',
      data: {
        counter: 0,
        x: 0,
        y: 0
      },
      methods: {
        increase: function() {
          this.counter++;
        },
        updateCoordinates: function(event) {
          this.x = event.clientX;  {/* these are built-in JS, not Vue */}
          this.y = event.clientY;
        }
      }
    })
  </script>
</body>
</html>
```

---

## 20: Passing Your Own Arguments with Events

### index2.html

#### added `step`

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <button v-on:click="increase(2, $event)">Click Me!</button>
    <!-- we simply add arguments to the function -->
    <!-- $event isn't used in this one, but it can be passed in if we want to use it -->
    <p>{{ counter }}</p>
    <p v-on:mousemove="updateCoordinates">Coordinates {{ x }} / {{ y }}</p>
  </div>

  <script>
    new Vue({
      el: '#app',
      data: {
        counter: 0,
        x: 0,
        y: 0
      },
      methods: {
        increase: function(step, event) {
          this.counter += step;
        },
        updateCoordinates: function(event) {
          this.x = event.clientX;
          this.y = event.clientY;
        }
      }
    })
  </script>
</body>
</html>
```

---

## 21: Modifying an Event - with Event Modifiers

[Available Modifiers](https://vuejs.org/v2/guide/events.html#Event-Modifiers)

### index2.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <button v-on:click="increase(2, $event)">Click Me!</button>
    <p>{{ counter }}</p>
    <p v-on:mousemove="updateCoordinates">
      Coordinates {{ x }} / {{ y }}
      - <span v-on:mousemove.stop="">DEAD SPOT</span>
      <!-- since this is within the <p> calling mousemove, we can use 'stop' here -->
      <!-- it allows us to modify the behavior of this event --"event modifier" -->
    </p>
  </div>

  <script>
    new Vue({
      el: '#app',
      data: {
        counter: 0,
        x: 0,
        y: 0
      },
      methods: {
        increase: function(step, event) {
          this.counter += step;
        },
        updateCoordinates: function(event) {
          this.x = event.clientX;
          this.y = event.clientY;
        }
      }
    })
  </script>
</body>
</html>
```

---

## 22: Listening to Keyboard Events

[Available Key Modifiers](https://vuejs.org/v2/guide/events.html#Key-Modifiers)

### index2.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <button v-on:click="increase(2, $event)">Click Me!</button>
    <p>{{ counter }}</p>
    <p v-on:mousemove="updateCoordinates">
      Coordinates {{ x }} / {{ y }}
      - <span v-on:mousemove.stop="">DEAD SPOT</span>
    </p>
    <input type="text" v-on:keyup.enter.space="alertMe">
    <!-- only creates alert with enter or space keyup -->
  </div>

  <script>
    new Vue({
      el: '#app',
      data: {
        counter: 0,
        x: 0,
        y: 0
      },
      methods: {
        increase: function(step, event) {
          this.counter += step;
        },
        updateCoordinates: function(event) {
          this.x = event.clientX;
          this.y = event.clientY;
        },
        alertMe: function() {
          alert('Alert!');
        }
      }
    })
  </script>
</body>
</html>
```

---

## Assignment 2: Time to Practice - Events

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="exercise">
    <!-- 1) Show an alert when the Button gets clicked -->
    <div>
      <button v-on:click="buttonAlert()">Show Alert</button>
    </div>

    <!-- 2) Listen to the "keydown" event and store the value in a data property (hint: event.target.value gives you the value) -->
    <div>
      <input type="text" v-on:keydown="changeValue1()">
      <p>{{ value1 }}</p>
    </div>

    <!-- 3) Adjust the example from 2) to only fire if the "key down" is the ENTER key -->
    <div>
      <input type="text" v-on:keydown.enter="changeValue2()">
      <p>{{ value2 }}</p>
    </div>
</div>

  <script>
    new Vue({
      el: '#exercise',
      data: {
        value1: '',
        value2: ''
      },
      methods: {
        buttonAlert: function() {
          alert('Button was clicked!');
        },
        changeValue1: function() {
          this.value1 = event.target.value;
        },
        changeValue2: function() {
          this.value2 = event.target.value;
        }
      }
    });
  </script>
</body>
</html>
```

![image-20191104092051539](./images/image-20191104092051539.png)

### A slightly more efficient way

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="exercise">
    <!-- 1) Show an alert when the Button gets clicked -->
    <div>
      <button v-on:click="buttonAlert()">Show Alert</button>
    </div>
    <!-- 2) Listen to the "keydown" event and store the value in a data property (hint: event.target.value gives you the value) -->
    <div>
    <!-- here we change the value directly in the tag instead of a function below -->
      <input type="text" v-on:keydown="value1 = $event.target.value">
      <p>{{ value1 }}</p>
    </div>
    <!-- 3) Adjust the example from 2) to only fire if the "key down" is the ENTER key -->
    <div>
      <input type="text" v-on:keydown.enter="value2 = $event.target.value">
      <p>{{ value2 }}</p>
    </div>
</div>

  <script>
    new Vue({
      el: '#exercise',
      data: {
        value1: '',
        value2: ''
      },
      methods: {
        buttonAlert: function() {
          alert('Button was clicked!');
        }
      }
    });
  </script>
</body>
</html>
```

---

## 23: Writing JavaScript Code in the Templates

### index2.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <button v-on:click="increase(2, $event)">Click Me!</button>
    <!-- We can write simple JS any time we are accessing the Vue element, such as counter++, or even ternary functions -->
    <button v-on:click="counter++">Click Me!</button>
    <p>{{ counter * 2 > 10 ? 'Greater than 10' : 'Smaller than 10' }}</p>
    <p v-on:mousemove="updateCoordinates">
      Coordinates {{ x }} / {{ y }}
      - <span v-on:mousemove.stop="">DEAD SPOT</span>
    </p>
    <input type="text" v-on:keyup.enter.space="alertMe">
  </div>

  <script>
    new Vue({
      el: '#app',
      data: {
        counter: 0,
        x: 0,
        y: 0
      },
      methods: {
        increase: function(step, event) {
          this.counter += step;
        },
        updateCoordinates: function(event) {
          this.x = event.clientX;
          this.y = event.clientY;
        },
        alertMe: function() {
          alert('Alert!');
        }
      }
    });
  </script>
</body>
</html>
```

---

## 24: Using Two-Way-Binding

### index3.html

```html
<!-- v-model provides 2-way data binding -->
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <input type="text" v-model="name">
    <p>{{ name }}</p>
  </div>

  <script>
    new Vue({
      el: '#app',
      data: {
        name: 'Max'
      }
    });
  </script>
</body>
</html>
```

![image-20191104103727042](./images/image-20191104103727042.png)

---

## 25: Reacting to Changes with Computed Properties

### Known Options for Vue Instance

- **el**: Connect to the DOM
- **data**: Store data to be used
- **methods**: Methods of the Vue instance
- **computed**: Dependent properties

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <button v-on:click="counter++">Increase</button>
    <button v-on:click="counter--">Decrease</button>
    <button v-on:click="secondCounter++">Increase Second</button>
    <p>Counter: {{ counter }} | {{ secondCounter }}</p>
    <p>Result: {{ result() }} | {{ output }}</p>
  </div>

  <script>
    new Vue({
      el: '#app',
      // data is not reactive, we can not put calculations or functions here
      data: {
        counter: 0,
        secondCounter: 0
      },
      /*  computed items are calculated and stored;   everything stored in computed can be used just like anything stored in the data object */
      computed: {
        output: function() {
          console.log('Computed');
          return this.counter > 5 ? 'Greater than 5' : 'Smaller than 5'
        }
      },
      // methods get run every time the DOM is refreshed
      methods: {
        result: function() {
          console.log('Method')
          return this.counter > 5 ? 'Greater than 5' : 'Smaller than 5'
        }
      }
    });
    <!-- Methods get run every time the DOM is refreshed. Computed only gets run once. -->
    <!-- For Computed properties, Vue analyzes the code to see whether it needs to be run again
        For Methods, Vue doesn't check first -->
  </script>
</body>
</html>
```

![image](./images/image-20191104111014182.png)

---

## 26: An Alternative to Computed Properties: Watching for Changes

### Known Options for Vue Instance

- **el**: Connect to the DOM
- **data**: Store data to be used
- **methods**: Methods of the Vue instance
- **computed**: Dependent properties
- **watch**: Execute code upon data changes

It is considered optimal to use **computed*- when possible. But they must always be run synchronously.

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <button v-on:click="counter++">Increase</button>
    <button v-on:click="counter--">Decrease</button>
    <button v-on:click="secondCounter++">Increase Second</button>
    <p>Counter: {{ counter }} | {{ secondCounter }}</p>
    <p>Result: {{ result() }} | {{ output }}</p>
  </div>

  <script>
    new Vue({
      el: '#app',
      // data is not reactive, we can not put calculations or functions here
      data: {
        counter: 0,
        secondCounter: 0
      },
      // computed properties can not be asynchronous; use 'watch' for these
      computed: {
        output: function() {
          console.log('Computed');
          return this.counter > 5 ? 'Greater than 5' : 'Smaller than 5'
        }
      },
      {/  must match one of our properties  /}
      watch: {
        counter: function(value) {
          var vm = this; // since the function is scoped, we have to bind this
          setTimeout(function() {
            vm.counter = 0;
          }, 2000)
        }
      },
      methods: {
        result: function() {
          console.log('Method')
          return this.counter > 5 ? 'Greater than 5' : 'Smaller than 5'
        }
      }
    });
  </script>
</body>
</html>
```

---

## 27: Saving Time with Shorthands

### index4.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <button v-on:click="changeLink">Click to Change Link</button>
    <!-- v-on can be replaced with @ -->
    <a v-bind:href="link">Link</a>
    <!-- v-bind can be dropped completely -->
  </div>

  <script>
    new Vue({
      el: 'app',
      data: {
        link: 'http://google.com'
      },
      methods: {
        changeLink: function() {
          this.link = 'http://apple.com'
        }
      }
    });
  </script>
</body>
</html>
```

---

## Assignment 3: Time to Practice - Reactive Properties

Now using separate files

### index.html

```html
<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="">
  <script src="vue.js"></script>
</head>
<body>
  <div id="exercise">

    <!-- 1) Show a "result" of 'not there yet' as long as "value" is not equal to 37 - you can change "value" with the buttons. Print 'done' once you did it -->
    <div>
      <p>Current Value: {{ value }}</p>
      <button @click="value += 5">Add 5</button>
      <button @click="value += 1">Add 1</button>
      <p>{{ result }}</p>
    </div>

    <!-- 2) Watch for changes in the "result" and reset the "value" after 5 seconds (hint: setTimeout(..., 5000) -->
    <div>
      <input type="text">
      <p>{{ value }}</p>
    </div>
  </div>

  <script src="script.js"></script>
</body>
</html>
```

### script.js

```js
new Vue({
  el: '#exercise',
  data: {
    value: 0
  },
  computed: {
    result: function() {
      return this.value < 37 ? 'Not there yet' : this.value > 37 ? 'Too high' : 'Exactly 37'
    }
  },
  watch: {
    result: function(value) {
      var vm = this;
      setTimeout(function() {
        vm.value = 0;
      }, 5000)
    }
  }
});
```

![Video](./images/Exercise03.gif)

---

## 28: Dynamic Styling with CSS Classes - Basics

### index5.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>

  <div id="app">
    <!-- class of red starts as false; on click it changes to true -->
    <div
      class="demo"
      @click="attachRed = !attachRed"
      :class="{red: attachRed}">
    </div>
    <div class="demo"></div>
    <div class="demo"></div>
  </div>

  <script src="script5.js"></script>
</body>
</html>
```

### style5.css

```css
.demo {
  width: 100px;
  height: 100px;
  background-color: gray;
  display: inline-block;
  margin: 10px;
}

.red {
  background-color: red;
}
.green {
  background-color: green;
}
.blue {
  background-color: blue;
}
```

### script5.js

```js
new Vue({
  el: '#app',
  data: {
    attachRed: false
  }
});
```

---

## 29: Dynamic Styling with CSS Classes - Using Objects

### index5.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <div
      class="demo"
      @click="attachRed = !attachRed"
      :class="divClasses"> <!-- class is computed below -->
    </div>
    <div class="demo" :class="{red: attachRed}"></div>
    <div class="demo"></div>
  </div>

  <script src="script5.js"></script>
</body>
</html>
```

### style5.css (unchanged)

### script5.js

```js
new Vue({
  el: '#app',
  data: {
    attachRed: false
  },
  /* on click, attachRed flips, then class is set to values below */
  computed: {
    divClasses: function() {
      return {
        red: this.attachRed,
        blue: !this.attachRed
      };
    }
  }
});
```

---

## 30: Dynamic Styling with CSS Classes - Using Names

### index5.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <div
      class="demo"
      @click="attachRed = !attachRed"
      :class="divClasses"></div>
    <div class="demo" :class="{red: attachRed}"></div>
    <!-- Here we use an array of values; the first is the CSS class directly, the second is an object, in the form of 'CSS Class: Condition' -->
    <div class="demo" :class="[color, {red: attachRed}]"></div>
    <hr>
    <input type="text" v-model="color">
    <!-- v-model uses 2-way binding, attaching the value of the input box to 'color' -->
  </div>

  <script src="script5.js"></script>
</body>
</html>
```

### script5.js

```js
new Vue({
  el: '#app',
  data: {
    attachRed: false,
    color: 'green'
  },
  computed: {
    divClasses: function() {
      return {
        red: this.attachRed,
        blue: !this.attachRed
      };
    }
  }
});
```

![Video](./images/Lesson30.gif)

---

## 31: Setting Styles Dynamically (without CSS Classes)

### index6.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <div class="demo" :style="{backgroundColor: color}"></div>
    <div class="demo" :style="myStyle"></div> <!--uses myStyle function-->
    <div class="demo"></div>
    <hr>
    <input type="text" v-model="color">
    <input type="text" v-model="width">
  </div>

  <script src="script6.js"></script>
</body>
</html>
```

### script6.js

```js
new Vue({
  el: '#app',
  data: {
    color: 'gray',
    width: 100
  },
  computed: {
    myStyle: function() {
      return {
        backgroundColor: this.color,
        width: this.width + 'px'
      };
    }
  }
});
```

---

## 32: Styling Elements with the Array Syntax

### index6.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <div class="demo" :style="{backgroundColor: color}"></div>
    <div class="demo" :style="myStyle"></div>
    <div class="demo" :style="[myStyle, {height: width + 'px'}]"></div>
    <hr>
    <input type="text" v-model="color">
    <input type="text" v-model="width">
  </div>

  <script src="script6.js"></script>
</body>
</html>
```

![image-20191105151913474](./images/image-20191105151913474.png)

---

## Assignment 4: Time to Practice - Styling

### index.html

```html
<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="styles.css">
  <script src="https://npmcdn.com/vue/dist/vue.js"></script>
</head>
<body>
  <div id="exercise">

    <!-- 1) Start the Effect with the Button. The Effect should alternate the "highlight" or "shrink" class on each new setInterval tick (attach respective class to the div with id "effect" below) -->
    <div>
      <button @click="startEffect">Start Effect</button>
      <div id="effect" :class="{ highlight: highlight, shrink: shrink}"></div>
    </div>

    <!-- 2) Create a couple of CSS classes and attach them via the array syntax -->
    <div :class="[{red: gotClass}, {large: gotClass}]">I got no class :(</div>


    <!-- 3) Let the user enter a class (create some example classes) and attach it -->
    <div>
      <input type="text" v-model="class2">
      <div :class="[class2, {large: gotClass}]">Change Me with a class name</div>
    </div>


    <!-- 4) Let the user enter a class and enter true/ false for another class (create some example classes) 
      and attach the classes -->
    <div>
      <input type="text" v-model="class3">
      <input type="text" v-model="class4">
      <div :class="[class3, {italic: class4}]">Change me too!</div>
    </div>


    <!-- 5) Repeat 3) but now with values for styles (instead of class names). Attach the respective styles.  -->
    <div>
      <input type="text" v-model="fontSize2">
      <div :style="{'font-size': fontSize2 + 'px'}">Change Me with a value for font size</div>
    </div>


    <!-- 6) Create a simple progress bar with setInterval and style bindings. Start it by hitting the below button. -->
    <div>
      <button @click="progressBar">Start Progress</button>
      <div id="progress" :style="[{width:barWidth + 'px'}]"></div>
      <p>{{barWidth}}</p>
    </div>
  </div>


  <script src="app.js"></script>
</body>
</html>
```

### app.js

```js
new Vue({
  el: '#exercise',
  data: {
    highlight: false,
    shrink: false,
    gotClass: true,
    barWidth: 50
  },
  methods: {
    startEffect: function() {
      this.highlight = true;
      var vm = this;
      setInterval(function() {
        vm.highlight = !vm.highlight;
        vm.shrink = !vm.shrink;
      }, 500)
    },
    progressBar: function() {
      this.barWidth = 50;
      var vm = this;
      setInterval(function() {
        if (vm.barWidth < 1000) {
          vm.barWidth += 50;
        }
      }, 500)
    }
  }
});
```

### styles.css

```css
#effect {
  width: 100px;
  height: 100px;
  border: 1px solid black;
}

#progress {
  width: 50px;
  height: 50px;
  border: 1px solid black;
  background-color: blueviolet;
}

.highlight {
  background-color: red;
  width: 200px !important;
}

.shrink {
  background-color: gray;
  width: 50px !important;
}

.red {
  color: red;
  font-style: italic;
}

.blue {
  color: blue;
}

.green {
  color: green;
}

.yellow {
  color: yellow;
}

.large {
  font-size: 4rem;
  font-weight: 900;
}

.italic {
  font-style: italic;
}
```

![Video](./images/Exercise04.gif)

---

## 33: Module Wrap Up

Done

---

## 34: Module Resources & Useful Links

### JSFiddle Links:

- [Getting Started](https://jsfiddle.net/smax/pcjtcmdm/)
- [Template Syntax](https://jsfiddle.net/smax/bkk97b7g/)
- [Events](https://jsfiddle.net/smax/7zdak05g/)
- [Two-Way-Binding](https://jsfiddle.net/smax/ut0tsbcu/)
- [Computed Properties & Watch](https://jsfiddle.net/smax/yLjqxmw0/)
- [Dynamic Classes](https://jsfiddle.net/smax/gowg40ym/)
- [Dynamic Styles](https://jsfiddle.net/smax/3rvdLq5y/)

### Further Links

- [Official Docs - Getting Started](http://vuejs.org/guide/)
- [Official Docs - Template Syntax](http://vuejs.org/guide/syntax.html)
- [Official Docs - Events](http://vuejs.org/guide/events.html)
- [Official Docs - Computed Properties & Watchers](http://vuejs.org/guide/computed.html)
- [Official Docs - Class & Style Binding](http://vuejs.org/guide/class-and-style.html)

---
