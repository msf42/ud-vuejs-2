# An Introduction to Components

## 92: Module Introduction

Done

---

## 93: An Introduction to Components

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <my-cmp></my-cmp>
    <hr>
    <my-cmp></my-cmp>
    <hr>
    <my-cmp></my-cmp>
  </div>

  <script src="app.js"></script>
</body>
</html>
```

### app.js

```js
Vue.component('my-cmp', {
  data: function() {
    return {
      status: 'Critical'
    }
  },
  template: '<p>Server Status: {{ status }}</p>'
});

new Vue({
  el: '#app',
});
```

- Using a component allows us to re-use our Vue code. We move the `data` and `template` to the component. The `data` section must be converted to a function that returns the data.
- When we create a Vue component, it is essentially like creating our own html tag, `<my-cmp>` in this case.

![image-20191119111858202](./images/image-20191119111858202.png)

---

## 94: Storing Data in Components with the Data Method

### app.js

```js
Vue.component('my-cmp', {
  data: function() {
    return {
      status: 'Critical'
    }
  },
  template: `<p>Server Status: {{ status }} (<button @click="changeStatus">Change</button>)</p>`,
  methods: {
    changeStatus: function() {
      this.status = 'Normal';
    }
  }
});

new Vue({
  el: '#app',
});
```

- The function executes independently. Each copy of the component has a button to change the status.

---

## 95: Registering Components Locally and Globally

### app.js

```js
// var data = { status: 'Critical' };


var cmp = {
  data: function() {
    return {
      status: 'Critical'
    }
  },
  template: `<p>Server Status: {{ status }} (<button @click="changeStatus">Change</button>)</p>`,
  methods: {
    changeStatus: function() {
      this.status = 'Normal';
    }
  }
};

new Vue({
  el: '#app',
  components: {
    'my-cmp': cmp {/* The component is registered now */}
  }
});

new Vue({
  el: '#app2',
});
```

### Globally vs Locally

#### Globally

```js
Vue.component('my-cmp', {
  data: function() {
    return {
      status: 'Critical'
    }
  },
  template: `<p>Server Status: {{ status }} (<button @click="changeStatus">Change</button>)</p>`,
  methods: {
    changeStatus: function() {
      this.status = 'Normal';
    }
  }
});
```

#### Locally

```js
var cmp = {                        {/* this line is changed */}
  data: function() {
    return {
      status: 'Critical'
    }
  },
  template: `<p>Server Status: {{ status }} (<button @click="changeStatus">Change</button>)</p>`,
  methods: {
    changeStatus: function() {
      this.status = 'Normal';
    }
  }
};

new Vue({
  el: '#app',
  components: {                       {/* and this is added */}
    'my-cmp': cmp
  }
});
```

---

## 96: The "Root Component" in the App.vue File

```js
new Vue({
  el: '#app',
  render: h => h(App)
})
```

Above, `render` is better than using `template`, because we render the compiled JS code, and don't suffer the limitations of the template given by the DOM.

### app.vue

```html
<template>
  <p>Server Status: {{ status }}</p>
</template>

<script>
  export default {
    data: function () {
      return {
        status: 'Critical'
      }
    }
  }
</script>

<style>
</style>
```

### main.js

```js
import Vue from 'vue'
import App from './App.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})

```

---

## 97: Creating a Component

### index.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Vue Components</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"></head>
  <body>
    <div id="app">
    </div>
    <script src="/dist/build.js"></script>
  </body>
</html>

```

### main.js

```js
import Vue from 'vue'
import App from './App.vue'
import Home from './Home.vue';

Vue.component('app-server-status', Home);

new Vue({
  el: '#app',
  render: h => h(App)
})

```

### App.vue

```html
<template>
  <app-server-status></app-server-status>
</template>

<script></script>

<style></style>
```

### Home.vue

```html
<template>
  <div>
    <p>Server Status: {{ status }}</p>
    <hr>
    <button @click="changeStatus">Change Status</button>
  </div>
</template>

<script>
export default {
  data: function() {
    return {
      status: 'Critical'
    }
  },
  methods: {
    changeStatus() {
      this.status = 'Normal';
    }
  }
}
</script>
```

---

## 98: Using Components

### index.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Vue Components</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"></head>
  <body>
    <div id="app">
    </div>
    <script src="/dist/build.js"></script>
  </body>
</html>
```

- The `id="app"` is how **main.js** changes what is displayed

### main.js

```js
import Vue from 'vue'
import App from './App.vue'
import Home from './Home.vue';

Vue.component('app-servers', Home); // GLOBAL

new Vue({
  el: '#app',
  render: h => h(App)
})

/*
render tells it to render what is in the App.vue
App.vue only contains app-servers element, which is registered globally above

app-servers refers to everything in Home.vue
Home.vue imports ServerStatus.vue and displays it all as app-servers-status
*/
```

- Main.js controls what is displayed in the html page.
  - It imports Home and creates the `app-servers` global component.

### App.vue

```html
<template>
  <app-servers></app-servers>
</template>

<script></script>

<style></style>
```

- App.vue displays the `app-servers` global component

### Home.vue

```html
<template>
  <div>
    <app-server-status v-for="server in 5"></app-server-status>
  </div>
</template>

<script>
  import ServerStatus from './ServerStatus.vue';
  export default {
    components: {
      'app-server-status': ServerStatus
    }
  }
</script>
```

- `Home.vue` imports ServerStatus and creates the component `app-server-status`. It displays 5 copies of it.

### ServerStatus.vue

```html
<template>
  <div>
    <p>Server Status: {{ status }}</p>
    <hr>
    <button @click="changeStatus">Change Status</button>
  </div>
</template>
<!-- our normal template div above and normal data and methods sections below -->
<script>
export default {
  data: function() {
    return {
      status: 'Critical'
    }
  },
  methods: {
    changeStatus() {
      this.status = 'Normal';
    }
  }
}
</script>
```

This creates what we see - the server status and the button.

---

## Assignment 6: Time to Practice - Components

I used only local components. I took the header and footer out of the main app and made them separate files. I took the entire body out as a third file, then took the server list and server status out of it and made them separate files.

Structure:

- main.js
- App.vue
  - Header.vue
  - ServerBody.vue
    - ServerList.vue
    - ServerStatus.vue
  - Footer.vue

![image-20191120121543781](./images/image-20191120121543781.png)

### main.js

```js
import Vue from 'vue'
import App from './App.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})
```

### App.vue

```html
<template>
  <div class="container">
    <app-header></app-header>
    <hr>
      <app-body></app-body>
    <hr>
    <app-footer></app-footer>
  </div>
</template>

<script>
  import Header from './Header.vue';
  import Footer from './Footer.vue';
  import ServerBody from './ServerBody.vue';
  export default {
    components: {
      'app-header': Header,
      'app-footer': Footer,
      'app-body': ServerBody
    }
  }
</script>

<style>

</style>
```

### Header.vue

```html
<template>
  <div class="row">
    <div class="col-xs-12">
      <header>
        <h1>Steve Server Status</h1>
      </header>
    </div>
  </div>
</template>
```

### Footer.vue

```html
<template>
  <div class="row">
    <div class="col-xs-12">
      <footer>
        <p>All Steve Servers are managed here</p>
      </footer>
    </div>
  </div>
</template>
```

### ServerBody.vue

```html
<template>
  <div class="row">
    <app-server-list></app-server-list>
    <app-server-details></app-server-details>
  </div>
</template>

<script>
  import ServerDetails from './ServerDetails.vue';
  import ServerList from './ServerList.vue';
  export default {
    components: {
      'app-server-details': ServerDetails,
      'app-server-list': ServerList
    }
  }
</script>
```

### ServerDetails.vue

```html
<template>
  <div class="col-xs-12 col-sm-6">
    <p>Steve's Server Details are currently not updated</p>
  </div>
</template>

<script>
export default {
  
}
</script>
```

### ServerList.vue

```html
<template>
  <div class="col-xs-12 col-sm-6">
    <ul class="list-group">
      <li
        class="list-group-item"
        v-for="index in 5">
      Steve's Server #{{ index }}
      </li>
    </ul>
  </div>
</template>
```

---

## 99: Time to Practice - Components (Code)

Just the code answer file.

---

## 100: Moving to a Better Folder Structure

For small to medium projects:

- place all components in a subfolder called "components"
- place additional subfolders in 'components', such as 'shared' (for header and footer) and 'server' (for other three)

---

## 101: Alternative Folder Structures

In the last lecture I introduced one possible folder structure. It's a great structure in small and medium sized projects.

Whilst it also might work fine in bigger projects, there's also an alternative you might want to consider.

Instead of having your components in a components/ folder (and storing other shared files in other folders - e.g. shared/), you can also group your files by feature.

This could look like this:

- main.js
- users/
  - account/
  - analytics/
- shop
  - main/
  - checkout/

---

## 102: How to Name Your Component Tags (Selectors)

```html
<script>
  import Header from './components/Shared/Header.vue';
  import Footer from './components/Shared/Footer.vue';
  import Servers from './components/Server/Servers.vue';
  import ServerDetails from './components/Server/ServerDetails.vue';

  export default {
    components: {
      appHeader: Header, /* can use camelCase here, but still app-header in template */
      Servers, /* ES6, we can use one word for both */
      'app-server-details': ServerDetails, /* original ways */
      'app-footer': Footer
    }
  }
</script>
```

---

## 103: Scoping Component Styles

- By default, any style applied will be applied *globally*, like in a global style sheet. We can add

```html
<style scoped>
</style>
```

---

## 104: Module Wrap-up

Done

---

## 105: Module Resources and Useful Links

[VueJS Components](http://vuejs.org/guide/components.html)

Also see: [Component Registration](https://vuejs.org/v2/guide/components-registration.html)

---
