# Second Course Project: Wonderful Quotes

## 137: Module Introduction

Done.

---

## 138: Setting Up the Project

General overview of setup.

---

## 139: Initializing the Application

### Summary

- added data (quotes and maxQuotes) to App.vue
- created Quote.vue and QuoteGrid.vue

### App.vue

```html
<template>
  <div class="container">
  
  </div>
</template>

<script>
  export default {
    data: function() {
      return {
        quotes: [
          'Just a quote to see something'
        ],
        maxQuotes: 10
      }
    }
  }
</script>

<style>
</style>
```

---

## 140: Creating the Application Components

### Summary

- No change in App.vue
- Set up QuoteGrid to receive quotes as props
- Set up Quote to receive info in slots

### Quote.vue

```html
<template>
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="panel panel-default">
      <div class="panel-body quote">
        <slot></slot>
      </div>
    </div>

  </div>
</template>

<script>
</script>

<style scoped>
  .panel-body {
    font-family: "Arizonia", cursive;
    font-size: 24px;
    color: #6e6e6e;
  }

  .quote {
    cursor: pointer;
  }

  .quote:hover {
    background-color: #ffe2e2;
  }
</style>
```

### QuoteGrid.vue

```html
<template>
  <div class="row">

  </div>
</template>

<script>
export default {
  props: ['quotes']
}
</script>

<style scoped>

</style>
```

---

## 141: Passing Data with Props and Slots

### Summary

- No change in Quote.vue
- imported Quote into QuoteGrid, and QuoteGrid into App

### App.vue

```html
<template>
  <div class="container">
    <app-quote-grid :quotes="quotes"></app-quote-grid>
  </div>
</template>

<script>
  import QuoteGrid from './components/QuoteGrid.vue';
  export default {
    data: function() {
      return {
        quotes: [
          'Just a quote to see something'
        ],
        maxQuotes: 10
      }
    },
    /* When we import, we add the component here so we can use it */
    components: {
      appQuoteGrid: QuoteGrid
    }
  }
</script>

<style>
</style>
```

### QuoteGrid.vue

```html
<template>
  <div class="row">
    <app-quote v-for="quote in quotes">{{ quote }}</app-quote>
    <!-- passing quote to the slot in Quote.vue -->
  </div>
</template>

<script>
  import Quote from './Quote.vue';
  export default {
    props: ['quotes'],
    components: {
      appQuote: Quote
    }
  }
</script>

<style scoped>
</style>
```

---

## 142: Allowing Users to Create Quotes with a NewQuote Component

### Summary

- Made NewQuote.vue
  - Made label, text area, and button.
  - Used a lot of Bootstrap
  - Added @click event for button, but not the method yet

### NewQuote.vue

```html
<template>
  <div class="row">
    <form>
      <div class="col-sm-8 col-sm-offset-2 col-xs-12 col-md-6 col-md-offset-3 form-group">
        <label>Quote</label>
        <textarea class="form-control" row="3" v-model="quote"></textarea>
      </div>
      <div class="col-sm-8 col-sm-offset-2 col-xs-12 col-md-6 col-md-offset-3 form-group">
        <button class="btn btn-primary" @click.prevent="createNew">Add Quote</button>
      </div>
    </form>
  </div>
</template>

<script>
  export default {
    data: function() {
      return {
        quote: ''
      }
    },
    methods: {
      createNew() {

      }
    }
  }
</script>

<style scoped>

</style>
```

---

## 143: Adding Quotes with Custom Events

### Summary

- Added `createNew` method to NewQuote.
  - Since text area has 2 way binding via `v-model`, we just emit `quoteAdded, this.quote`, then reset `this.quote` to an empty string
- In App.vue, we add     `<app-new-quote @quoteAdded="newQuote"></app-new-quote>`, which listens for "quoteAdded" and sends it (this.quote from NewQuote) to the newQuote method, which pushes it to the quotes list

### App.vue

```html
<template>
  <div class="container">
    <app-new-quote @quoteAdded="newQuote"></app-new-quote>
    <app-quote-grid :quotes="quotes"></app-quote-grid>
  </div>
</template>

<script>
  import QuoteGrid from './components/QuoteGrid.vue';
  import NewQuote from './components/NewQuote.vue';
  export default {
    data: function() {
      return {
        quotes: [
          'Just a quote to see something'
        ],
        maxQuotes: 10
      }
    },
    methods: {
      newQuote(quote) {
        this.quotes.push(quote);
      }
    },
    components: {
      appQuoteGrid: QuoteGrid,
      appNewQuote: NewQuote
    }
  }
</script>

<style>
</style>
```

### NewQuote.vue

```html
<template>
  <div class="row">
    <form>
      <div class="col-sm-8 col-sm-offset-2 col-xs-12 col-md-6 col-md-offset-3 form-group">
        <label>Quote</label>
        <textarea class="form-control" row="3" v-model="quote"></textarea>
      </div>
      <div class="col-sm-8 col-sm-offset-2 col-xs-12 col-md-6 col-md-offset-3 form-group">
        <button class="btn btn-primary" @click.prevent="createNew">Add Quote</button>
      </div>
    </form>
  </div>
</template>

<script>
  export default {
    data: function() {
      return {
        quote: ''
      }
    },
    methods: {
      createNew() {
        this.$emit('quoteAdded', this.quote);
        this.quote = '';
      }
    }
  }
</script>

<style scoped>

</style>
```

---

## 144: Adding an Info Box

Just added the info box to

### App.vue

```html
<div class="row">
  <div class="col-sm-12 text-center">
    <div class="alert alert-info">
      Info: Click on a Quote to delete it!
    </div>
  </div>
</div>
```

---

## 145: Allowing for Quote Deletion

### Summary

- In QuoteGrid
  - Added index to the v-for loop (line 3)
  - Added @click.native, which activates only when the particular item in the DOM is clicked. Good for using in for loops. Use it to call `deleteQuote(index)`
  - Added deleteQuote method, which emits `quoteDeleted` and index (line 15)
- In App.vue
  - Added listener to `<app-quote-grid>`, @quoteDeleted
    - Used it to call `deleteQuote` function. Not the same one in QuoteGrid, but a new one created here in App.vue (line 31)
  - This `deleteQuote` method runs `this.quotes.splice(index, 1);` (line 32)

---

## 146: Controlling Quotes with a Progress Bar

### Summary

- Adding progress bar - used a lot of Bootstrap
- set width as a calculation
- passed props of quoteCount and maxQuotes

### Header.vue

```html
<template>
  <div class="row">
    <div class="col-sm-12">
      <h3>Quotes Added</h3>
      <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
          :style="{width: (quoteCount/maxQuotes)*100 + '%'}">
          {{ quoteCount }} / {{ maxQuotes }}
        </div>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    props: ['quoteCount', 'maxQuotes']
  }
</script>
```

---

## 147: Finishing Touches and State Management

### Summary

- added ability to delete quote

### App.vue

```html
<template>
  <div class="container">
    <app-header :quoteCount="quotes.length" :maxQuotes="maxQuotes"></app-header>
    <app-new-quote @quoteAdded="newQuote"></app-new-quote>
    <app-quote-grid :quotes="quotes" @quoteDeleted="deleteQuote"></app-quote-grid>
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="alert alert-info">
          Info: Click on a Quote to delete it!
        </div>
      </div>
    </div>
  </div>
</template>

<script>
  import QuoteGrid from './components/QuoteGrid.vue';
  import NewQuote from './components/NewQuote.vue';
  import Header from './components/Header.vue';
  export default {
    data: function() {
      return {
        quotes: [
          'Just a quote to see something'
        ],
        maxQuotes: 10
      }
    },
    methods: {
      newQuote(quote) {
        if (this.quotes.length >= this.maxQuotes) {
          return alert('Please delete quote first')
        }
        this.quotes.push(quote);
      },
      deleteQuote(index) {
        this.quotes.splice(index, 1);
      }
    },
    components: {
      appQuoteGrid: QuoteGrid,
      appNewQuote: NewQuote,
      appHeader: Header
    }
  }
</script>

<style>
</style>
```

---

## 148: Module Resources

Done

---
