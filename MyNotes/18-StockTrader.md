# Final Project - Stock Trader

## 283: Project Introduction

Done

---

## 284: Project Setup and Planning

- added `babel-preset-stage-2`, which allows spread operator

---

## 285: Creating the First Components

### Layout

- components
  - portfolio
    - Portfolio.vue
    - Stock.vue
  - stocks
    - Stock.vue
    - Stocks.vue
  - Header.vue
  - Home.vue

---

## 286: Setup Project Routes

- added `vue-router`

### main.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'

import App from './App.vue'
import { routes } from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})

```

### routes.js

```js
import Home from './components/Home.vue';
import Portfolio from './components/portfolio/Portfolio.vue';
import Stocks from './components/stocks/Stocks.vue';

export const routes = [
  { path: '/', component: Home },
  { path: '/portfolio', component: Portfolio },
  { path: '/stocks', component: Stocks }
]
```

---

## 287: Adding a Header and Navigation

### Header.vue (modified from Bootstrap default nav)

```html
<template>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <router-link to="/" class="navbar-brand">Stock Trader</router-link>
    </div>

    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <router-link to="/portfolio" activeClass="active" tag="li"><a>Portfolio</a></router-link>
        <router-link to="/stocks" activeClass="active" tag="li"><a>Stocks</a></router-link>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">End Day</a></li>
        <li class="dropdown">
          <a
            href="#"
            class="dropdown-toggle"
            data-toggle="dropdown"
            role="button"
            aria-haspopup="true"
            aria-expanded="false">Save & Load <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Save Data</a></li>
            <li><a href="#">Load Data</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
</template>
```

---

## 288: Planning the Next Steps

Done

---

## 289: Creating Stocks Components

### stocks/Stocks.vue

```html
<template>
  <div>
    <app-stock v-for="stock in stocks"></app-stock>
  </div>
</template>

<script>
import Stock from './Stock.vue'
export default {
  data () {
    return {
      stocks: [
        { id: 1, name: 'BMW', price: 110 },
        { id: 2, name: 'Google', price: 200 },
        { id: 3, name: 'Apple', price: 250 },
        { id: 4, name: 'Twitter', price: 8 }
      ]
    }
  },
  components: {
    appStock: Stock
  }
}
</script>
```

---

## 290: Adding a "Buy" Button

### Stock.vue

```html
<template>
  <div class="col-sm-6 col-md-4">
    <div class="panel panel-success">
      <div class="panel-heading">
        <h3 class="panel-title">
          {{ stock.name }}
          <small>(Price: {{ stock.price }})</small>
        </h3>
      </div>
      <div class="panel-body">
        <div class="pull-left">
          <input
            type="number"
            class="form-control"
            placeholder="Quantity"
            v-model="quantity">
        </div>
        <div class="pull-right">
          <button
            class="btn btn-success"
            @click="buyStock"
            :disabled="quantity <= 0">
            Buy
          </button>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  props: ['stock'],
  data () {
    return {
      quantity: 0
    }
  },
  methods: {
    buyStock() {
      const order = {
        stockId: this.stock.id,
        stockPrice: this.stock.price,
        quantity: this.quantity
      }
      console.log(order);
      this.quantity = 0;

    }
  }
}
</script>
```

---

## 291: Setting up the Vuex State Management

- set up Vuex
- added store folder with store.js
  - added modules subfolder with stocks.js

### store.js

```js
import Vue from 'vue';
import Vuex from 'vuex';

import stocks from './modules/stocks';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    stocks
  }
})
```

### stocks.js

```js
import stocks from '../../data/stocks';

const state = {
  stocks: []
};

const mutations = {
  'SET_STOCKS' (state, stocks) {
    state.stocks = stocks;
  },
  'RND_STOCKS' (state) {

  }
};

const actions = {
  buyStock: ({ commit}, order) => {
    commit();
  },
  initStocks: ({commit}) => {
    commit('SET_STOCKS', stocks)
  },
  randomizeStocks: ({commit}) => {
    commit('RND_STOCKS');
  }
};

const getters = {
  stocks: state => {
    return state.stocks;
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
```

### data/stocks.js

```js
export default [
  { id: 1, name: 'BMW', price: 110 },
  { id: 2, name: 'Google', price: 200 },
  { id: 3, name: 'Apple', price: 250 },
  { id: 4, name: 'Twitter', price: 8 }
]
```

---

## 292: Adding a Portfolio Module to Vuex

### store/modules/portfolio.js

```js
const state = {
  funds: 10000,
  stocks: []
};

const mutations = {
  'BUY_STOCK'(state, {stockId, quantity, stockPrice}) {
    const record = state.stocks.find(element => element.id == stockId)
    if (record) {
      record.quantity += quantity
    } else {
      state.stocks.push({
        id: stockId,
        quantity: quantity
      })
    }
    state.funds -= stockPrice * quantity
  },
  'SELL_STOCK' (state, {stockId, quantity, stockPrice}) {
    const record = state.stocks.find(element => element.id == stockId)
    if (record.quantity > quantity) {
      record.quantity -= quantity
    } else {
      state.stocks.splice(state.stocks.indexOf(record), 1)
    }
    state.funds += stockPrice * quantity
  }
}

const actions = {
  sellStock({commit}, order) {
    commit('SELL_STOCK', order)
  }
}

const getters = {
  stockPortfolio (state, getters) {
    return state.stocks.map(stock => {
      const record = getters.stocks.find(element => element.id == stock.id)
      return {
        id: stock.id,
        quantity: stock.quantity,
        name: record.name,
        price: record.price
      }
    })
  },
  funds (state) {
    return state.funds
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
```

---

## 293: Working on the Portfolio Stocks

### portfolio/Stock.vue

```html
<template>
  <div class="col-sm-6 col-md-4">
    <div class="panel panel-success">
      <div class="panel-heading">
        <h3 class="panel-title">
          {{ stock.name }}
          <small>(Price: {{ stock.price }} | Quantity: {{ stock.quantity }})</small>
        </h3>
      </div>
      <div class="panel-body">
        <div class="pull-left">
          <input
            type="number"
            class="form-control"
            placeholder="Quantity"
            v-model="quantity">
        </div>
        <div class="pull-right">
          <button
            class="btn btn-success"
            @click="sellStock"
            :disabled="quantity <= 0">
            Sell
          </button>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
import {mapActions} from 'vuex'

export default {
  props: ['stock'],
  data () {
    return {
      quantity: 0
    }
  },
  methods: {
    ...mapActions([
      'sellStock'
    ]),
    sellStock() {
      const order = {
        stockId: this.stock.id,
        stockPrice: this.stock.price,
        quantity: this.quantity
      }
      this.sellStock();
    }
  }
}
</script>
```

---

## 294: Connecting the Portfolio with Vuex

### portfolio/Portfolio.vue

```html
<template>
  <div>
    <app-stock v-for="stock in stocks" :stock="stock"></app-stock>
  </div>
</template>

<script>
  import {mapGetters} from 'vuex'
  import Stock from './Stock.vue'

  export default {
    computed: {
      ...mapGetters({
        stocks: 'stockPortfolio'
      })
    },
    components: {
      appStock: Stock
    }
}
</script>
```

---

## 295: Time to Fix Some Errors

### portfolio/Stock.vue

```html
<template>
  <div class="col-sm-6 col-md-4">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">
          {{ stock.name }}
          <small>(Price: {{ stock.price }} | Quantity: {{ stock.quantity }})</small>
        </h3>
      </div>
      <div class="panel-body">
        <div class="pull-left">
          <input
            type="number"
            class="form-control"
            placeholder="Quantity"
            v-model="quantity">
        </div>
        <div class="pull-right">
          <button
            class="btn btn-info"
            @click="sellStock"
            :disabled="quantity <= 0">
            Sell
          </button>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
import {mapActions} from 'vuex'

export default {
  props: ['stock'],
  data () {
    return {
      quantity: 0
    }
  },
  methods: {
    ...mapActions({
      placeSellOrder: 'sellStock'
    }),
    sellStock() {
      const order = {
        stockId: this.stock.id,
        stockPrice: this.stock.price,
        quantity: this.quantity
      }
      this.placeSellOrder(order);
      this.quantity = 0;
    }
  }
}
</script>
```

---

## 296: Displaying the Funds

### Home.vue

- also added funds to the header

```html
<template>
  <div>
    <h1>Trade or View Your Portfolio</h1>
    <h6>You May Save & Load Your Data</h6>
    <h6>Click on 'End Day' to Begin a New Day!</h6>
    <hr>
    <p>Your Funds: {{ funds }}</p>
  </div>
</template>

<script>
export default {
  computed: {
    funds() {
      return this.$store.getters.funds
    }
  }
}
</script>
```

---

## 297: Adding Some Order Checks

### stocks/Stock.vue

```html
<template>
  <div class="col-sm-6 col-md-4">
    <div class="panel panel-success">
      <div class="panel-heading">
        <h3 class="panel-title">
          {{ stock.name }}
          <small>(Price: {{ stock.price }})</small>
        </h3>
      </div>
      <div class="panel-body">
        <div class="pull-left">
          <input
            type="number"
            class="form-control"
            placeholder="Quantity"
            v-model="quantity"
            :class="{danger: insufficientFunds}">
        </div>
        <div class="pull-right">
          <button
            class="btn btn-success"
            @click="buyStock"
            :disabled="insufficientFunds || quantity <= 0">
            {{ insufficientFunds ? 'Insufficient Funds' : 'Buy'}}
          </button>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  props: ['stock'],
  data () {
    return {
      quantity: 0
    }
  },
  computed: {
    funds() {
      return this.$store.getters.funds
    },
    insufficientFunds() {
      return this.quantity * this.stock.price > this.funds;
    }
  },
  methods: {
    buyStock() {
      const order = {
        stockId: this.stock.id,
        stockPrice: this.stock.price,
        quantity: this.quantity
      }
      this.$store.dispatch('buyStock', order)
      this.quantity = 0;

    }
  }
}
</script>

<style scoped>
  .danger {
    border: 1px solid red;
  }
</style>
```

### portfolio/Stock.vue

```html
<template>
  <div class="col-sm-6 col-md-4">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">
          {{ stock.name }}
          <small>(Price: {{ stock.price }} | Quantity: {{ stock.quantity }})</small>
        </h3>
      </div>
      <div class="panel-body">
        <div class="pull-left">
          <input
            type="number"
            class="form-control"
            placeholder="Quantity"
            v-model="quantity"
            :class="{danger: insufficientFunds}">
        </div>
        <div class="pull-right">
          <button
            class="btn btn-info"
            @click="sellStock"
            :disabled="insufficientQuantity || quantity <= 0">
            {{ insufficientQuantity ? 'Not Enough' : 'Sell'}}
          </button>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
import {mapActions} from 'vuex'

export default {
  props: ['stock'],
  data () {
    return {
      quantity: 0
    }
  },
  computed: {
    insufficientQuantity() {
      return this.quantity > this.stock.quantity;
    }
  },
  methods: {
    ...mapActions({
      placeSellOrder: 'sellStock'
    }),
    sellStock() {
      const order = {
        stockId: this.stock.id,
        stockPrice: this.stock.price,
        quantity: this.quantity
      }
      this.placeSellOrder(order);
      this.quantity = 0;
    }
  }
}
</script>

<style scoped>
  .danger {
    border: 1px solid red;
  }
</style>
```

---

## 298: Making Funds Look Nicer with Filters

### added to main.js

```js
Vue.filter('currency', (value) => {
  return '$' + value.toLocaleString();
});
```

### added filter to Home and Header

```js
{{ funds | currency }}
```

---

## 299: Ending the Day - Randomizing Stocks

### added methods to Header.vue

```js
methods: {
    ...mapActions([
      'randomizeStocks'
    ]),
    endDay() {
      this.randomizeStocks();
    }
  }
```

### added our mutation in stocks.js

```js
const mutations = {
  'SET_STOCKS' (state, stocks) {
    state.stocks = stocks;
  },
  'RND_STOCKS' (state) {
    state.stocks.forEach(stock => {
      stock.price = Math.round(stock.price * (1 + Math.random() - 0.5))
    });
  }
};
```

---

## 300: Animating the Route Transitions

### animations in App.vue

```css
.slide-enter-active {
    animation: slide-in 200ms ease-out forwards;
  }

  .slide-leave-active {
    animation: slide-out 200ms ease-out forwards;
  }

  @keyframes slide-in {
    from {
      transform: translateY(-30px);
      opacity: 0;
    }
    to {
      transform: translateY(0);
      opacity: 1;
    }
  }

    @keyframes slide-out {
    from {
      transform: translateY(0);
      opacity: 1;
    }
    to {
      transform: translateY(-30px);
      opacity: 0;
    }
  }
```

---

## 301: Saving & Fetching Data - Adding a Dropdown

### added dropdown to Header.vue

```html
<li
  class="dropdown"
  @click="isDropdownOpen = !isDropdownOpen"
  :class="{open: isDropdownOpen}">
```

---

## 302: Setting Up vue-resource and Firebase

- installed `vue-resource`
- set up database on Firebase

### main.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

import App from './App.vue'
import { routes } from './routes';
import store from './store/store';

Vue.use(VueRouter);
Vue.use(VueResource);

Vue.http.options.root = 'https://vuejs-stock-trader-3ec0f.firebaseio.com/'

Vue.filter('currency', (value) => {
  return '$' + value.toLocaleString();
});

const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
```

---

## 303: Saving Data (PUT Request)

### saveData method in Header.vue

```js
saveData() {
  const data = {
    funds: this.$store.getters.funds,
    stockPortfolio: this.$store.getters.stockPortfolio,
    stocks: this.$store.getters.stocks
  };
  this.$http.put('data.json', data);
}
```

---

## 304: Fetching Data (GET Request)

### new mutation in portfolio.js

```js
'SET_PORTFOLIO' (state, portfolio) {
    state.funds = portfolio.funds;
    state.stocks = portfolio.stockPortfolio ? portfolio.stockPortfolio : [];
  }
```

### actions.js (new file in store)

```js
import Vue from 'vue'

export const loadData = ({commit}) => {
  Vue.http.get('data.json')
    .then(response => response.json())
    .then(data => {
      if (data) {
        const stocks = data.stocks;
        const funds = data.funds;
        const stockPortfolio = data.stockPortfolio;

        const portfolio = {
          stockPortfolio,
          funds
        };

        commit('SET_STOCKS', stocks);
        commit('SET_PORTFOLIO', portfolio);
      }
    })
}
```

---

## 305: Testing and Bug Fixes

### setting up method in Header.vue

```js
methods: {
    ...mapActions({
      randomizeStocks: 'randomizeStocks',
      fetchData: 'loadData'
    }),
    endDay() {
      this.randomizeStocks();
    },
    saveData() {
      const data = {
        funds: this.$store.getters.funds,
        stockPortfolio: this.$store.getters.stockPortfolio,
        stocks: this.$store.getters.stocks
      };
      this.$http.put('data.json', data);
    },
    loadData() {
      this.fetchData();
    }
  }
```

### also added to the store

```js
import Vue from 'vue';
import Vuex from 'vuex';

import stocks from './modules/stocks';
import portfolio from './modules/portfolio';

import * as actions from './actions'

Vue.use(Vuex);

export default new Vuex.Store({
  actions,
  modules: {
    stocks,
    portfolio
  }
})
```

---

## 306: Project Wrap-up

- Done

---

## 307: Bonus: Debugging Vuex with Developer Tools

- just a review of Dev Tools

---

## 308: Module Resources and Useful Links

[Vue Developer Tools](https://github.com/vuejs/vue-devtools)

---

## Debugging on my own

### Bug #1

- When quantity was >= 10, `insufficientQuantity` wasn't behaving properly
- added `parseInt` to calculation and fixed it

### Bug #2

- when purchasing additional shares, 3 + 1000 = 31000
- similar fix as Bug #1 in `portfolio.js`

---
