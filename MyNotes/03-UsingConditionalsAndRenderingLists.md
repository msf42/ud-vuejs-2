# Using Conditionals and Rendering Lists

---

## 35: Module Introduction

Intro.

---

## 36: Conditional Rendering with `v-if`

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <p v-if="show">You can see me!</p>
    <p v-else>Now you see me!</p>
    <p>Do you also see me?</p>
    <button @click="show=!show">Switch</button>
  </div>

  <script src="app.js"></script>
</body>
</html>
```

### app.js

```js
new Vue({
  el: '#app',
  data: {
    show: true
  }
})
```

![Video](./images/Lesson36.gif)

---

## 37: `v-else-if` in Vue.js 2.1

[v-else-if in Vue 2](https://vuejs.org/v2/guide/conditional.html#v-else-if)

```html
<div v-if="type === 'A'">
  A
</div>
<div v-else-if="type === 'B'">
  B
</div>
<div v-else-if="type === 'C'">
  C
</div>
<div v-else>
  Not A/B/C
</div>
```

---

## 38: Using an Alternative `v-if` Syntax

The `<template>` element itself does not display. We can use it to group things in cases where we don't want to use a `<div>`

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
  <div id="app">
    <p v-if="show">You can see me!</p>
    <p v-else>Now you see me!</p>
    <template v-if="show">
      <h2> A heading inside a template</h2>
      <p> A paragraph inside a template</p>
    </template>
    <p>Do you also see me?</p>
    <button @click="show=!show">Switch</button>
  </div>

  <script src="app.js"></script>
</body>
</html>
```

---

## 39: Don't Detach It with `v-show`

Whereas `v-if` results in an element being completely removed, or detached, `v-show` works similarly, but gives the element a `style="display: none;"` and leaves it in place.

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <p v-if="show">You can see me!</p>
    <p v-else>Now you see me!</p>
    <template v-if="show">
      <h2> A heading inside a template</h2>
      <p> A paragraph inside a template</p>
    </template>
    <p v-show="show">Do you also see me?</p>
    <button @click="show=!show">Switch</button>
  </div>

  <script src="app.js"></script>
</body>
</html>
```

---

## 40: Rendering Lists with `v-for`

Easy way to display a list that can change as needed.

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <ul>
      <li v-for="ingredient in ingredients">{{ ingredient }}</li>
    </ul>
  </div>
  <script src="app2.js"></script>
</body>
</html>

```

### app.js

```js
new Vue({
  el: '#app',
  data: {
    ingredients: ['meat', 'fruit', 'cookies'],
    persons: [
      {name: 'Max', age: 27, color: 'red'},
      {name: 'Anna', age: 'unknown', color: 'blue'}
    ]
  }
});
```

---

## 41: Getting the Current Index

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <ul>
      <li v-for="(ingredient, i) in ingredients">{{ ingredient }} ({{ i }})</li>
    </ul>
  </div>

  <script src="app2.js"></script>
</body>
</html>
```

![image-20191107112116064](./images/image-20191107112116064.png)

---

## 42: Using an Alternative `v-for` Syntax

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <ul>
      <li v-for="(ingredient, i) in ingredients">{{ ingredient }} ({{ i }})</li>
    </ul>
    <template v-for="(ingredient, index) in ingredients">
      <h1>{{ ingredient }}</h1>
      <p>{{ index }}</p>
    </template>
  </div>

  <script src="app2.js"></script>
</body>
</html>

```

![image](./images/image-20191107112446783.png)

---

## 43: Looping Through Objects

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <ul>
      <li v-for="(ingredient, i) in ingredients">{{ ingredient }} ({{ i }})</li>
    </ul>
    <hr>
      <ul>
        <li v-for="person in persons">
          <div v-for="(value, key, index) in person">{{ key }}:{{ value }} ({{ index }})</div>
        </li>
      </ul>
    <hr>
    <template v-for="(ingredient, index) in ingredients">
      <h1>{{ ingredient }}</h1>
      <p>{{ index }}</p>
    </template>
  </div>

  <script src="app2.js"></script>
</body>
</html>
```

![image](./images/image-20191107125945054.png)

---

## 44: Looping Through a List of Numbers

```html
      <span v-for="n in 10">{{ n }}</span>
```

---

## 45: Keeping Track of Elements When Using `v-for`

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>
  <div id="app">
    <ul>
      <li v-for="(ingredient, i) in ingredients" :key="ingredient">{{ ingredient }} ({{ i }})</li>
    </ul>
    <button @click="ingredients.push('spices')">Add New</button>
    <hr>
      <ul>
        <li v-for="person in persons">
          <div v-for="(value, key, index) in person">
            {{ key }}:{{ value }} ({{ index }})
          </div>
        </li>
      </ul>
      <span v-for="n in 10">{{ n }}</span>
    <hr>
    <template v-for="(ingredient, index) in ingredients">
      <h1>{{ ingredient }}</h1>
      <p>{{ index }}</p>
    </template>
  </div>

  <script src="app2.js"></script>
</body>
</html>
```

- Added the key in the first `ul`. This will keep track of the item and prevent it from being overwritten, which could occur if we simply used the index.

---

## Assignment 5: Time to Practice - Conditionals and Lists

### index.html

```html
<!DOCTYPE html>
<head>.....</head>
<body>

  <div id="exercise">

    <!-- 1) Hook up the button to toggle the display of the two paragraphs. 
      Use both v-if and v-show and inspect the elements to see the difference -->
    <div>
      <button @click="show=!show">Toggle</button>
      <p v-if="show">You either see me ...</p>
      <p v-else>...or me</p>
      <p v-show="show">You either see me ...</p>
      <p v-show="!show">...or me</p>
    </div>

    <!-- 2) Output an <ul> of array elements of your choice. Also print the index of each element. -->
    <ul>
      <li v-for="(person, i) in array">{{ person }} ({{ i }})</li>
    </ul>


    <!-- 3) Print all key-value pairs of the following object: 
      {title: 'Lord of the Rings', author: 'J.R.R. Tolkiens', books: '3'}. Also print the index of each item. -->
    <ul>
      <li v-for="(value, key, index) in myObject">{{key}}:{{value}} - ({{index}})</li>
    </ul>


    <!-- 4) Print the following object (only the values) and also create a nested loop for the 
      array: {name: 'TESTOBJECT', data: [1.67, 1.33, 0.98, 2.21]} (hint: use v-for and v-if to achieve this) -->
    <ul>
        <li v-for="(value, key) in testData">
          <div v-if="!Array.isArray(value)">{{ value }}</div>
          <template v-else>
            <li v-for="item in value">{{ item }}</li>
          </template>
        </li>
    </ul>
  </div>

  <script src="app.js"></script>
</body>
</html>
```

### app.js

```js
new Vue({
  el: '#exercise',
  data: {
    show: true,
    array: ['Max', 'Anna', 'Chris', 'Manu'],
    myObject: {
      title: 'Lord of the Rings',
      author: 'J.R.R. Tolkiens',
      books: '3'
    },
    testData: {
      name: 'TESTOBJECT',
      id: 10,
      data: [1.67, 1.33, 0.98, 2.21]
    }
  }
});
```

![image-20191107150312458](./images/image-20191107150312458.png)

---

## 46: Module Wrap-up

Done

---

## 47: Module Resources and Useful Links

### JSFiddle

- [Conditionals (v-if and v-show)](https://jsfiddle.net/smax/hoc719j5/)
- [Lists](https://jsfiddle.net/smax/o7uy2g0u/)

### Useful Links

- Official Docs - [Conditionals](http://vuejs.org/guide/conditional.html)
- Official Docs - [Lists](http://vuejs.org/guide/list.html)

---
