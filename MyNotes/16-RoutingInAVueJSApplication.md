# Routing in a VueJS Application

## 232: Module Introduction

- we use a single page, single url
- behind the scenes, it parts of the page are reloaded

---

## 233: Setting up the VueJS Router (vue-router)

- just downloaded start code and intalled vue-router

---

## 234: Setting Up and Loading Ruoutes

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Routing</h1>
        <hr>
        <router-view></router-view>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
  }
</script>
```

### main.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import { routes } from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
  routes
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})

```

### routes.js

```js
import User from './components/user/User.vue';
import Home from './components/Home.vue';

export const routes = [
  { path: '', component: Home },
  { path: '/user', component: User}
];
```

---

## 235: Understanding Routing Modes (Hash vs History)

- the `/#/` is used in the address bar to prevent the request from going to the server
- the part after the `#` goes to our js application
- server must always return index.html file, even with 404s
- added the following to main.js

```js
const router = new VueRouter({
  routes,
  mode: 'history' // default is 'hash'
});
```

---

## 236: Navigating with Router Links

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Routing</h1>
        <hr>
        <app-header></app-header>
        <!-- app-header always visible
        the router view is visible below -->
        <router-view></router-view>
      </div>
    </div>
  </div>
</template>

<script>
import Header from './components/Header.vue'
  export default {
    components: {
      appHeader: Header
    }
  }
</script>
```

### Header.vue

```html
<template>
  <ul class="nav nav-pills">
    <li role="presentation"><router-link to="/">Home</router-link></li>
    <li role="presentation"><router-link to="/user">User</router-link></li>
  </ul>
</template>
```

---

## 237: Where am I? - Styling Active Links

### Header.vue

- `exact` prevents `/` from makeing *Home* highlighted all the time
- leave it out if using a submenu to keep main menu item always highlighted

```html
<template>
  <ul class="nav nav-pills">
    <router-link to="/" tag="li" active-class="active" exact><a>Home</a></router-link>
    <router-link to="/user" tag="li" active-class="active"><a>User</a></router-link>
  </ul>
</template>
```

---

## 238: Navigating from Code (Imperitive Navigation)

### User.vue

```html
<template>
<div>
  <h1>The User Page</h1>
  <hr>
  <button @click="navigateToHome">Go To Home</button>
</div>
</template>

<script>
export default {
  methods: {
    navigateToHome() {
      this.$router.push('/');
    }
  }
}
</script>
```

![Video](./images/Lesson238.gif)

---

## 239: Setting Up Route Parameters

### changed to Header.vue

```html
    <router-link to="/user/10" tag="li" active-class="active"><a>User</a></router-link>
```

### changed in routes.js

```js
{ path: '/user/:id', component: User}
```

- path will now look for an id
- without one, the address won't work

---

## 240: Fetching and Using Route Parameters

### User.vue

```html
<template>
<div>
  <h1>The User Page</h1>
  <hr>
  <p>Loaded ID: {{ id }}</p>
  <button @click="navigateToHome">Go To Home</button>
</div>
</template>

<script>
export default {
  data() {
    return {
      id: this.$route.params.id
    }
  },
  /* params stores k:v pairs for the route
    this returns the 'id' paramater */
  methods: {
    navigateToHome() {
      this.$router.push('/');
    }
  }
}
</script>
```

---

## 241: Reacting to Changes in Route Parameters

### Header.vue - added link for Users 1 and 2

```html
<template>
  <ul class="nav nav-pills">
    <router-link to="/" tag="li" active-class="active" exact><a>Home</a></router-link>
    <router-link to="/user/1" tag="li" active-class="active"><a>User 1</a></router-link>
    <router-link to="/user/2" tag="li" active-class="active"><a>User 2</a></router-link>
  </ul>
</template>
```

### User.vue - added watch for route

```js
watch: {
    '$route'(to, from) {
      this.id = to.params.id
    }
  },
```

---

## 242: vue-router 2.2: Extract Route Params via "props"

[New Feature](https://github.com/vuejs/vue-router/blob/dev/examples/route-props/app.js)

- as of vue-router version 2.2, there are other ways to monitor the route

---

## 243: Setting Up Child Routes (Nested Routes)

### routes.js

```js
import User from './components/user/User.vue';
import Home from './components/Home.vue';
import UserStart from './components/user/UserStart.vue'
import UserDetail from './components/user/UserDetail.vue'
import UserEdit from './components/user/UserEdit.vue'

export const routes = [
  { path: '', component: Home },
  { path: '/user', component: User, children: [
    { path: '', component: UserStart },
    { path: ':id', component: UserDetail },
    { path: ':id/edit', component: UserEdit }
  ]}
];
```

### added to User.vue

- now directs to the sub-pages above

```html
<router-view></router-view>
```

---

## 244: Navigating to Nested Routes

### UserStart.vue

- changed `li` to `router-link` and added `tag="li"`

```html
<template>
  <div>
    <p>Please select a User</p>
    <hr>
    <ul class="list-group">
      <router-link
        tag="li"
        to="/user/1"
        class="list-group-item"
        style="cursor: pointer">User 1</router-link>
      <router-link
        tag="li"
        to="/user/2"
        class="list-group-item"
        style="cursor: pointer">User 2</router-link>
      <router-link
        tag="li"
        to="/user/3"
        class="list-group-item"
        style="cursor: pointer">User 3</router-link>
    </ul>
  </div>
</template>
```

## UserDetail.vue

- added line to show id

```html
<template>
  <div>
    <h3>Some User Details</h3>
    <p>User loaded has ID: {{ $route.params.id }}</p>
  </div>
</template>
```

---

## 245: Making Router Links More Dynamic

### UserDetail.vue

```html
<template>
  <div>
    <h3>Some User Details</h3>
    <p>User loaded has ID: {{ $route.params.id }}</p>
    <router-link
      tag="button"
      :to="'/user' + $route.params.id + '/edit'"
      class="btn btn-primary">Edit User</router-link>
  </div>
</template>
```

---

## 246: A Better Way of Creating Links - With Named Routes

### UserDetail.vue

- we inserted the name here

```html
<template>
  <div>
    <h3>Some User Details</h3>
    <p>User loaded has ID: {{ $route.params.id }}</p>
    <router-link
      tag="button"
      :to="{ name: 'userEdit', params: { id: $route.params.id } }"
      class="btn btn-primary">Edit User</router-link>
  </div>
</template>
```

### routes.js

- we added the name here

```js
export const routes = [
  { path: '', component: Home },
  { path: '/user', component: User, children: [
    { path: '', component: UserStart },
    { path: ':id', component: UserDetail },
    { path: ':id/edit', component: UserEdit, name: 'userEdit' }
  ]}
];
```

---

## 247: Using Query Parameters

### UserDetail.vue

- passing in parameters here in the router-link
- these show up in the address bar

```html
<template>
  <div>
    <h3>Some User Details</h3>
    <p>User loaded has ID: {{ $route.params.id }}</p>
    <router-link
      tag="button"
      :to="{ name: 'userEdit', params: { id: $route.params.id }, query: { locale: 'en', q: 100 } }"
      class="btn btn-primary">Edit User</router-link>
  </div>
</template>
```

### UserEdit.vue

- accessing those parameters here

```html
<template>
<div>
  <h3>Edit the User</h3>
  <p>Locale: {{ $route.query.locale }}</p>
  <p>Analytics: {{ $route.query.q }}</p>
</div>
</template>
```

---

## 248: Multiple Router Views (Named Router Views)

### App.vue

- added named router-views

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Routing</h1>
        <hr>
        <router-view name="header-top"></router-view>
        <router-view></router-view>
        <router-view name="header-bottom"></router-view>
      </div>
    </div>
  </div>
</template>

<script>
import Header from './components/Header.vue'
  export default {
    components: {
      appHeader: Header
    }
  }
</script>
```

### routes.js

- modified routes so we can choose which components show and where

```js
import User from './components/user/User.vue';
import Home from './components/Home.vue';
import UserStart from './components/user/UserStart.vue'
import UserDetail from './components/user/UserDetail.vue'
import UserEdit from './components/user/UserEdit.vue'
import Header from './components/Header.vue'

export const routes = [
  { path: '', name: 'home', components: {
    default: Home,
    'header-top': Header
  } },
  { path: '/user', components: {
    default: User,
    'header-bottom': Header
  }, children: [
    { path: '', component: UserStart },
    { path: ':id', component: UserDetail },
    { path: ':id/edit', component: UserEdit, name: 'userEdit' }
  ]}
];
```

---

## 249: Redirecting

- added at the end of routes.js
- this only redirects specifically from `/redirect-me`

```js
{ path: '/redirect-me', redirect: '/user' }
```

![video](./images/Lesson249.gif)

---

## 250: Setting Up "Catch All" Routes / Wildcards

- added wildcard to catch any wrong addresses

```js
{ path: '*', redirect: '/' }
```

---

## 251: Animating Route Transitions

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Routing</h1>
        <hr>
          <router-view name="header-top"></router-view>
        <!-- Wrap the router-view in a transition -->
          <transition name="slide" mode="out-in">
            <router-view></router-view>
          </transition>
          <router-view name="header-bottom"></router-view>
      </div>
    </div>
  </div>
</template>

<script>
  import Header from './components/Header.vue';
  export default {
    components: {
      appHeader: Header
    }
  }
</script>

<style>
  .slide-leave-active {
    transition: opacity 1s ease;
    opacity: 0;
    animation: slide-out 1s ease-out forwards;
  }
  .slide-leave {
    opacity: 1;
    transform: translateX(0);
  }
  .slide-enter-active {
    animation: slide-in 1s ease-out forwards;
  }
  @keyframes slide-out {
    0% {
      transform: translateY(0);
    }
    100% {
      transform: translateY(-30px);
    }
  }
  @keyframes slide-in {
    0% {
      transform: translateY(-30px);
    }
    100% {
      transform: translateY(0);
    }
  }
</style>
```

---

## 252: Passing the Hash Fragment

### UserEdit.vue

- aded some extra at the bottom to push the data section out of view

```html
<template>
  <div>
    <h3>Edit the User</h3>
    <p>Locale: {{ $route.query.locale }}</p>
    <p>Analytics: {{ $route.query.q }}</p>
    <div style="height: 700px"></div>
    <p id="data">Some extra data</p>
  </div>
</template>
```

### UserDetail.vue

- moved `link` info to data
- added hash to direct directly to data section

```html
<template>
  <div>
    <h3>Some User Details</h3>
    <p>User loaded has ID: {{ $route.params.id }}</p>
    <router-link
      tag="button"
      :to="link"
      class="btn btn-primary">Edit User</router-link>
  </div>
</template>

<script>
export default {
  data () {
    return {
      link: {
        name: 'userEdit',
        params: {
          id: this.$route.params.id
        },
        query: {
          locale: 'en',
          q: 100
        },
        hash: 'data'
      }
    }
  }
}
</script>
```

---

## 253: Controlling the Scroll Behavior

### main.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import { routes } from './routes';

Vue.use(VueRouter);

/*
if saved position, return to it
if directing to hash, then do so
else return to top
*/
const router = new VueRouter({
  routes,
  mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return { selector: to.hash };
    }
    return {x: 0, y: 0 }
  }
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
```

---

## 254: Protecting Routes with Guards

- only added 'confirm' button to UserEdit page

---

## 255: Using the "beforeEnter" Guard

### main.js

- added this in main.js
- it is activated for all route movements
  
```js
router.beforeEach((to, from, next) => {
  console.log('global beforeEach');
  next();
});
```

### routes.js

- added this in the UserDetail path in routes.js
- it only loads when we go to UserDetail page

```js
{ path: ':id', component: UserDetail, beforeEnter: (to, from, next) => {
      console.log('inside route setup');
      next();
    } },
```

### UserDetail.vue

- added this inside the script in UserDetail.vue
- it is a way to verify that a user has permission to see the page
- simply set to 'true' in this example

```js
beforeRouteEnter(to, from, next) {
    if (true) {
      next();
    } else {
      next(false);
    }
  }
```

---

## 256: Using the "beforeLeave" Guard

### UserEdit.vue

- added event listener to button
- added `beforeRouteLeave`
  - allows leaving if confirm button has been clicked
  - else it asks "are you sure?"

```html
<template>
  <div>
    <h3>Edit the User</h3>
    <p>Locale: {{ $route.query.locale }}</p>
    <p>Analytics: {{ $route.query.q }}</p>
    <hr>
    <button class="btn btn-primary" @click="confirmed = true">Confirm</button>
    <div style="height: 700px"></div>
    <p id="data">Some extra data</p>
  </div>
</template>

<script>
export default {
  data () {
    return {
      confirmed: false
    }
  },
  beforeRouteLeave(to, from, next) {
    if (this.confirmed) {
      next();
    } else {
      if (confirm('Are you sure?')) {
        next();
      } else {
        next(false)
      }
    }
  }
}
</script>
```

---

## 257: Loading Routes Lazily

### routes.js

- lazy loading only downloads when they are required
- could also add a third argument with a group name
  - this would load the whole bundle when needed

```js
import Home from './components/Home.vue';
import Header from './components/Header.vue'

const User = resolve => {
  require.ensure(['./components/user/User.vue'], () => {
    resolve(require('./components/user/User.vue'));
  });
};
const UserStart = resolve => {
  require.ensure(['./components/user/UserStart.vue'], () => {
    resolve(require('./components/user/UserStart.vue'));
  });
};
const UserEdit = resolve => {
  require.ensure(['./components/user/UserEdit.vue'], () => {
    resolve(require('./components/user/UserEdit.vue'));
  });
};
const UserDetail = resolve => {
  require.ensure(['./components/user/UserDetail.vue'], () => {
    resolve(require('./components/user/UserDetail.vue'));
  });
};

export const routes = [
  { path: '', name: 'home', components: {
    default: Home,
    'header-top': Header
  } },
  { path: '/user', components: {
    default: User,
    'header-bottom': Header
  }, children: [
    { path: '', component: UserStart },
    { path: ':id', component: UserDetail, beforeEnter: (to, from, next) => {
      console.log('inside route setup');
      next();
    } },
    { path: ':id/edit', component: UserEdit, name: 'userEdit' }
  ]},
  { path: '/redirect-me', redirect: '/user' },
  { path: '*', redirect: '/' }
];
```

---

## 258: Wrap Up

Done

---

## 259: Module Resources and Useful Links

### Useful Links

- [vue-router Github Page](https://github.com/vuejs/vue-router)
- [vue-router Documentation](https://router.vuejs.org/en/)

---
