# Better State Management with Vuex

## 260: Module Introduction

Done

---

## 261: Why a Different State Management May Be Needed

- using props or even an Event Bus can get pretty complex in large applications

---

## 262: Understanding "Centralized State:

- one file, a central 'store' that holds the state

---

## 263: Using the Centralized State

### Without Vuex

#### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Vuex</h1>
        <app-result :counter="counter"></app-result>
        <hr>
        <app-counter @updated="counter += $event"></app-counter>
      </div>
    </div>
  </div>
</template>

<script>
  import Counter from './components/Counter.vue';
  import Result from './components/Result.vue';

  export default {
    data() {
      return {
        counter: 0
      }
    },
    components: {
      appCounter: Counter,
      appResult: Result,
    }
  }
</script>


```

#### Result.vue

```html
<template>
  <p>Counter is: {{ counter }}</p>
</template>

<script>
  export default {
    props: ['counter']
  }
</script>
```

#### Counter.vue

```html
<template>
<div>
  <button class="btn btn-primary" @click="increment">Increment</button>
  <button class="btn btn-primary" @click="decrement">Decrement</button>
</div>
</template>

<script>
export default {
  methods: {
  increment() {
    this.$emit('updated', 1);
  },
  decrement() {
    this.$emit('updated', -1);
  }
  }
}
</script>
```

### With Vuex

#### store/store.js

```js
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    counter: 0
  }
});
```

#### main.js

```js
import Vue from 'vue'
import App from './App.vue'

import { store } from './store/store';

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
```

#### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Vuex</h1>
        <app-result></app-result>
        <hr>
        <app-counter></app-counter>
      </div>
    </div>
  </div>
</template>

<script>
  import Counter from './components/Counter.vue';
  import Result from './components/Result.vue';

  export default {
    components: {
      appCounter: Counter,
      appResult: Result,
    }
  }
</script>
```

#### Counter.vue

```html
<template>
<div>
  <button class="btn btn-primary" @click="increment">Increment</button>
  <button class="btn btn-primary" @click="decrement">Decrement</button>
</div>
</template>

<script>
export default {
  methods: {
  increment() {
    this.$store.state.counter++;
  },
  decrement() {
    this.$store.state.counter--;
  }
  }
}
</script>
```

#### Result.vue

```html
<template>
  <p>Counter is: {{ counter }}</p>
</template>

<script>
  export default {
    computed: {
      counter () {
        return this.$store.state.counter;
      }
    }
  }
</script>
```

![video](./images/Lesson263.gif)

---

## 264: Why a Centralized State Alone Won't Fix It

### Result.vue and AnotherResult.vue

- we added this simple calculation in Result.vue, then duplicated it in AnotherResult.vue
- it still works fine, but this could cause problems in more complex applications
  - errors in the formula in one location, producing different results
  - large amounts of duplicated data

```html
<template>
  <p>Counter is: {{ counter }}</p>
</template>

<script>
  export default {
    computed: {
      counter () {
        return this.$store.state.counter * 2;
      }
    }
  }
</script>
```

---

## 265: Understanding Getters

![image](./images/Lesson265.png)

---

## 266: Using Getters

### store.js

- added the 'getters' section
- here, doubleCounter does the calculation for us
  - we don't have to repeatedly do the same calculation

```js
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    counter: 0
  },
  getters: {
    doubleCounter: state => {
      return state.counter * 2;
    }
  }
});
```

- then we simply add this to our Result.vue

```html
<script>
  export default {
    computed: {
      counter () {
        return this.$store.getters.doubleCounter
      }
    }
  }
</script>
```

---

## 267: Mapping Getters to Properties

### AnotherResult.vue

- added a second getter in the store
- here we use mapGetters to get them all
- use the spread operator, then add our own computed properties

```html
<template>
<div>
  <p>Counter is: {{ doubleCounter }}</p>
  <p>Number of Clicks: {{ stringCounter }}</p>
</div>
</template>

<script>
import { mapGetters } from 'vuex';
  export default {
    computed: {
      ...mapGetters([
      'doubleCounter',
      'stringCounter'
      ]),
      // add more computed properties here
    }
  }
</script>
```

---

## 268: Understanding Mutations

![image](./images/Lesson268.png)

---

## 269: Using Mutations

### AnotherCounter.vue

- added another counter
- used mutations instead of functions/getters

```html
<template>
<div>
  <button class="btn btn-primary" @click="increment">Increment</button>
  <button class="btn btn-primary" @click="decrement">Decrement</button>
</div>
</template>

<script>
  import { mapMutations } from 'vuex';
export default {
  methods: {
    ...mapMutations([
      'increment',
      'decrement'
    ]),
    // other mutations here
  }
}
</script>
```

---

## 270: Why Mutations Have to Run Synchronously

- if we ran them async, vue couldn't tell where the change came from or when

---

## 271: How Actions Improve Mutations

- An Action is an extra function that runs an asynchronous task
- after the async task is complete, it commits the mutation

![image](./images/Lesson271.png)

---

## 272: Using Actions

### store.js

```js
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    counter: 0
  },
  getters: {
    doubleCounter: state => {
      return state.counter * 2;
    },
    stringCounter: state => {
      return state.counter + ' Clicks';
    }
  },
  mutations: {
    increment: state => {
      state.counter++;
    },
    decrement: state => {
      state.counter--;
    }
  },
  actions: {
    increment: ({ commit }) => {
      commit('increment');
    },
    decrement: ({ commit }) => {
      commit('decrement');
    },
    asyncIncrement: ({ commit }) => {
      setTimeout(() => {
        commit('increment')
      }, 1000);
    },
    asyncDecrement: ({ commit }) => {
      setTimeout(() => {
        commit('decrement')
      }, 1000);
    },
  }
  /*
  // could also write it this way;
  // above only uses the 'commit' feature
  actions: {
    increment: context => {
      context.commit('increment')
    }
  }
  */
});
```

---

## 273: Mapping Actions to Methods

### store.js

- takes payload (by and duration) from AnotherCounter

```js
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    counter: 0
  },
  getters: {
    doubleCounter: state => {
      return state.counter * 2;
    },
    stringCounter: state => {
      return state.counter + ' Clicks';
    }
  },
  mutations: {
    increment: (state, payload) => {
      state.counter += payload;
    },
    decrement: (state, payload) => {
      state.counter -= payload;
    }
  },
  actions: {
    increment: ({ commit }, payload) => {
      commit('increment', payload);
    },
    decrement: ({ commit }, payload) => {
      commit('decrement', payload);
    },
    asyncIncrement: ({ commit }, payload) => {
      setTimeout(() => {
        commit('increment', payload.by)
      }, payload.duration);
    },
    asyncDecrement: ({ commit }, payload) => {
      setTimeout(() => {
        commit('decrement', payload.by)
      }, payload.duration);
    },
  }
});
```

### AnotherCounter.vue

- we pass in an object which includes `by` and `duration`

```html
<template>
<div>
  <button class="btn btn-primary" @click="asyncIncrement({by: 50, duration: 500})">Increment</button>
  <button class="btn btn-primary" @click="asyncDecrement({by: 50, duration: 500})">Decrement</button>
</div>
</template>

<script>
  import { mapActions } from 'vuex';

  export default {
    methods: {
      ...mapActions([
        'asyncIncrement',
        'asyncDecrement'
      ]),
    }
  }
</script>
```

#### in action - lower buttons are async

![gif](./images/Lesson273.gif)

---

## 274: A Summary of Vuex

- Done - nice review of Vuex

---

## 275: Two-Way-Binding (v-model) and Vuex

### store.js

- added state for `value`
- added `getter`, `mutation`, and `action` to change value

```js
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    counter: 0,
    value: 0,
  },
  getters: {
    doubleCounter: state => {
      return state.counter * 2;
    },
    stringCounter: state => {
      return state.counter + ' Clicks';
    },
    value: state => {
      return state.value
    }
  },
  mutations: {
    increment: (state, payload) => {
      state.counter += payload;
    },
    decrement: (state, payload) => {
      state.counter -= payload;
    },
    updateValue: (state, payload) => {
      state.value = payload
    }
  },
  actions: {
    increment: ({ commit }, payload) => {
      commit('increment', payload);
    },
    decrement: ({ commit }, payload) => {
      commit('decrement', payload);
    },
    asyncIncrement: ({ commit }, payload) => {
      setTimeout(() => {
        commit('increment', payload.by)
      }, payload.duration);
    },
    asyncDecrement: ({ commit }, payload) => {
      setTimeout(() => {
        commit('decrement', payload.by)
      }, payload.duration);
    },
    updateValue({commit}, payload) {
      commit('updateValue', payload)
    }
  }
});
```

### App.vue

- using v-model for `value`
- added `get()` and `set()` for value to computed

```js
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Vuex</h1>
        <app-result></app-result>
        <app-another-result></app-another-result>
        <hr>
        <app-counter></app-counter>
        <app-another-counter></app-another-counter>
        <hr>
        <input type="text" v-model="value">
        <p>{{ value }}</p>
      </div>
    </div>
  </div>
</template>

<script>
  import Counter from './components/Counter.vue';
  import Result from './components/Result.vue';
  import AnotherResult from './components/AnotherResult.vue';
  import AnotherCounter from './components/AnotherCounter.vue';

  export default {
    computed: {
      value: {
        get() {
          return this.$store.getters.value
        },
        set(value) {
          this.$store.dispatch('updateValue', value);
        }
      }
    },
    methods: {
      updateValue(event) {
        this.$store.dispatch('updateValue', event.target.value)
      }
    },
    components: {
      appCounter: Counter,
      appResult: Result,
      appAnotherResult: AnotherResult,
      appAnotherCounter: AnotherCounter,
    }
  }
</script>
```

---

## 276: Improving Folder Structures

- created store/modules folder

---

## 277:Modularizing the State Management

### store.js (all counter info removed)

```js
import Vue from 'vue';
import Vuex from 'vuex';
import counter from './modules/counter'

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    value: 0,
  },
  getters: {
    value: state => {
      return state.value
    }
  },
  mutations: {
    updateValue: (state, payload) => {
      state.value = payload
    }
  },
  actions: {
    updateValue({commit}, payload) {
      commit('updateValue', payload)
    }
  },
  modules: {
    counter
  }
});
```

### new counter.js file

```js
const state = {
  counter: 0
};

const getters = {
  doubleCounter: state => {
    return state.counter * 2;
  },
  stringCounter: state => {
    return state.counter + ' Clicks';
  }
};

const mutations = {
  increment: (state, payload) => {
    state.counter += payload;
  },
  decrement: (state, payload) => {
    state.counter -= payload;
  }
};

const actions = {
  increment: ({ commit }, payload) => {
    commit('increment', payload);
  },
  decrement: ({ commit }, payload) => {
    commit('decrement', payload);
  },
  asyncIncrement: ({ commit }, payload) => {
    setTimeout(() => {
      commit('increment', payload.by)
    }, payload.duration);
  },
  asyncDecrement: ({ commit }, payload) => {
    setTimeout(() => {
      commit('decrement', payload.by)
    }, payload.duration);
  }
};

export default {
  state,
  getters,
  mutations,
  actions,
}
```

---

## 278: Using Separate Files

- within store folder, we added:
  - mutations.js
  - getters.js
  - actions.js
- this is another way to split up the store.js file

### store.js (now streamlined)

```js
import Vue from 'vue';
import Vuex from 'vuex';
import counter from './modules/counter'
import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    value: 0,
  },
  getters,
  mutations,
  actions,
  modules: {
    counter
  }
});
```

### mutations.js

```js
export const updateValue = (state, payload) => {
  state.value = payload
}
```

### getters.js

```js
export const value = state => {
  return state.value
}
```

### actions.js

```js
export const updateValue = ({commit}, payload) => {
  commit('updateValue', payload)
}
```

---

## 279: Using Namespaces to Avoid Naming Problems

- a lot of work, not sure if it would be worth it

---

## 280: Auto-namespacing with Vuex 2.1

Vuex now has auto-namespacing

- [link](https://github.com/vuejs/vuex/releases/tag/v2.1.0)

---

## 281: Wrap-up

Done

---

## 282: Module Resources and Useful Links

### Useful Links:

- [Vuex Github Page](https://github.com/vuejs/vuex)
- [Vuex Documenation](https://vuex.vuejs.org/en/)

---
