# Moving to a "Real" Development Workflow with Webpack and Vue CLI

## 80: Module Introduction

Done.

---

## 81: Why Do We Need a Development Server?

![image-20191118131114346](./images/image-20191118131114346.png)

---

## 82: What does "Development Workflow" Mean?

![image-20191118141502537](./images/image-20191118141502537.png)

---

## 83: Using the Vue CLI to Create Projects

![image-20191118141718936](./images/image-20191118141718936.png)

---

## 84: Installing the Vue CLI and Creating a New Project

Did the `vue-cli` install and created a new project. Very similar to the React project setup.

- [Vue CLI on Github](https://github.com/vuejs/vue-cli)

---

## 85: An Overview Over the Webpack Template Folder Structure

- Babel: allows us to use ES6 syntax
- index.html -file that actually gets served
  - imports script
  - the `<div id="app"></div>` is where all of our code will be placed

---

## 86: Understanding `.vue` Files

### App.vue

Contains 3 parts - the template, the script, and (optionally) styles.

```js
<template>
  <h1>Hello World!</h1>
</template>

<script>
export default {
  
  }
</script>

<style>

</style>
```

---

## 87: Understanding the Object in the Vue File

Done.

---

## 88: How to Build Your App for Production

`npm run build` creates the production copy of the program.

---

## 89: Module Wrap Up

Done

---

## 90: More About `.vue` Files and the CLI

Done

---

## 91: Debugging VueJS Projects

Done

---
