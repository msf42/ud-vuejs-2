# Adding Animations and Transitions

## 189: Module Introduction

Done.

---

## 190: Understanding Transitions

Done.

---

## 191: Preparing Code to Use Transitions

Not using a transition yet.

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Animations</h1>
        <hr>
        <button class="btn btn-primary" @click="show = !show">Show Alert</button>
        <br><br>
        <transition>
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        show: false
      }
    }
  }
</script>
```

---

## 192: Setting Up a Transition

![image-20191212191032469](./images/image-20191212191032469.png)

---

## 193: Assigning CSS Classes for Transitions

- Just setting it up

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Animations</h1>
        <hr>
        <button class="btn btn-primary" @click="show = !show">Show Alert</button>
        <br><br>
        <!-- all .fade styles will get attached to this-->
        <transition name="fade">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        show: false
      }
    }
  }
</script>

<style>
  .fade-enter {

  }

  .fade-enter-active {

  }

  .fade-leave {

  }

  .fade-leave-active {

  }
</style>
```

---

## 194: Creating a "Fade" Transition with CSS Transition Property

- Added CSS

### App.vue (partial)

```html
<style>
  .fade-enter {
    opacity: 0; /* only lasts one frame, then transitions to 1, 1 is default */
  }

  .fade-enter-active {
    transition: opacity 1s; /* can also transition all */
    /* opacity starts at 0 by default */
  }

  .fade-leave {
    /* opacity: 1;    --> default is 1, so unnecessary */
  }

  .fade-leave-active {
    transition: opacity 1s;
    opacity: 0; /* default is already 1 */
  }
</style>
```

---

## 195: Creating a "Slide" Transition with the CSS Animation Property

### App.vue

- Keyframes are used in CSS to define animation rules - [w3schools page on keyframes](https://www.w3schools.com/cssref/css3_pr_animation-keyframes.asp)

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Animations</h1>
        <hr>
        <button class="btn btn-primary" @click="show = !show">Show Alert</button>
        <br><br>
        <transition name="fade">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <!------------ Added this ------------>
        <transition name="slide">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <!------------ Added this ------------>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        show: false
      }
    }
  }
</script>

<style>
  .fade-enter {
    opacity: 0;
  }

  .fade-enter-active {
    transition: opacity 1s;
  }

  .fade-leave {
    /* opacity: 1; */
  }

  .fade-leave-active {
    transition: opacity 1s;
    opacity: 0;
  }

/*------------ Added this ------------*/
  .slide-enter {
    /* transform: translateY(20px); */
  }

  .slide-enter-active {
    animation: slide-in 1s ease-out forwards;
  }
/* forwards makes it stay at this state */
  .slide-leave {

  }

  .slide-leave-active {
    animation: slide-out 1s ease-out forwards;
  }
/* tells it where to start (from) and go (to) */
  /* defines the specifics of the css classes above */
  @keyframes slide-in {
    from {
      transform: translateY(20px);
    }
    to {
      transform: translateY(0);
    }
  }
  @keyframes slide-out {
    from {
      transform: translateY(0);
    }
    to {
      transform: translateY(20px);
    }
  }
</style>
```

---

## 196: Mixing Transition and Animation Properties

- if you set up `transition` and `animation`, make sure you specify which one dictates the length. Otherwise the movement will look strange. Use this:

```html
<transition name="slide" type="animation"> <!-- see CSS note below -->
  <div class="alert alert-info" v-if="show">This is some info</div>
</transition>
```

because we use both:

```css
  .slide-leave-active {
    animation: slide-out 1s ease-out forwards; /* type=animation gives this priority */
    transition: opacity 3s;
    opacity: 0;
  }
```

---

## 197: Animating `v-if` and `v-show`

`v-show` is only for animating. `v-if` can also be used.

---

## 198: Setting Up an Initial (on-load) Animation

- changed initial value of 'show' to 'true'
- added 'appear' to transition element

---

## 199: Using Different CSS Class Names

### Animate CSS

 [animate.css](https://daneden.github.io/animate.css/)

`npm install animate.css --save`

```html
<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
</head>
```

- added the cdn link above
- added these lines below:

```html
<transition
  enter-active-class="animated bounce"
  leave-active-class="animated shake"
 >
  <div class="alert alert-info" v-if="show">This is some info</div>
</transition>
```

![Video](./images/Lesson199.gif)

---

## 200: Using Dynamic Names and Attributes

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Animations</h1>
        <hr>

        <!-- added this section -->
        <select v-model="alertAnimation" class="form-control">
          <option value="fade">Fade</option>
          <option value="slide">Slide</option>
        </select>
        <!----------------------->
        <br><br>
        <button class="btn btn-primary" @click="show = !show">Show Alert</button>
        <br><br>
        <transition :name="alertAnimation">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition name="slide" type="animation">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition
          appear
          enter-active-class="animated bounce"
          leave-active-class="animated shake"
        >
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        show: true,             /* button changes this */
        alertAnimation: 'fade' /* added this */
      }
    }
  }
</script>

<style>
  /* this section unchanged */
</style>
```

---

## 201: Transitioning Between Multiple Elements (Theory)

Often we want one transition to begin as soon as one ends.

---

## 202: Transitioning Between Multiple Elements (Practice)

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Animations</h1>
        <hr>
        <select v-model="alertAnimation" class="form-control">
          <option value="fade">Fade</option>
          <option value="slide">Slide</option>
        </select>
        <br><br>
        <button class="btn btn-primary" @click="show = !show">Show Alert</button>
        <br><br>
        <transition :name="alertAnimation">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition name="slide" type="animation">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition
          enter-active-class="animated bounce"
          leave-active-class="animated shake"
        >
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <!-- mode out-in makes it wait for last div to fade out before fading in -->
        <!-- when we use two of the same elements within a transition, 
					we must 'key' them, so that VueJS can differentiate them -->
        <transition :name="alertAnimation" mode="out-in">
          <div class="alert alert-info" v-if="show" key="info">This is some Info</div>
          <div class="alert alert-warning" v-else key="warning">This is some Info</div>

        </transition>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        show: true,
        alertAnimation: 'fade'
      }
    }
  }
</script>

<style>
  .fade-enter {
    opacity: 0;
  }

  .fade-enter-active {
    transition: opacity 1s;
  }

  .fade-leave {
    /* opacity: 1; */
  }

  .fade-leave-active {
    transition: opacity 1s;
    opacity: 0;
  }

  .slide-enter {
    opacity: 0;
    /* transform: translateY(20px); */
  }

  .slide-enter-active {
    animation: slide-in 1s ease-out forwards;
    transition: opacity 0.5s;
  }

  .slide-leave {

  }

  .slide-leave-active {
    animation: slide-out 1s ease-out forwards;
    transition: opacity 1s;
    opacity: 0;
  }

  @keyframes slide-in {
    from {
      transform: translateY(20px);
    }
    to {
      transform: translateY(0);
    }
  }
  @keyframes slide-out {
    from {
      transform: translateY(0);
    }
    to {
      transform: translateY(20px);
    }
  }
</style>
```

---

## 203: Listening to Transition Event Hooks

![image-20191219100236711](./images/image-20191219100236711.png)

---

## 204: Understanding JavaScript Animations

![image-20191219103347971](./images/image-20191219103347971.png)

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Animations</h1>
        <hr>
        <select v-model="alertAnimation" class="form-control">
          <option value="fade">Fade</option>
          <option value="slide">Slide</option>
        </select>
        <br><br>
        <button class="btn btn-primary" @click="show = !show">Show Alert</button>
        <br><br>
        <transition :name="alertAnimation">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition name="slide" type="animation">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition
          enter-active-class="animated bounce"
          leave-active-class="animated shake"
        >
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition :name="alertAnimation" mode="out-in">
          <div class="alert alert-info" v-if="show" key="info">This is some Info</div>
          <div class="alert alert-warning" v-else key="warning">This is some Info</div>
        </transition>
        <hr>
        <button class="btn btn-primary" @click="load = !load">Load / Remove Element</button>
        <br><br>
        <transition
          @before-enter="beforeEnter"
          @enter="enter"
          @after-enter="afterEnter"
          @enter-cancelled="enterCancelled"

          @before-leave="beforeLeave"
          @leave="leave"
          @after-leave="afterLeave"
          @leave-cancelled="leaveCancelled"
          >
          <div style="width: 100px; height: 100px; background-color: lightgreen" v-if="load"></div>
        </transition>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        show: false,
        load: true,
        alertAnimation: 'fade'
      }
    },
    methods: {
      beforeEnter(el) {
        console.log('beforeEnter');
      },
      enter(el, done) {
        console.log('enter');
        done(); /* tells VueJS when this is finished */
      },
      afterEnter(el) {
        console.log('afterEnter');
      },
      enterCancelled(el) {
        console.log('enterCancelled');
      },
      beforeLeave(el) {
        console.log('beforeLeave');
      },
      leave(el, done) {
        console.log('leave');
        done();
      },
      afterLeave(el) {
        console.log('afterLeave');
      },
      leaveCancelled(el) {
        console.log('leaveCancelled');
      }
    }
  }
</script>

<style>
  .fade-enter {
    opacity: 0;
  }

  .fade-enter-active {
    transition: opacity 1s;
  }

  .fade-leave {
    /* opacity: 1; */
  }

  .fade-leave-active {
    transition: opacity 1s;
    opacity: 0;
  }

  .slide-enter {
    opacity: 0;
    /* transform: translateY(20px); */
  }

  .slide-enter-active {
    animation: slide-in 1s ease-out forwards;
    transition: opacity 0.5s;
  }

  .slide-leave {

  }

  .slide-leave-active {
    animation: slide-out 1s ease-out forwards;
    transition: opacity 1s;
    opacity: 0;
  }

  @keyframes slide-in {
    from {
      transform: translateY(20px);
    }
    to {
      transform: translateY(0);
    }
  }
  @keyframes slide-out {
    from {
      transform: translateY(0);
    }
    to {
      transform: translateY(20px);
    }
  }
</style>
```

---

## 205: Excluding CSS from Your Animation

```html
<transition
          @before-enter="beforeEnter"
          @enter="enter"
          @after-enter="afterEnter"
          @enter-cancelled="enterCancelled"
          @before-leave="beforeLeave"
          @leave="leave"
          @after-leave="afterLeave"
          @leave-cancelled="leaveCancelled"
          :css="false">
  <!-- the last line saves time by saying: don't bother looking at css, we are using JavaScript -->
```

---

## 206: Creating an Animation in JavaScript

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Animations</h1>
        <hr>
        <select v-model="alertAnimation" class="form-control">
          <option value="fade">Fade</option>
          <option value="slide">Slide</option>
        </select>
        <br><br>
        <button class="btn btn-primary" @click="show = !show">Show Alert</button>
        <br><br>
        <transition :name="alertAnimation">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition name="slide" type="animation">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition
          enter-active-class="animated bounce"
          leave-active-class="animated shake"
        >
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition :name="alertAnimation" mode="out-in">
          <div class="alert alert-info" v-if="show" key="info">This is some Info</div>
          <div class="alert alert-warning" v-else key="warning">This is some Info</div>
        </transition>
        <hr>
        <button class="btn btn-primary" @click="load = !load">Load / Remove Element</button>
        <br><br>
        <transition
          @before-enter="beforeEnter"
          @enter="enter"
          @after-enter="afterEnter"
          @enter-cancelled="enterCancelled"

          @before-leave="beforeLeave"
          @leave="leave"
          @after-leave="afterLeave"
          @leave-cancelled="leaveCancelled"
          :css="false">
          <div style="width: 300px; height: 100px; background-color: lightgreen" v-if="load"></div>
        </transition>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        show: false,
        load: true,
        alertAnimation: 'fade',
        elementWidth: 100
      }
    },
    methods: {
      beforeEnter(el) {
        console.log('beforeEnter');
        this.elementWidth = 100;
        el.style.width = this.elementWidth + 'px';
      },
      enter(el, done) {
        console.log('enter');
        let round = 1;
        const interval = setInterval(() => {
          el.style.width = (this.elementWidth + round*10) + 'px';
          round++;
          if (round > 20) {
            clearInterval(interval);
            done();
          }
        }, 20);
        done(); /* tells VueJS when this is finished */
      },
      afterEnter(el) {
        console.log('afterEnter');
      },
      enterCancelled(el) {
        console.log('enterCancelled');
      },
      beforeLeave(el) {
        console.log('beforeLeave');
        this.elementWidth = 300;
        el.style.width = this.elementWidth + 'px';
      },
      leave(el, done) {
        console.log('leave');
        let round = 1;
        const interval = setInterval(() => {
          el.style.width = (this.elementWidth - round*10) + 'px';
          round++;
          if (round > 20) {
            clearInterval(interval);
            done();
          }
        }, 20);
      },
      afterLeave(el) {
        console.log('afterLeave');
      },
      leaveCancelled(el) {
        console.log('leaveCancelled');
      }
    }
  }
</script>

<style>
  .fade-enter {
    opacity: 0;
  }

  .fade-enter-active {
    transition: opacity 1s;
  }

  .fade-leave {
    /* opacity: 1; */
  }

  .fade-leave-active {
    transition: opacity 1s;
    opacity: 0;
  }

  .slide-enter {
    opacity: 0;
    /* transform: translateY(20px); */
  }

  .slide-enter-active {
    animation: slide-in 1s ease-out forwards;
    transition: opacity 0.5s;
  }

  .slide-leave {

  }

  .slide-leave-active {
    animation: slide-out 1s ease-out forwards;
    transition: opacity 1s;
    opacity: 0;
  }

  @keyframes slide-in {
    from {
      transform: translateY(20px);
    }
    to {
      transform: translateY(0);
    }
  }
  @keyframes slide-out {
    from {
      transform: translateY(0);
    }
    to {
      transform: translateY(20px);
    }
  }
</style>
```

---

## 207: Animating Dynamic Components

### App.vue (partial)

```html
<button class="button button-primary"
        @click="selectedComponent == 'app-success-alert' ?
                selectedComponent = 'app-danger-alert' :
                selectedComponent = 'app-success-alert'"
>Tottle Components</button>
<br><br>
<transition name="fade" mode="out-in">
  <component :is="selectedComponent"></component>
</transition>

<!------------------------------------------------------------->
<script>
import DangerAlert from './DangerAlert.vue';
import SuccessAlert from './SuccessAlert.vue';

components: {
      appDangerAlert: DangerAlert,
      appSuccessAlert: SuccessAlert
</script>
```

Then added DangerAlert.vue and SuccessAlert.vue

```html
<template>
  <div class="alert alert-danger">This is dangerous!</div>
</template>
```

```html
<template>
  <div class="alert alert-success">This is successful!</div>
</template>
```

![Video](./images/Lesson207.gif)

---

## 208: Animating Lists with `<transition-group>`

Done.

---

## 209: Using `<transition-group>` Preparations

![Video](./images/Lesson209.gif)

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Animations</h1>
        <hr>
        <select v-model="alertAnimation" class="form-control">
          <option value="fade">Fade</option>
          <option value="slide">Slide</option>
        </select>
        <br><br>
        <button class="btn btn-primary" @click="show = !show">Show Alert</button>
        <br><br>
        <transition :name="alertAnimation">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition name="slide" type="animation">
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition
          enter-active-class="animated bounce"
          leave-active-class="animated shake"
        >
          <div class="alert alert-info" v-if="show">This is some info</div>
        </transition>
        <transition :name="alertAnimation" mode="out-in">
          <div class="alert alert-info" v-if="show" key="info">This is some Info</div>
          <div class="alert alert-warning" v-else key="warning">This is some Info</div>
        </transition>
        <hr>
        <button class="btn btn-primary" @click="load = !load">Load / Remove Element</button>
        <br><br>
        <transition
          @before-enter="beforeEnter"
          @enter="enter"
          @after-enter="afterEnter"
          @enter-cancelled="enterCancelled"

          @before-leave="beforeLeave"
          @leave="leave"
          @after-leave="afterLeave"
          @leave-cancelled="leaveCancelled"
          :css="false">
          <div style="width: 300px; height: 100px; background-color: lightgreen" v-if="load"></div>
        </transition>
        <hr>
        <button class="button button-primary"
          @click="selectedComponent == 'app-success-alert' ?
                  selectedComponent = 'app-danger-alert' :
                  selectedComponent = 'app-success-alert'"
        >Tottle Components</button>
        <br><br>
        <transition name="fade" mode="out-in">
          <component :is="selectedComponent"></component>
        </transition>
        <hr>
<!----------------------added this --------------------------------------->
        <button class="btn btn-primary" @click="addItem">Add Item</button>
        <br><br>
        <ul class="list-group">
          <li
            class="list-group-item"
            v-for="(number, index) in numbers"
            @click="removeItem(index)"
            style="cursor: pointer">{{ number }}</li>
        </ul>
<!----------------------------------------------------------------------->
      </div>
    </div>
  </div>
</template>

<script>
  import DangerAlert from './DangerAlert.vue';
  import SuccessAlert from './SuccessAlert.vue';
  export default {
    data() {
      return {
        show: false,
        load: true,
        alertAnimation: 'fade',
        elementWidth: 100,
        selectedComponent: 'app-success-alert',
        numbers: [1, 2, 3, 4, 5]      /* -----------added -------------*/
      }
    },
    methods: {
      beforeEnter(el) {
        console.log('beforeEnter');
        this.elementWidth = 100;
        el.style.width = this.elementWidth + 'px';
      },
      enter(el, done) {
        console.log('enter');
        let round = 1;
        const interval = setInterval(() => {
          el.style.width = (this.elementWidth + round*10) + 'px';
          round++;
          if (round > 20) {
            clearInterval(interval);
            done();
          }
        }, 20);
        done();
      },
      afterEnter(el) {
        console.log('afterEnter');
      },
      enterCancelled(el) {
        console.log('enterCancelled');
      },
      beforeLeave(el) {
        console.log('beforeLeave');
        this.elementWidth = 300;
        el.style.width = this.elementWidth + 'px';
      },
      leave(el, done) {
        console.log('leave');
        let round = 1;
        const interval = setInterval(() => {
          el.style.width = (this.elementWidth - round*10) + 'px';
          round++;
          if (round > 20) {
            clearInterval(interval);
            done();
          }
        }, 20);
      },
      afterLeave(el) {
        console.log('afterLeave');
      },
      leaveCancelled(el) {
        console.log('leaveCancelled');
      },
/* ---------------------------added this -----------------------------------*/
      addItem() {
        const pos = Math.floor(Math.random() * this.numbers.length);
        this.numbers.splice(pos, 0, this.numbers.length + 1);
      },
      removeItem(index) {
        this.numbers.splice(index, 1);
      }
/*----------------------------------------------------------------------------*/
    },
    components: {
      appDangerAlert: DangerAlert,
      appSuccessAlert: SuccessAlert
    }
  }
</script>

<style>
  .fade-enter {
    opacity: 0;
  }

  .fade-enter-active {
    transition: opacity 1s;
  }

  .fade-leave {
    /* opacity: 1; */
  }

  .fade-leave-active {
    transition: opacity 1s;
    opacity: 0;
  }

  .slide-enter {
    opacity: 0;
    /* transform: translateY(20px); */
  }

  .slide-enter-active {
    animation: slide-in 1s ease-out forwards;
    transition: opacity 0.5s;
  }

  .slide-leave {

  }

  .slide-leave-active {
    animation: slide-out 1s ease-out forwards;
    transition: opacity 1s;
    opacity: 0;
  }

  @keyframes slide-in {
    from {
      transform: translateY(20px);
    }
    to {
      transform: translateY(0);
    }
  }
  @keyframes slide-out {
    from {
      transform: translateY(0);
    }
    to {
      transform: translateY(20px);
    }
  }
</style>
```

---

## 210: Using `<transition-group>` to Animate a List

- `<transition>` is NOT rendered to the DOM
- `<transition-group>` DOES render a new HTML tag
  - usually `<span>`, but this can be overwritten with `<transition-group tag="TAG">`

### Wrapped the list in a transition-group

```html
<transition-group name="slide">
  <li
      class="list-group-item"
      v-for="(number, index) in numbers"
      @click="removeItem(index)"
      style="cursor: pointer"
      :key="number">{{ number }}</li> <!--added key-->
</transition-group>
```

### Also added new class (`.slide-move`)

```css
  .slide-leave-active {
    animation: slide-out 1s ease-out forwards;
    transition: opacity 1s;
    opacity: 0;
    position: absolute; /* new */
    width: 100%;    /* new */
  }

  .slide-move {
    transition: transform 1s;
  }
```

![Video](./images/Lesson211.gif)

---

## 211: Understanding the App

Intro to the Super Quiz app.

---

## 212: Creating the App

General review of the basic code set-up. The focus will be on the animations.

### Question.vue

```html
<template>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title text-center">{{ question }}</h3>
    </div>
    <div class="panel-body">
      <div class="col-xs-12 col-sm-6 text-center">
        <button class="btn btn-primary btn-lg" style="margin: 10px" @click="onAnswer(btnData[0].correct)">{{ btnData[0].answer }}</button>
      </div>
      <div class="col-xs-12 col-sm-6 text-center">
        <button class="btn btn-primary btn-lg" style="margin: 10px" @click="onAnswer(btnData[1].correct)">{{ btnData[1].answer }}</button>
      </div>
      <div class="col-xs-12 col-sm-6 text-center">
        <button class="btn btn-primary btn-lg" style="margin: 10px" @click="onAnswer(btnData[2].correct)">{{ btnData[2].answer }}</button>
      </div>
      <div class="col-xs-12 col-sm-6 text-center">
        <button class="btn btn-primary btn-lg" style="margin: 10px" @click="onAnswer(btnData[3].correct)">{{ btnData[3].answer }}</button>
      </div>
    </div>
  </div>
</template>
<style>

</style>
<script>
  const MODE_ADDITION = 1;
  const MODE_SUBTRACTION = 2;
  export default{
    data() {
      return {
        question: 'Oops, an error ocurred :/',
        btnData: [
          {correct: true, answer: 0},
          {correct: false, answer: 0},
          {correct: false, answer: 0},
          {correct: false, answer: 0}
        ]
      };
    },
    methods: {
      generateQuestion() {
        const firstNumber = this.generateRandomNumber(1, 100);
        const secondNumber = this.generateRandomNumber(1, 100);
        const modeNumber = this.generateRandomNumber(1, 2);

        let correctAnswer = 0;

        switch (modeNumber) {
          case MODE_ADDITION:
            correctAnswer = firstNumber + secondNumber;
            this.question = `What's ${firstNumber} + ${secondNumber}?`;
            break;
          case MODE_SUBTRACTION:
            correctAnswer = firstNumber - secondNumber;
            this.question = `What's ${firstNumber} - ${secondNumber}?`;
            break;
          default:
            correctAnswer = 0;
            this.question = 'Oops, an Error occurred :/';
        }
        /* generate random number that is wrong and set correct to false
          we will replace a random button with the correct answer */
        this.btnData[0].answer = this.generateRandomNumber(correctAnswer - 10, correctAnswer + 10, correctAnswer);
        this.btnData[0].correct = false;
        this.btnData[1].answer = this.generateRandomNumber(correctAnswer - 10, correctAnswer + 10, correctAnswer);
        this.btnData[1].correct = false;
        this.btnData[2].answer = this.generateRandomNumber(correctAnswer - 10, correctAnswer + 10, correctAnswer);
        this.btnData[2].correct = false;
        this.btnData[3].answer = this.generateRandomNumber(correctAnswer - 10, correctAnswer + 10, correctAnswer);
        this.btnData[3].correct = false;
        /* generate correct button */
        const correctButton = this.generateRandomNumber(0, 3);
        this.btnData[correctButton].correct = true;
        this.btnData[correctButton].answer = correctAnswer;
      },
      /* a random number generator that will exclude the exception */
      generateRandomNumber(min, max, except) {
        const rndNumber = Math.round(Math.random() * (max - min)) + min;
        console.log(min, max, rndNumber);
        if (rndNumber == except) {
          return this.generateRandomNumber(min, max, except);
        }
        return rndNumber;
      },
      /* emit the question was answered correctly */
      onAnswer(isCorrect) {
        this.$emit('answered', isCorrect);
      }
    },
    created() {
      this.generateQuestion();
    }
  }
</script>
```

### Answer.vue

```html
<template>
  <div class="alert alert-success text-center">
    <h1>That's Correct!</h1>
    <hr>
    <button class="btn btn-primary" @click="onNextQuestion">Next Question</button>
  </div>
</template>
<style>

</style>
<script>

  export default{
    methods: {
      onNextQuestion() {
        this.$emit('confirmed');
      }
    }
  }
</script>
```

### App.vue

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1 class="text-center">The Super Quiz</h1>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
      <!-- listen for answer and confirmed -->
          <component :is="mode" @answered="answered($event)" @confirmed="mode = 'app-question'"></component>

      </div>
    </div>
  </div>
</template>

<script>
  import Question from './components/Question.vue';
  import Answer from './components/Answer.vue';

  export default {
    data() {
      return {
        mode: 'app-question'
      }
    },
    methods: {
      answered(isCorrect) {
        if (isCorrect) {
          this.mode = 'app-answer';
        } else {
          this.mode = 'app-question';
          alert('Wrong, try again!');
        }
      }
    },
    components: {
      appQuestion: Question,
      appAnswer: Answer
    }
  }
</script>
```

---

## 213: Adding Animations

### Changes in the App.vue file

```html
<!-- added transition -->
<transition name="flip" mode="out-in">
  <component :is="mode" @answered="answered($event)" @confirmed="mode = 'app-question'"></component>
</transition>

<!-- and the css -->
<style>
  /* .flip-enter {
    transform: rotateY(0deg); -- default
   } */

  .flip-enter-active {
    animation: flip-in 0.5s ease-out forwards;
  }

  /* .flip-leave {
    transform: rotateY(0deg); -- default
   } */

  .flip-leave-active {
    animation: flip-out 0.5s ease-out forwards;
  }

  @keyframes flip-out {
    from {
      transform: rotateY(0deg);
    }
    to {
      transform: rotateY(90deg);
    }
  }

  @keyframes flip-in {
    from {
      transform: rotateY(90deg);
    }
    to {
      transform: rotateY(0deg);
    }
  }

</style>
```

![video](./images/Lesson213.gif)

---

## 214: Wrap Up

Done

---

## 215: Module Resources & Useful Links

Just section code

---

## 216: The Animated "Monster Slayer" App

In Code folder.

---
