# Bonus: Using Axios Instead of `vue-resource`

## 341: About this Section

Done

---

## 342: Module Introduction

- Axios - a popular package for making http requests from JavaScript
- alternative to vue-resource
- using either is fine

---

## 343: Project Setup

Set up Firebase

---

## 344: Axios Setup

Added Axios to our project

---

## 345: Sending a POST Request

### Expanded `onSubmit()` in signup.vue

```js
onSubmit () {
  const formData = {
    email: this.email,
    age: this.age,
    password: this.password,
    confirmPassword: this.confirmPassword,
    country: this.country,
    hobbies: this.hobbyInputs.map(hobby => hobby.value),
    terms: this.terms
  }
  console.log(formData)
  axios.post('https://udemy-vuejs-axios-515be.firebaseio.com/users.json', formData)
    .then(res => console.log(res))
    .catch(error => console.log(error))
}
```

### Results in Firebase

![image](./images/Lesson345.png)

---

## 346: Sending a GET Request

### added `created()` section into dashboard.vue

```js
created () {
  axios.get('https://udemy-vuejs-axios-515be.firebaseio.com/users.json')
    .then(res => console.log(res))
    .catch(error => console.log(error))
}
```

### Results in console

![image](./images/Lesson346.png)

---

## 347: Accessing & Using Response Data

### expanding dashboard.vue

```js
created () {
  axios.get('https://udemy-vuejs-axios-515be.firebaseio.com/users.json')
    .then(res => {
      console.log(res)
      const data = res.data // simplifying next lines
      const users = []      // empty array
      for (let key in data) { // user set as the unique key in data
        const user = data[key]
        user.id = key         // set this key as the user id
        users.push(user)      // add this user to the list
      }
      console.log(users);
      this.email = users[0].email  // set email as the first user's email
    })
    .catch(error => console.log(error))
}
  ```

---

## 348: Setting a Global Request Configuration

### added baseURL to main.js

- common header can also be added to all requests
- in this case, the 3rd line is added to all `get` requests

```js
axios.defaults.baseURL = 'https://udemy-vuejs-axios-515be.firebaseio.com'
axios.defaults.headers.common['Authorization'] = 'test'
axios.defaults.headers.get['Accepts'] = 'application/json'
```

---

## 349: Using Interceptors

- Interceptors are functions you can define which should be executed on every request that leaves the app, or every response that reaches it

### main.js

```js
const reqInterceptor = axios.interceptors.request.use(config => {
  console.log('Request Interceptor', config)  
  return config
})

const resInterceptor = axios.interceptors.response.use(res => {
  console.log('Response Interceptor', res)
  return res
})

axios.interceptors.request.eject(reqInterceptor)
axios.interceptors.response.eject(resInterceptor)
```

---

## 350: Custom Axios Instances

### created axios-auth.js to use a custom instance

- we can import this instead of the default

```js
import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://udemy-vuejs-axios-515be.firebaseio.com'
})

instance.defaults.headers.common['SOMETHING'] = 'something'

export default instance
```

---

## 351: Wrap Up

Done

---

## 352: Useful Resources & Links

[Axios](https://github.com/axios/axios)

---
