# Connecting to Servers via HTTP - Using vue-resource

## 217: Module Introduction

Done.

---

## 218: Accessing HTTP

### Installed Vue Resource via npm and added it to main.js

```js
import Vue from 'vue'
import App from './App.vue'
import VueResource from './vue-resource'

Vue.use(VueResource); // `use' tells Vuejs to add a plugin

new Vue({
  el: '#app',
  render: h => h(App)
})
```

---

## 219: Firebase & the Right Database

Choose Realtime Databases

---

## 220: Creating an Application and Setting Up a Server (Firebase)

### app.js

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Http</h1>
        <div class="form-group">
          <label>Username</label>
          <input class="form-control" type="text" v-model="user.username">
        </div>
        <div class="form-group">
          <label>Mail</label>
          <input class="form-control" type="text" v-model="user.email">
        </div>
        <button class="btn btn-primary" @click="Submit">Submit</button>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        user: {
          username: '',
          email: ''
        }
      }
    },
    methods: {
      submit() {
        console.log(this.user.username, this.user.email)
      }
    }
  }
</script>

<style>
</style>

```

![image](./images/Lesson220.png)

---

## 221: POSTing Data to a Server (Sending a POST Request)

### app.js

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Http</h1>
        <div class="form-group">
          <label>Username</label>
          <input class="form-control" type="text" v-model="user.username">
        </div>
        <div class="form-group">
          <label>Mail</label>
          <input class="form-control" type="text" v-model="user.email">
        </div>
        <button class="btn btn-primary" @click="submit">Submit</button>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        user: {
          username: '',
          email: ''
        }
      }
    },
    methods: {
      submit() {
        // console.log(this.user.username, this.user.email)
        this.$http.post('https://vuejs-http-acf21.firebaseio.com/data.json', this.user)
          .then(response => {
            console.log(response)
          }, error => {
            console.log(error)
          });

      }
    }
  }
</script>
```

### input page and console output

![image](./images/Lesson221.png)

### data results on Firebase

- each submission gets a unique identifier

![image](./images/Lesson221b.png)

---

## 222: GETting and Transforming Data (Sending a GET Request)

### app.js

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Http</h1>
        <div class="form-group">
          <label>Username</label>
          <input class="form-control" type="text" v-model="user.username">
        </div>
        <div class="form-group">
          <label>Mail</label>
          <input class="form-control" type="text" v-model="user.email">
        </div>
        <button class="btn btn-primary" @click="submit">Submit</button>
        <hr>
        <button class="btn btn-primary" @click="fetchData">Get Data</button>
        <br><br>
        <!-- make a list of users in the array -->
        <ul class="list-group">
          <li :key="i" class="list-group-item" v-for="(u,i) in users">{{ u.username }} - {{ u.email }}</li>
        </ul>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        user: {
          username: '',
          email: ''
        },
        users: []
      }
    },
    methods: {
      submit() {
        this.$http.post('https://vuejs-http-acf21.firebaseio.com/data.json', this.user)
          .then(response => {
            console.log(response)
          }, error => {
            console.log(error)
          });
      },
      fetchData() {
        this.$http.get('https://vuejs-http-acf21.firebaseio.com/data.json')
          .then(response => {
            return response.json(); // a promise
          })
          // make an array
          .then(data => {
            const resultArray = [];
            for (let key in data) {
              resultArray.push(data[key]);
            }
            this.users = resultArray;
          });
      }
    }
  }
</script>
```

![image](./images/Lesson222.png)

---

## 223: Configuring vue-resource Globally

### added to main.js

- we can save code by putting things here that don't change

```js
Vue.http.options.root = 'https://vuejs-http-acf21.firebaseio.com/data.json'
```

---

## 224: Intercepting Requests

- PUT overwrites old data
- POST appends

### main.js

```js
import Vue from 'vue'
import VueResource from 'vue-resource';
import App from './App.vue'

Vue.use(VueResource);

Vue.http.options.root = 'https://vuejs-http-acf21.firebaseio.com/data.json'
Vue.http.interceptors.push((request, next) => {
  console.log(request);
  /* if the method is a POST, change it to PUT
    so that it overwrites everything
  */
  if (request.method == 'POST') {
    request.method = 'PUT';
  }
  next();
})


new Vue({
  el: '#app',
  render: h => h(App)
})

```

---

## 225: Intercepting Responses

### main.js

```js
import Vue from 'vue'
import VueResource from 'vue-resource';
import App from './App.vue'

Vue.use(VueResource);

Vue.http.options.root = 'https://vuejs-http-acf21.firebaseio.com/data.json'
Vue.http.interceptors.push((request, next) => {
  console.log(request);
  if (request.method == 'POST') {
    request.method = 'PUT';
  }
  // added this
  next(response => {
    response.json = () => { return {messages: response.body}}
  });
})
// extracts response.body and assigns it to 'messages'

new Vue({
  el: '#app',
  render: h => h(App)
})
```

---

## 226: Where the "resource" in vue-resource Comes From

- allows us to set up mappings to common http requests

### app.vue

```js
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Http</h1>
        <div class="form-group">
          <label>Username</label>
          <input class="form-control" type="text" v-model="user.username">
        </div>
        <div class="form-group">
          <label>Mail</label>
          <input class="form-control" type="text" v-model="user.email">
        </div>
        <button class="btn btn-primary" @click="submit">Submit</button>
        <hr>
        <button class="btn btn-primary" @click="fetchData">Get Data</button>
        <br><br>
        <ul class="list-group">
          <li :key="i" class="list-group-item" v-for="(u,i) in users">{{ u.username }} - {{ u.email }}</li>
        </ul>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        user: {
          username: '',
          email: ''
        },
        users: [],
        resource: [] // new
      }
    },
    methods: {
      submit() {
        // ditched old submit for this
        // still works same
        this.resource.save({},this.user)
      },
      fetchData() {
        this.$http.get('data.json')
          .then(response => {
            return response.json();
          })
          .then(data => {
            const resultArray = [];
            for (let key in data) {
              resultArray.push(data[key]);
            }
            this.users = resultArray;
          });
      }
    },
    // added created section
    created() {
      this.resource = this.$resource('data.json');
    }
  }
</script>
```

---

## 227: Creating Custom Resources

### app.js

```js
// new Submit
this.resource.saveAlt(this.user);

// new created()
created() {
    const customActions = {
      saveAlt: {method: 'POST', url: 'alternative.json'}
    }
    this.resource = this.$resource('data.json', {}, customActions);
  }

/*
now we call 'saveAlt', where we can have any custom action we want
new image shows 'alternative' section in addition to data
we could use this to send data to different places depending on the situation
*/
```

![image](./images/Lesson227.png)

---

## 228: Resources vs "Normal" HTTP Requests

- resources are optional, not required if 'normal' requests are sufficient

---

## 229: Understanding Template URLs

### app.js

- here we shortened the GET request just as we had the POST request

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Http</h1>
        <div class="form-group">
          <label>Username</label>
          <input class="form-control" type="text" v-model="user.username">
        </div>
        <div class="form-group">
          <label>Mail</label>
          <input class="form-control" type="text" v-model="user.email">
        </div>
        <button class="btn btn-primary" @click="submit">Submit</button>
        <hr>
        <input class="form-control" type="text" v-model="node">
        <br><br>
        <button class="btn btn-primary" @click="fetchData">Get Data</button>
        <br><br>
        <ul class="list-group">
          <li :key="i" class="list-group-item" v-for="(u,i) in users">{{ u.username }} - {{ u.email }}</li>
        </ul>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        user: {
          username: '',
          email: ''
        },
        users: [],
        resource: [],
        node: 'data',
      }
    },
    methods: {
      submit() {
        // console.log(this.user.username, this.user.email)
        // this.$http.post('data.json', this.user)
        //   .then(response => {
        //     console.log(response)
        //   }, error => {
        //     console.log(error)
        //   });
        // this.resource.save({},this.user)
        this.resource.saveAlt(this.user);
      },
      fetchData() {
        // this.$http.get('data.json')
          // .then(response => {
          //   return response.json();
          // })
          // .then(data => {
          //   const resultArray = [];
          //   for (let key in data) {
          //     resultArray.push(data[key]);
          //   }
          //   this.users = resultArray;
          // });
        this.resource.getData({node: this.node}) // replaces {node} key with this.node
          .then(response => {
            return response.json();
          })
          .then(data => {
            const resultArray = [];
            for (let key in data) {
              resultArray.push(data[key]);
            }
            this.users = resultArray;
          });
      }
    },
    created() {
      const customActions = {
        saveAlt: {method: 'POST', url: 'alternative.json'},
        getData: {method: 'GET'}
      }
      this.resource = this.$resource('{node}.json', {}, customActions);
    }
  }
</script>
```

---

## 230: Wrap Up

Done

---

## 231: Module Resources & Useful Links

- [vue-resource on Github](https://github.com/yyx990803/vue-resource)
- [Some Code Recipes for vue-resource](https://github.com/yyx990803/vue-resource/blob/master/docs/recipes.md)
- [Template URLs](https://medialize.github.io/URI.js/uri-template.html)
- [Requests and Responses (incl. Different File Formats)](https://github.com/yyx990803/vue-resource/blob/master/docs/http.md)

---
