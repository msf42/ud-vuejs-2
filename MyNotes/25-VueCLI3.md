# Vue CLI 3

## 387: Vue CLI 3 Summary

[Summary](https://medium.com/the-vue-point/vue-cli-3-0-is-here-c42bebe28fbb)

---

## 388: Module Introduction

![image](./images/Lesson388.png)

---

## 389: Creating a Project

`npm install -g @vue/cli`

`vue create new-project-name`

- walked through installation process and choosing options
- info saved in `.vuerc` file
- `npm run serve` runs dev server

---

## 390: Analyzing the Created Project

- look through package.json and other config files

---

## 391: Using Plugins

- naming convention: `vue-cli-plugin-mypluginname`
- official plugins: `@vuecli-plugin-pluginname`

- instead of `npm install @vuecli-plugin-vuetify`, we can run `vue add vuetify`

---

## 392: CSS Pre-Processors

- we can add them in an existing project
- `npm install --save sass-loader node-sass`

---

## 393: Environment Variables

- create a file, `.env` in the main folder
- we can add any environment variable here and reference them anywhere

```js
VUE_APP_URL=https://dev.api.com
```

- then, in our data section of a vue file, we can define:

```js
url: process.env.VUE_APP_URL
```

- now we can use `url` like any Vue variable by using `{{ url }}`
- name must be `VUE_APP_`
- we can also make 3 different files to use different variables at different stages of development
  - `.env.test`
  - `.env.development`
  - `.env.production`

---

## 394: Building the Project

- just notes on build options; pretty straightforward

---

## 395: Instant Prototyping

`npm install -g @vue/cli-service-global`

- now we can run `vue serve Hello.vue` to inspect a single Vue component
  - even if it is not part of our overall project

---

## 396: Different Build Targets

### 3 Different Build Targets

- Vue App
- Vue Library
- Web Component
  - a custom HTML element that can be used outside of a Vue project

![image](./images/Lesson396.png)

---

## 397: Using the "Old" CLI Templates (vue init)

- `npm install -g @vue/cli-init` allows using the old commmand

---

## 398: Using the GUI

- `vue ui` opens the GUI
- allows browsing folders, creating new projects, etc
- same features as CLI, but in GUI form
- can also search plugins

---

## 399: Alternative Lazy Loading Syntax

Alternatively to the lazy loading syntax shown earlier in the course (in the Routing section), you can also use the following syntax for loading components lazily upon routing when using a project created with Vue CLI 3+:

```js
const User = () => import('./components/user/User.vue');
...
export const routes = [
  ...,
    {
      path: '/user', components: {
        default: User,
        ...
      }
    }
];
```

---
